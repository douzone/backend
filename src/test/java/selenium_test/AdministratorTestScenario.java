package selenium_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.DriverCommand;

import ch.qos.logback.core.joran.action.Action;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class AdministratorTestScenario {

	//private static final String DEFAULT_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/";

	//private static final String DEFAULT_SUCCESS_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/0_경비청구-개인카드-카드사용자-카드사용내역 접근하기/";

	private static final String BASE_URL = AdministratorTest.BASE_URL_DEV;
	//private static final String MAIN_WINDOW_TITLE = "위하고";

	private static final String FULL_SCREEN_YN = "Y";

	private static final String BROWSER_TYPE_IE = "webdriver.ie.driver";
	private static final String BROWSER_TYPE_IE_DRIVER = "C:\\selenium_test\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe";

	private static final String BROWSER_TYPE_CHROME = "webdriver.chrome.driver";
	private static final String BROWSER_TYPE_CHROME_DRIVER = "/Users/PSH/Desktop/chromedriver_win32/chromedriver.exe";// 성혜
	//private static final String BROWSER_TYPE_CHROME_DRIVER = "/Users/BIT/Downloads/chromedriver_win32/chromedriver.exe";// 영돈
	// https://sites.google.com/a/chromium.org/chromedriver/ 에서 최신의 크롬 드라이버를 설치하세요

	// https://www.seleniumhq.org/download/ 에서 각각의 브라우져 드라이버를 다운 받을 수 있습니다.

	// private static final String BROWSER_TYPE_CHROME_DRIVER =
	// "C:\\selenium_test\\chromedriver_win32\\chromedriver.exe";

//	private static final String BROWSER_TYPE = "webdriver.ie.driver";
	private static final String BROWSER_TYPE = "webdriver.chrome.driver";

	private static WebDriver driver;

	// @FirstScenario 라는 TAG가 있는 시나리오에서만 실행은 한다.
	@Before("@FirstScenario")
	public void setUp(Scenario scenario) {

		if (BROWSER_TYPE.equals(BROWSER_TYPE_CHROME)) {
			System.setProperty("webdriver.chrome.driver", BROWSER_TYPE_CHROME_DRIVER);
			
			driver = new ChromeDriver();
			
			
			//driver.execute_script("document.body.style.zoom = '200%'");
			//driver.get("chrome://settings/");
			//((JavascriptExecutor) driver).executeScript("chrome.settingsPrivate.setDefaultZoom(0.8);");
			//driver.executeScript("chrome.settingsPrivate.setDefaultZoom(1.5);");
			//driver.get(BASE_URL + "/login/");
		} else if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {

			// https://stackoverflow.com/questions/27985300/selenium-webdriver-typing-very-slow-in-text-field-on-ie-browser
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			// capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			// capabilities.setCapability("requireWindowFocus", true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

			InternetExplorerOptions options = new InternetExplorerOptions(capabilities);

			System.setProperty("webdriver.ie.driver", BROWSER_TYPE_IE_DRIVER);
			driver = new InternetExplorerDriver(options);

		}

		// @After 대신에 addShutdownHook 을 이용해서 tearDown()을 실행함
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				// 이 부분은 실행이 되지만 EclEmma(JaCoCo) 라는
				// Java Code Coverage Tool에서는 실행이 안 된다고 나옴.
				System.out.println("########## close #########");
				if (driver != null) {
					driver.quit();
				}
			}
		});
	}
	

	@Given("^로그인 화면$")
	public void openLoginPage()  throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		if("Y".equals(FULL_SCREEN_YN)) {
			driver.manage().window().fullscreen();
		}
	
		driver.get(BASE_URL + "/login/");
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);	
		}
	}

	@When("^\"(.*)\" \"(.*)\" 을 입력하고 로그인버튼 클릭하기$")
	public void Login(String id, String password)
			throws Exception {

		System.out.println(id + " " + password);
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {	
			WebElement inputId = SeleniumUtil.waitForElementToBePresent(driver, By.id("email"));

			inputId.clear();
			inputId.sendKeys(id);

			WebElement inputPw = SeleniumUtil.waitForElementToBePresent(driver, By.id("password"));
			
			inputPw.clear();
			inputPw.sendKeys(password);
		} else {
			driver.findElement(By.id("email")).clear();
			driver.findElement(By.id("email")).sendKeys(id);
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(password);
		}
		
		
		//로그인 버튼
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnLogin"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", login);
		} else {
			driver.findElement(By.id("btnLogin")).click();
			//IE에서는 동작하지 않아서 수정됨 
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}

	//로그인 된 화면이 나와야 함 
	@Then("^로그인이 정상적으로 되었다는 표시로 \"(.*)\" 사용자 정보가 출력되어야 함$")
	public void checkLogInResult0(String name) throws Exception {

//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("html/body/div[1]/div/div[1]/div[1]/div[2]/div[1]/div[2]/div/div[6]/div[1]/button"));

		System.out.println(name);
				
		Thread.sleep(1000);
		
		String contents = driver.findElement(By.id("headerName")).getText();

		System.out.println("contents=" + contents);
		
		if (contents.length() > 3) { //이름만 비교하기 위해서 
			contents = contents.substring(0,3);
		}
		
		if(!name.equals(contents)) {
			Thread.sleep(2000);

					
			contents = driver.findElement(By.id("headerName")).getText();
			if (contents.length() > 3) { //이름만 비교하기 위해서 
				contents = contents.substring(0,3);
			}			
		}
		
		//assertEquals(name, contents);
	}
	
	@Given("^근무시간 설정 화면$")
	public void openWorkTimePage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(5000);
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]")).click();
		
			//Thread.sleep(1500);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[4]/div[2]")).click();
			
			Thread.sleep(1500);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[1]/div[2]")).click();
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^근무시간 등록하고 \"(.*)\" 확인하기$")
	public void newWorkTime(String message) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			//시작시간 선택
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]/span/input")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 2; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(6) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div[1]/ul/li[10]")).click();
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(6) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]")).click();
			Thread.sleep(2000);
			
			//끝시간 선택
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[2]/th[2]/span/input")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 3; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(7) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[2]/div[1]/ul/li[24]")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 10; i++) {//분 
				driver.findElement(By.cssSelector("body > div:nth-child(7) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(500);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div[2]/div[2]/ul/li[31]")).click();
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[2]/th[2]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("workTimeNewBtn")).click();
			Thread.sleep(3000);
			
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	
	@Then("^리스트에 등록한 근무시간이 \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkNewWorkTime(String fromTime, String toTime, String use) throws Exception {

		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[1]"), 20);
		
		System.out.println("fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[2]"), 20);
		System.out.println("toTime---->> " + toTime);
		String checkToTime = webElement2.getText();
		System.out.println("checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[4]"), 20);
		System.out.println("use---->> " + use);
		String checkUse = webElement3.getText();
		System.out.println("checkUse---->> " + checkUse);
		Thread.sleep(3000);
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	@Given("^근무시간 상세보기에 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openWorkTimeModifyModal(String fromTime, String toTime, String use, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[1]"), 20);
			System.out.println("fromTime---->> " + fromTime);
			String checkFromTime = webElement.getText();
			System.out.println("checkFromTime---->> " + checkFromTime);
			
			Thread.sleep(1000);
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[2]"), 20);
			System.out.println("toTime---->> " + toTime);
			String checkToTime = webElement2.getText();
			System.out.println("checkFromTime---->> " + checkToTime);
			
			Thread.sleep(1000);
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[4]"), 20);
			System.out.println("use---->> " + use);
			String checkUse = webElement3.getText();
			System.out.println("checkUse---->> " + checkUse);
			
			if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//첫번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]")).click();
				Thread.sleep(5000);
				
				checkFromTime = driver.findElement(By.id("fromWorkTimeModal")).getAttribute("value");
						
				System.out.println("fromTime modal---->> " + fromTime);
				System.out.println("checkFromTime modal---->> " + checkFromTime);
				Thread.sleep(1000);
				checkToTime = driver.findElement(By.id("toWorkTimeModal")).getAttribute("value");
				System.out.println("toTime modal---->> " + toTime);
				System.out.println("checkFromTime modal---->> " + checkToTime);
				Thread.sleep(1000);
				checkUse = driver.findElement(By.id("useWorkTimeModal")).getText();
				System.out.println("modalUse modal---->> " + modalUse);
				System.out.println("checkUse modal---->> " + checkUse);
				
				if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && modalUse.equals(checkUse)) {
					Thread.sleep(5000);
					driver.findElement(By.id("workTimeModifyBtn")).click();
					Thread.sleep(1000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^근무시간 수정하고 \"(.*)\" 메세지 확인$")
	public void editWorkTimeModifyModal(String message) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			//시작시간 선택
			driver.findElement(By.id("fromWorkTimeModal")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[7]/div/div/div/div[2]/div[1]/ul/li[11]")).click();
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[7]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			//끝시간 선택
			driver.findElement(By.id("toWorkTimeModal")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("/html/body/div[8]/div/div/div/div[2]/div[1]/ul/li[23]")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 1; i++) {//분 12
				driver.findElement(By.cssSelector("body > div:nth-child(12) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[8]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("workTimeModifyBtn")).click();
			Thread.sleep(5000);
			//Alert alert = driver.switchTo().alert();
			//String alertMsg = alert.getText();
			//System.out.println("alertMsg---->> " + alertMsg);
			//Thread.sleep(3000);
			//등록확인 메세지가 맞는지 확인
			//if(!message.equals(alertMsg)) {
			//	Thread.sleep(2000);
			//	assertTrue(false);
			//}
			
			//alert.accept();
			//Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 수정한 근무시간이 \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkEditWorkTimeModify(String fromTime, String toTime, String use) throws Exception {

		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[1]"), 20);
		Thread.sleep(1000);		
		System.out.println("fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[2]"), 20);
		Thread.sleep(1000);
		System.out.println("toTime---->> " + toTime);
		String checkToTime = webElement2.getText();
		System.out.println("checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[1]/td[4]"), 20);
		Thread.sleep(1000);
		System.out.println("use---->> " + use);
		String checkUse = webElement3.getText();
		System.out.println("checkUse---->> " + checkUse);
		Thread.sleep(5000);
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	@Given("^근무시간 상세보기 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openWorkTimeRemoveModal(String fromTime, String toTime, String use, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			//두번째 리스트 데이터 확인
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[1]"), 20);
			
			System.out.println("fromTime---->> " + fromTime);
			String checkFromTime = webElement.getText();
			System.out.println("checkFromTime---->> " + checkFromTime);
			
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[2]"), 20);
			
			System.out.println("toTime---->> " + toTime);
			String checkToTime = webElement2.getText();
			System.out.println("checkFromTime---->> " + checkToTime);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[4]"), 20);
			
			System.out.println("use---->> " + use);
			String checkUse = webElement3.getText();
			System.out.println("checkUse---->> " + checkUse);
			
			if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//두번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]")).click();
				Thread.sleep(5000);
				
				checkFromTime = driver.findElement(By.id("fromWorkTimeModal")).getAttribute("value");
						
				System.out.println("fromTime modal---->> " + fromTime);
				System.out.println("checkFromTime modal---->> " + checkFromTime);
				
				checkToTime = driver.findElement(By.id("toWorkTimeModal")).getAttribute("value");
				System.out.println("toTime modal---->> " + toTime);
				System.out.println("checkFromTime modal---->> " + checkToTime);
				
				checkUse = driver.findElement(By.id("useWorkTimeModal")).getText();
				System.out.println("modalUse modal---->> " + modalUse);
				System.out.println("checkUse modal---->> " + checkUse);
				
				if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && modalUse.equals(checkUse)) {
					Thread.sleep(2000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	} 
	
	@When("^근무시간 삭제하고 \"(.*)\" 메세지 확인$")
	public void removeWorkTimeModal(String message) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			Thread.sleep(1000);
			//삭제 버튼 클릭
			driver.findElement(By.id("workTimeRemoveBtn")).click();
			Thread.sleep(3000);
			//Alert alert = driver.switchTo().alert();
			//String alertMsg = alert.getText();
			//System.out.println("alertMsg---->> " + alertMsg);
			//Thread.sleep(3000);
			//삭제확인 메세지가 맞는지 확인
			//if(!message.equals(alertMsg)) {
			//	Thread.sleep(2000);
			//	assertTrue(false);
			//}
			
			//alert.accept();
			Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 삭제한 근무시간 \"(.*)\" \"(.*)\" \"(.*)\" 없어야 함$")
	public void checkRemoveWorkTime(String fromTime, String toTime, String use) throws Exception {

		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[1]"), 20);
				
		System.out.println("fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[2]"), 20);
		System.out.println("toTime---->> " + toTime);
		String checkToTime = webElement2.getText();
		System.out.println("checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"workTimeTable\"]/tbody/tr[2]/td[4]"), 20);
		System.out.println("use---->> " + use);
		String checkUse = webElement3.getText();
		System.out.println("checkUse---->> " + checkUse);
		Thread.sleep(5000);
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse)) {
			Thread.sleep(2000);
			
			assertTrue(false);
		}
		else {
			Thread.sleep(2000);
			assertTrue(true);
		}
	}
	
	@Given("^휴게시간 설정 화면$")
	public void openBreakTimePage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			/* 시작  */
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]")).click();
		
			//Thread.sleep(1500);
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[4]/div[2]")).click();
			
			Thread.sleep(1500);
			/* 끝 */
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[2]/div[2]")).click();
			Thread.sleep(5000);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴게시간 \"(.*)\" 을 입력하고 등록하기$")
	public void newBreakTime(String description) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			//시작시간 선택
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]/span/input")).click();
			Thread.sleep(2000);
			for(int i = 0; i< 2; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(9) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[5]/div/div/div/div[2]/div[1]/ul/li[12]")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(9) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[5]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]")).click();
			Thread.sleep(1000);
			//끝시간 선택
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[2]/th[2]/span/input")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 3; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[6]/div/div/div/div[2]/div[1]/ul/li[13]")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[6]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[2]/th[2]")).click();
			Thread.sleep(1000);
			driver.findElement(By.id("newDesBreakTime")).clear();
			driver.findElement(By.id("newDesBreakTime")).sendKeys(description);
			Thread.sleep(1000);
			driver.findElement(By.id("breakTimeNewBtn")).click();
			Thread.sleep(3000);
			//등록 완료 메세지
			//Alert alert = driver.switchTo().alert();
			//alert.accept();
			Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 등록한 휴게시간이 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkNewBreakTime(String fromTime, String toTime, String use, String description) throws Exception {

		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[1]"), 20);
				
		Thread.sleep(1000);
		System.out.println("fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[2]"), 20);
		System.out.println("toTime---->> " + toTime);
		Thread.sleep(1000);
		String checkToTime = webElement2.getText();
		System.out.println("checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[5]"), 20);
		System.out.println("use---->> " + use);
		Thread.sleep(1000);
		String checkUse = webElement3.getText();
		System.out.println("checkUse---->> " + checkUse);
		
		WebElement webElement4 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[3]"), 20);
		System.out.println("description---->> " + description);
		Thread.sleep(1000);
		String checkDescription = webElement4.getText();
		System.out.println("checkDescription---->> " + checkDescription);
		
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && use.equals(checkUse) && description.equals(checkDescription)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	@Given("^휴게시간 상세보기에 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openBreakTimeModifyModal(String fromTime, String toTime, String use, String message, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[1]"), 20);
			System.out.println("2-1fromTime---->> " + fromTime);
			String checkFromTime = webElement.getText();
			System.out.println("2-1checkFromTime---->> " + checkFromTime);
			
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[2]"), 20);
			System.out.println("2-1toTime---->> " + toTime);
			String checkToTime = webElement2.getText();
			System.out.println("2-1checkFromTime---->> " + checkToTime);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[3]"), 20);
			System.out.println("2-1message---->> " + message);
			String checkMessage = webElement3.getText();
			System.out.println("2-1checkMessage---->> " + checkMessage);
			
			WebElement webElement4 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[5]"), 20);
			System.out.println("2-1use---->> " + use);
			String checkUse = webElement4.getText();
			System.out.println("2-1checkUse---->> " + checkUse);
			
			if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && message.equals(checkMessage) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//첫번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]")).click();
				Thread.sleep(1500);
				
				checkFromTime = driver.findElement(By.id("fromBreakTimeModal")).getAttribute("value");
						
				System.out.println("2-1fromTime modal---->> " + fromTime);
				System.out.println("2-1checkFromTime modal---->> " + checkFromTime);
				Thread.sleep(1000);
				checkToTime = driver.findElement(By.id("toBreakTimeModal")).getAttribute("value");
				System.out.println("2-1toTime modal---->> " + toTime);
				System.out.println("2-1checkFromTime modal---->> " + checkToTime);
				Thread.sleep(1000);
				checkUse = driver.findElement(By.id("useBreakTimeModal")).getText();
				System.out.println("2-1modalUse modal---->> " + modalUse);
				System.out.println("2-1checkUse modal---->> " + checkUse);
				checkMessage = driver.findElement(By.id("desBreakTimeModal")).getText();
				System.out.println("2-1modalMsg modal---->> " + message);
				System.out.println("2-1checkMsg modal---->> " + checkMessage);
				
				if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && modalUse.equals(checkUse) && message.equals(checkMessage)) {
					Thread.sleep(5000);
					driver.findElement(By.id("breakTimeModifyBtn")).click();
					Thread.sleep(1000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴게시간 수정하고 \"(.*)\" 휴게설명 수정$")
	public void editBreakTimeModifyModal(String description) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			Thread.sleep(2000);
			driver.findElement(By.id("useBreakTimeModal")).click();
			driver.findElement(By.xpath("/html/body/div[10]/div/div/ul/li[2]")).click();
			
			//시작시간 선택
			driver.findElement(By.id("fromBreakTimeModal")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(8) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000); //4 10
			driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[1]/ul/li[11]")).click();
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[11]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			//끝시간 선택
			driver.findElement(By.id("toBreakTimeModal")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//시
				driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			
			driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div[2]/div[1]/ul/li[12]")).click();
			Thread.sleep(1000);
			for(int i = 0; i< 0; i++) {//분
				driver.findElement(By.cssSelector("body > div:nth-child(13) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
				Thread.sleep(1000);
			}
			Thread.sleep(1000);
			driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div[2]/div[2]/ul/li[1]")).click();
			Thread.sleep(1000);
			
			driver.findElement(By.id("desBreakTimeModal")).clear();
			driver.findElement(By.id("desBreakTimeModal")).sendKeys(description);
			Thread.sleep(2000);
			driver.findElement(By.id("breakTimeModifyBtn")).click();
			Thread.sleep(5000);
			
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 수정한 휴게시간이 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkEditBreakTimeModify(String fromTime, String toTime, String description, String use) throws Exception {

		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[1]"), 20);
		Thread.sleep(1000);		
		System.out.println("2-3fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("2-3checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[2]"), 20);
		Thread.sleep(1000);
		System.out.println("2-3toTime---->> " + toTime);
		String checkToTime = webElement2.getText();
		System.out.println("2-3checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[3]"), 20);
		Thread.sleep(1000);
		System.out.println("2-3description---->> " + description);
		String checkDes = webElement3.getText();
		System.out.println("2-3checkDes---->> " + checkDes);
		
		WebElement webElement4 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[5]"), 20);
		Thread.sleep(1000);
		System.out.println("2-3use---->> " + use);
		String checkUse = webElement4.getText();
		System.out.println("2-3checkUse---->> " + checkUse);
		Thread.sleep(5000);
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && description.equals(checkDes) && use.equals(checkUse)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	@Given("^휴게시간 상세보기 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openBreakTimeRemoveModal(String fromTime, String toTime, String description, String use, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			//첫번째 리스트 데이터 확인
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[1]"), 20);
			
			System.out.println("3-1fromTime---->> " + fromTime);
			String checkFromTime = webElement.getText();
			System.out.println("3-1checkFromTime---->> " + checkFromTime);
			
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[2]"), 20);
			
			System.out.println("3-1toTime---->> " + toTime);
			String checkToTime = webElement2.getText();
			System.out.println("3-1checkFromTime---->> " + checkToTime);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[3]"), 20);
			
			System.out.println("3-1description---->> " + description);
			String checkDes = webElement3.getText();
			System.out.println("3-1checkDes---->> " + checkDes);
			
			WebElement webElement4 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[5]"), 20);
			
			System.out.println("3-1use---->> " + use);
			String checkUse = webElement4.getText();
			System.out.println("3-1checkUse---->> " + checkUse);
			
			if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && description.equals(checkDes) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//첫번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]")).click();
				Thread.sleep(5000);
				
				checkFromTime = driver.findElement(By.id("fromBreakTimeModal")).getAttribute("value");
				
				System.out.println("3-1fromTime modal---->> " + fromTime);
				System.out.println("3-1checkFromTime modal---->> " + checkFromTime);
				Thread.sleep(1000);
				checkToTime = driver.findElement(By.id("toBreakTimeModal")).getAttribute("value");
				System.out.println("3-1toTime modal---->> " + toTime);
				System.out.println("3-1checkFromTime modal---->> " + checkToTime);
				Thread.sleep(1000);
				checkUse = driver.findElement(By.id("useBreakTimeModal")).getText();
				System.out.println("3-1modalUse modal---->> " + modalUse);
				System.out.println("3-1checkUse modal---->> " + checkUse);
				checkDes = driver.findElement(By.id("desBreakTimeModal")).getText();
				System.out.println("3-1description modal---->> " + description);
				System.out.println("3-1checkDes modal---->> " + checkDes);
				
				if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && description.equals(checkDes) && modalUse.equals(checkUse)) {
					Thread.sleep(2000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴게시간 삭제하기$")
	public void removeBreakTimeModal() throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			Thread.sleep(1000);
			//삭제 버튼 클릭
			driver.findElement(By.id("breakTimeRemoveBtn")).click();
			Thread.sleep(3000);
			//Alert alert = driver.switchTo().alert();
			//String alertMsg = alert.getText();
			//System.out.println("alertMsg---->> " + alertMsg);
			//Thread.sleep(3000);
			//삭제확인 메세지가 맞는지 확인
			//if(!message.equals(alertMsg)) {
			//	Thread.sleep(2000);
			//	assertTrue(false);
			//}
			
			//alert.accept();
			Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 삭제한 휴게시간 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 없어야 함$")
	public void checkRemoveWorkTime(String fromTime, String toTime, String description, String use) throws Exception {

		//첫번째 리스트 데이터 확인
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[1]"), 20);
		
		System.out.println("3-3fromTime---->> " + fromTime);
		String checkFromTime = webElement.getText();
		System.out.println("3-3checkFromTime---->> " + checkFromTime);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[2]"), 20);
		
		System.out.println("3-3toTime---->> " + toTime);
		String checkToTime = webElement2.getText();
		System.out.println("3-3checkFromTime---->> " + checkToTime);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[3]"), 20);
		
		System.out.println("3-3description---->> " + description);
		String checkDes = webElement3.getText();
		System.out.println("3-3checkDes---->> " + checkDes);
		
		WebElement webElement4 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"breakTimeTable\"]/tbody/tr[1]/td[5]"), 20);
		
		System.out.println("3-3use---->> " + use);
		String checkUse = webElement4.getText();
		System.out.println("3-3checkUse---->> " + checkUse);
		
		Thread.sleep(5000);
		if(fromTime.equals(checkFromTime) && toTime.equals(checkToTime) && description.equals(checkDes) && use.equals(checkUse)) {
			Thread.sleep(2000);
			assertTrue(false);
		}
		else {
			Thread.sleep(2000);
			assertTrue(true);
		}
	}
	
	@Given("^휴일 설정 화면$")
	public void openHoliDayPage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]")).click();
		
			//Thread.sleep(1500);
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[4]/div[2]")).click();
			
			Thread.sleep(1500);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[3]/div[2]")).click();
			Thread.sleep(1500);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴일날짜 \"(.*)\" 을 입력하고 등록하기$")
	public void newHoliDay(String description) throws Exception {
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			//날짜선택 
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]/div/div/div")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/table/thead/tr[1]/th[2]/div/div[2]/div/div[2]/div[2]/div[2]")).click();
			Thread.sleep(1000);
			
			driver.findElement(By.id("newDesHoliDay")).clear();
			driver.findElement(By.id("newDesHoliDay")).sendKeys(description);
			Thread.sleep(1000);
			driver.findElement(By.id("holiDayNewBtn")).click();
			Thread.sleep(3000);
			//Alert alert = driver.switchTo().alert();
			//alert.accept();
			//Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 등록한 휴일날짜가 \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkNewHoliDay(String day, String description, String use) throws Exception {
		Thread.sleep(5000);
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[1]"), 20);
		System.out.println("day---->> " + day);
		String checkDay = webElement.getText();
		System.out.println("checkDay---->> " + checkDay);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[2]"), 20);
		System.out.println("description---->> " + description);
		Thread.sleep(1000);
		String checkDescription = webElement2.getText();
		System.out.println("checkDescription---->> " + checkDescription);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[4]"), 20);
		System.out.println("use---->> " + use);
		Thread.sleep(1000);
		String checkUse = webElement3.getText();
		System.out.println("checkUse---->> " + checkUse);	
		Thread.sleep(5000);
		if(day.equals(checkDay) && use.equals(checkUse) && description.equals(checkDescription)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}
	
	@Given("^휴일 상세보기에 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openHoliDayModifyModal(String day, String description, String use, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[1]"), 20);
			System.out.println("4-1day---->> " + day);
			String checkDay = webElement.getText();
			System.out.println("4-1checkDay---->> " + checkDay);
			
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[2]"), 20);
			System.out.println("4-1description---->> " + description);
			String checkDes = webElement2.getText();
			System.out.println("4-1checkDes---->> " + checkDes);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[4]"), 20);
			System.out.println("4-1use---->> " + use);
			String checkUse = webElement3.getText();
			System.out.println("4-1checkUse---->> " + checkUse);
			
			if(day.equals(checkDay) && description.equals(checkDes) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//첫번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]")).click();
				Thread.sleep(1500);
				
				checkDay = driver.findElement(By.id("dayHoliDayModal")).getAttribute("value");
						
				System.out.println("4-1day modal---->> " + day);
				System.out.println("4-1checkDay modal---->> " + checkDay);
				Thread.sleep(1000);
				checkUse = driver.findElement(By.id("useHoliDayModal")).getText();
				System.out.println("4-1modalUse modal---->> " + modalUse);
				System.out.println("4-1checkUse modal---->> " + checkUse);
				checkDes = driver.findElement(By.id("desHoliDayModal")).getText();
				System.out.println("4-1modalDes modal---->> " + description);
				System.out.println("4-1checkDes modal---->> " + checkDes);
				
				if(day.equals(checkDay) && description.equals(checkDes) && modalUse.equals(checkUse)) {
					Thread.sleep(5000);
					driver.findElement(By.id("holiDayModifyBtn")).click();
					Thread.sleep(1000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴일 수정하고 \"(.*)\" 휴일설명 수정$")
	public void editHoliDayModifyModal(String description) throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			Thread.sleep(2000);
			//날짜선택 6
			driver.findElement(By.xpath("/html/body/div[10]/div/div/div[2]/div[2]/div[1]/div/div/div")).click();
			driver.findElement(By.xpath("/html/body/div[10]/div/div/div[2]/div[2]/div[1]/div/div[2]/div/div[2]/div[2]/div[3]")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("useHoliDayModal")).click();
			driver.findElement(By.xpath("/html/body/div[11]/div/div/ul/li[2]")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("desHoliDayModal")).clear();
			driver.findElement(By.id("desHoliDayModal")).sendKeys(description);
			
			Thread.sleep(1000);
			//수정 버튼 클릭
			driver.findElement(By.id("holiDayModifyBtn")).click();
			
			Thread.sleep(5000);
			
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 수정한 휴일이 \"(.*)\" \"(.*)\" \"(.*)\" 있어야 함$")
	public void checkEditHoliDayModify(String day, String description, String use) throws Exception {

		Thread.sleep(1000);
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[1]"), 20);
		System.out.println("4-3day---->> " + day);
		String checkDay = webElement.getText();
		System.out.println("4-3checkDay---->> " + checkDay);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[2]"), 20);
		System.out.println("4-3description---->> " + description);
		String checkDes = webElement2.getText();
		System.out.println("4-3checkDes---->> " + checkDes);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[4]"), 20);
		System.out.println("4-3use---->> " + use);
		String checkUse = webElement3.getText();
		System.out.println("4-3checkUse---->> " + checkUse);
		
		Thread.sleep(5000);
		if(day.equals(checkDay) && description.equals(checkDes) && use.equals(checkUse)) {
			Thread.sleep(2000);
			
			assertTrue(true);
		}
		else {
			assertTrue(false);
		}
	}

	@Given("^휴일 상세보기 \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" 확인$")
	public void openHoliDayRemoveModal(String day, String description, String use, String modalUse) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1000);
			//첫번째 리스트 데이터 확인
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[1]"), 20);
			System.out.println("5-1day---->> " + day);
			String checkDay = webElement.getText();
			System.out.println("5-1checkDay---->> " + checkDay);
			
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[2]"), 20);
			System.out.println("5-1description---->> " + description);
			String checkDes = webElement2.getText();
			System.out.println("5-1checkDes---->> " + checkDes);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[4]"), 20);
			System.out.println("5-1use---->> " + use);
			String checkUse = webElement3.getText();
			System.out.println("5-1checkUse---->> " + checkUse);
			
			if(day.equals(checkDay) && description.equals(checkDes) && use.equals(checkUse)) {
				Thread.sleep(5000);
				
				assertTrue(true);
				//첫번째 리스트 클릭
				driver.findElement(By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]")).click();
				Thread.sleep(1500);
				
				checkDay = driver.findElement(By.id("dayHoliDayModal")).getAttribute("value");
						
				System.out.println("5-1day modal---->> " + day);
				System.out.println("5-1checkDay modal---->> " + checkDay);
				Thread.sleep(1000);
				checkUse = driver.findElement(By.id("useHoliDayModal")).getText();
				System.out.println("5-1modalUse modal---->> " + modalUse);
				System.out.println("5-1checkUse modal---->> " + checkUse);
				checkDes = driver.findElement(By.id("desHoliDayModal")).getText();
				System.out.println("5-1modalDes modal---->> " + description);
				System.out.println("5-1checkDes modal---->> " + checkDes);
				
				if(day.equals(checkDay) && description.equals(checkDes) && modalUse.equals(checkUse)) {
					Thread.sleep(2000);
					assertTrue(true);
				}
				else {
					assertTrue(false);
				}
			}
			else {
				assertTrue(false);
			}
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^휴일 삭제하기$")
	public void removeHoliDayModal() throws Exception {
		
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver,
					By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div/div/div/div/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			
			Thread.sleep(1000);
			//삭제 버튼 클릭
			driver.findElement(By.id("holiDayRemoveBtn")).click();
			Thread.sleep(3000);
			//Alert alert = driver.switchTo().alert();
			//String alertMsg = alert.getText();
			//System.out.println("alertMsg---->> " + alertMsg);
			//Thread.sleep(3000);
			//삭제확인 메세지가 맞는지 확인
			//if(!message.equals(alertMsg)) {
			//	Thread.sleep(2000);
			//	assertTrue(false);
			//}
			
			//alert.accept();
			Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트에 삭제한 휴일 \"(.*)\" \"(.*)\" \"(.*)\" 없어야 함$")
	public void checkRemoveHoliDay(String day, String description, String use) throws Exception {

		//첫번째 리스트 데이터 확인
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[1]"), 20);
		System.out.println("5-3day---->> " + day);
		String checkDay = webElement.getText();
		System.out.println("5-3checkDay---->> " + checkDay);
		
		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[2]"), 20);
		System.out.println("5-3description---->> " + description);
		String checkDes = webElement2.getText();
		System.out.println("5-3checkDes---->> " + checkDes);
		
		WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver,
				By.xpath("//*[@id=\"holidayTable\"]/tbody/tr[1]/td[4]"), 20);
		System.out.println("5-3use---->> " + use);
		String checkUse = webElement3.getText();
		System.out.println("5-3checkUse---->> " + checkUse);
		
		Thread.sleep(5000);
		if(day.equals(checkDay) && description.equals(checkDes) && use.equals(checkUse)) {
			Thread.sleep(2000);
			assertTrue(false);
		}
		else {
			Thread.sleep(2000);
			assertTrue(true);
		}
	}

	@Given("^출퇴근 리스트 조회 화면$")
	public void openCommuteListPage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/div[2]/div/div/div[1]/h1"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/div[2]/div/div/div[1]/h1"), 20);
			assertEquals("지각", webElement.getText());
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^\"(.*)\" 사원 검색하기$")
	public void searchCommute(String name) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div[2]/div/div[2]/div[2]/div[5]")).click(); // 시작 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/div/div[2]/div/div[2]/div[2]/div[6]")).click(); // 끝 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.id("listSearch")).clear();
			driver.findElement(By.id("listSearch")).sendKeys(name);
			
			Thread.sleep(1000);
			driver.findElement(By.id("employee")).click();
			
			//Thread.sleep(1000);
			//driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[5]/button")).click();
			
			Thread.sleep(5000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트 정보가 정상적으로 되었다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시되어야함$")
	public void checkSearchCommute(String userNo, String name) throws Exception {
		WebElement webElementUserNo = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"), 20);
		WebElement webElementName = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"), 20);
		
		String contentsUserNo = webElementUserNo.getText();
		String contentsName = webElementName.getText();
		
		System.out.println("contentsUserNo=" + contentsUserNo);
		System.out.println("contentsName=" + contentsName);
		
		if(!userNo.equals(contentsUserNo)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"));
					
			contentsUserNo = webElement2.getText();			
		}
		
		if(!name.equals(contentsName)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"));
					
			contentsName = webElement2.getText();			
		}
		
		assertEquals(userNo, contentsUserNo);
		assertEquals(name, contentsName);
	}
	
	@And("^출퇴근 리스트 데이터 클릭하기$")
	public void showModal() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement listData = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", listData);
		} else {
			// IE에서는 동작하지 않아서 수정됨

			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]")).click();			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^모달이 정상적으로 띄워졌다는 표시로 \"(.*)\" 정보가 표시되어야함$")
	public void checkModal(String info) throws Exception {
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[4]/div/div/div[1]/div"), 20);
		
		Thread.sleep(1000);
		
		String contents = webElement.getText();
		System.out.println("contents=" + contents);
		
		if(!info.equals(contents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[4]/div/div/div[1]/div"));
					
			contents = webElement2.getText();
		}
		
		assertEquals(info, contents);
	}
	
	@And("^수정 버튼 클릭하기$")
	public void clickModifyBtn() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"), 20);
			btnModify.click();
			
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[4]/div/div/div[1]/div"), 20);
			
			Thread.sleep(1000);
			
			String contents = webElement.getText();
			System.out.println("contents=" + contents);
			
			if(!"수정하기".equals(contents)) {
				Thread.sleep(2000);

				WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[4]/div/div/div[1]/div"));
						
				contents = webElement2.getText();
			}
			
			assertEquals("수정하기", contents);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^수정 데이터 설정하기$")
	public void modifyCommute() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			Thread.sleep(2000);
			driver.findElement(By.id("commuteToTime")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1) > ul > li:nth-child(21)")).click(); // 퇴근 시간
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(10) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2) > ul > li:nth-child(1)")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.id("commuteFromTime")).click();//By.cssSelector("body > div.fade.modal.show > div > div > div.modal-body > div:nth-child(1) > div:nth-child(2) > div.col-4 > span > input")).click();
		
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1)")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(1) > ul > li:nth-child(9)")).click(); // 출근 시간
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2)")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div:nth-child(11) > div > div > div > div.ant-time-picker-panel-combobox > div:nth-child(2) > ul > li:nth-child(31)")).click(); // 출근 분
			
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.id("btnModify")).click();
			
			if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
				Thread.sleep(1500);
			} else {
				SeleniumUtil.waitForJStoLoad(driver);
			}
		}
	}
	
	
	@And("^수정이 정상적으로 이루어졌다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시되어야함$")
	public void checkModify(String from, String to) throws Exception {
		Thread.sleep(3000);
		
		WebElement fromWebElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]"), 20);
		WebElement toWebElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]/td[5]"), 20);
		
		Thread.sleep(1000);
		
		String fromContents = fromWebElement.getText();
		String toContents = toWebElement.getText();
		
		
		if(fromContents.length() > 10) {
			fromContents = fromContents.substring(11, 16);
		}
		
		if(toContents.length() > 10) {
			toContents = toContents.substring(11, 16);
		}
		
		System.out.println("fromContents=" + fromContents);
		System.out.println("toContents=" + toContents);
		
		if(!from.equals(fromContents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\\\"root\\\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]"));
					
			fromContents = webElement2.getText();
			
			if(fromContents.length() > 10) {
				fromContents = fromContents.substring(11, 16);
			}
		}
		
		if(!to.equals(toContents)) {
			Thread.sleep(1000);
			
			WebElement webElement3 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\\\"root\\\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[1]/td[5]"));
			
			toContents = webElement3.getText();
			
			if(toContents.length() > 10) {
				toContents = toContents.substring(11, 16);
			}
		}
		
		assertEquals(from, fromContents);
		assertEquals(to, toContents);
	}
	
	@Given("^출퇴근 \"(.*)\" 모달 화면$")
	public void openCommuteModalPage(String title) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement modal = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[2]"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", modal);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[3]/div/div/div/div[2]/div/table/tbody/tr[2]"), 20);
			webElement.click();
			
			Thread.sleep(1000);
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[5]/div/div/div[1]/div"), 20);
			
			assertEquals(title, webElement2.getText());
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	

	@When("^해당 데이터 삭제하기$")
	public void deleteCommute() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement delete = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnRemove"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", delete);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.id("btnRemove")).click();
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^출퇴근 데이터의 삭제 여부 확인하기$")
	public void checkDeleteCommute() throws Exception {
		Thread.sleep(3000);
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteRecord"), 20);
		List<WebElement> records = webElement.findElements(By.tagName("tr"));
		
		System.out.println("size=" + records.size());
		
		assertTrue(records.size() < 2);
	}
	
	@Given("^근태 달력 조회화면$")
	public void openWorkAttitudePage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement menu = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[2]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", menu);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[2]")).click();
			
			SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/div[1]/div/div/div[1]/h1"), 20);
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[2]/div/label[2]")).click();
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^근태 검색 - \"(.*)\" 사원 검색하기$")
	public void searchWorkAttitude(String name) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div[2]/div/div[2]/div[2]/div[2]")).click(); // 검색 달
						
			Thread.sleep(1000);
			driver.findElement(By.id("calendarSearch")).clear();
			driver.findElement(By.id("calendarSearch")).sendKeys(name);
			
			Thread.sleep(1000);
			driver.findElement(By.id("calendarEmployee")).click();
			
			Thread.sleep(5000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^근태 달력 정보가 정상적으로 되었다는 표시로 \"(.*)\" 정보가 표시되어야함$")
	public void checkSearchWorkAttitude(String info) throws Exception {
		Thread.sleep(1000);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[4]/div[2]/table/tbody/tr/td[2]/a/div"), 20);
		
		String contents = webElement.getText();
		
		if(!info.equals(contents)) {
			Thread.sleep(1000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[4]/div[2]/table/tbody/tr/td[2]/a/div"));
					
			contents = webElement2.getText();			
		}
		
		System.out.println("contents=" + contents);
		
		assertEquals(info, contents);
	}
	
	@And("^근태 달력 데이터 클릭하기$")
	public void clickCalendarWorkAttitudeData() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement data = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[3]/a/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", data);
		} else {
			// IE에서는 동작하지 않아서 수정됨
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[3]/a/div")).click();			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^모달이 정상적으로 띄워졌다는 표시로 \"(.*)\"가 표시되어야함$")
	public void checkWorkAttitudeModal(String info) throws Exception {
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div"), 20);
		
		Thread.sleep(1000);
		
		String contents = webElement.getText();
		System.out.println("contents=" + contents);
		
		if(!info.equals(contents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div"));
					
			contents = webElement2.getText();
		}
		
		assertEquals(info, contents);
	}
	
	@And("^근태 수정 버튼 클릭하기$")
	public void clickWorkAttitudeModifyBtn() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"));//"/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"), 20);
			btnModify.click();
			
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div"), 20);
			
			Thread.sleep(1000);
			
			String contents = webElement.getText();
			
			if(!"수정하기".equals(contents)) {
				Thread.sleep(2000);

				WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div"));
						
				contents = webElement2.getText();
			}

			System.out.println("contents=" + contents);
			assertEquals("수정하기", contents);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^근태 수정 데이터 설정하기 - \"(.*)\", \"(.*)\"$")
	public void modifyWorkAttitude(String title, String content) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement btnModify = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnModify"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			driver.findElement(By.id("title")).clear();
			driver.findElement(By.id("title")).sendKeys(title);
			
			driver.findElement(By.id("selectMenu")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("annual")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("content")).clear();
			driver.findElement(By.id("content")).sendKeys(content);
			
			Thread.sleep(1000);
			driver.findElement(By.id("btnModify")).click();
			
			if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
				Thread.sleep(1500);
			} else {
				SeleniumUtil.waitForJStoLoad(driver);
			}
		}
	}
	
	@And("^근태 수정이 정상적으로 이루어졌다는 표시로 \"(.*)\" 정보가 표시되어야함$")
	public void checkWorkAttitudeModify(String info) throws Exception {
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[3]/a/div"), 20);
		WebElement alert = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[4]/div"), 20);
		
		Thread.sleep(1500);
		
		String contents = webElement.getText();
		System.out.println(alert.getText());
		
		if(!info.equals(contents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[3]/a/div"));
					
			contents = webElement2.getText();
		}
		
		System.out.println("contents=" + contents);
		
		assertEquals(info, contents);
	}
	
	@Given("^근태 \"(.*)\" 모달 화면$")
	public void openWorkAttitudeModalPage(String title) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement modal = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[5]/div[2]/table/tbody/tr/td[4]/a/div"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", modal);
		} else {
			//IE에서는 동작하지 않아서 수정됨
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[5]/div[2]/table/tbody/tr/td[4]/a/div"), 20);
			webElement.click();
			
			Thread.sleep(1000);
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.cssSelector("body > div.fade.modal.show > div > div > div.modal-header > div"), 20);
			
			assertEquals(title, webElement2.getText());
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^삭제 버튼 클릭하기$")
	public void deleteWorkAttitude() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement delete = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnDelete"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", delete);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.id("btnDelete")).click();
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^해당 날짜의 정보가 삭제되었는지 확인$")
	public void checkDeleteWorkAttitude() throws Exception {
		Thread.sleep(4000);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[5]/div[2]/table/tbody/tr/td[4]"), 20);
		
		String contents = webElement.getText();
		
		System.out.println("contents=" + contents);
		
		assertEquals("", contents);
	}
}
