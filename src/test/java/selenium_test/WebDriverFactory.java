package selenium_test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverFactory {
	public static WebDriver createWebDriver(String webDriver) {
		//String webDriver = System.getProperty("brower","firefox");
		
		switch(webDriver) {
			case "firefox":
				return new FirefoxDriver();
			case "chrome":
				return new ChromeDriver();
			default:
				throw new RuntimeException("UnSupported WebDriver : " + webDriver);
				
		}
		
	}
}
