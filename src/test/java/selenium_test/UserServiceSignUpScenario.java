//package selenium_test;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertNotEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertNull;
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.util.Iterator;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.log4j.Logger;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.NoSuchWindowException;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.ie.InternetExplorerOptions;
//import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//import cucumber.api.Scenario;
//import cucumber.api.java.After;
//import cucumber.api.java.Before;
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import ru.yandex.qatools.ashot.comparison.ImageDiff;
//import org.openqa.selenium.JavascriptExecutor;
//
//
//public class UserServiceSignUpScenario {
//
//	private static final String DEFAULT_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/"; 
//	
//	private static final String DEFAULT_SUCCESS_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/0_경비청구-개인카드-카드사용자-카드사용내역 접근하기/"; 
//
//	
//	private static final String BASE_URL = UserServiceSignUpTest.BASE_URL_DEV;
//	private static final String MAIN_WINDOW_TITLE = "위하고";
//	
//	private static final String FULL_SCREEN_YN = "Y";
//	
//	private static final String BROWSER_TYPE_IE = "webdriver.ie.driver";
//	private static final String BROWSER_TYPE_IE_DRIVER = "C:\\selenium_test\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe";
//	
//	private static final String BROWSER_TYPE_CHROME = "webdriver.chrome.driver";
//	private static final String BROWSER_TYPE_CHROME_DRIVER = "/Users/PSH/Desktop/chromedriver_win32/chromedriver.exe";
//	//private static final String BROWSER_TYPE_CHROME_DRIVER = "C:\\selenium_test\\chromedriver_win32\\chromedriver.exe";
//	
//
////	private static final String BROWSER_TYPE = "webdriver.ie.driver";
//	private static final String BROWSER_TYPE = "webdriver.chrome.driver";
//	
//	
//	private static WebDriver driver;
//
//	//@FirstScenario 라는 TAG가 있는 시나리오에서만 실행은 한다.
//	@Before("@FirstScenario")
//	public void setUp(Scenario scenario) {
//		
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_CHROME)) {
//	    	System.setProperty("webdriver.chrome.driver", BROWSER_TYPE_CHROME_DRIVER);
//			driver = new ChromeDriver();
//
//		} else if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			
//			//https://stackoverflow.com/questions/27985300/selenium-webdriver-typing-very-slow-in-text-field-on-ie-browser
//			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//	    	capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true); 
//			//capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);	
//			//capabilities.setCapability("requireWindowFocus", true);
//			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
//
//			InternetExplorerOptions options = new InternetExplorerOptions(capabilities);
//			
//			System.setProperty("webdriver.ie.driver", BROWSER_TYPE_IE_DRIVER);
//	    	driver = new InternetExplorerDriver(options);
//
//		}
//
//		// @After 대신에 addShutdownHook 을 이용해서 tearDown()을 실행함
//		Runtime.getRuntime().addShutdownHook(new Thread() {
//			public void run() {
//				// 이 부분은 실행이 되지만 EclEmma(JaCoCo) 라는 
//				// Java Code Coverage Tool에서는 실행이 안 된다고 나옴.
//				System.out.println("########## close #########");
//				if (driver != null) {
//					driver.quit();
//				}
//			}
//		});
//	}
//	
//	@Given("^로그인 화면$")
//	public void openLoginPage()  throws Exception {
//		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//		
//		if("Y".equals(FULL_SCREEN_YN)) {
//			driver.manage().window().fullscreen();
//		}
//	
//		driver.get(BASE_URL + "/#/login/");
//		
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			Thread.sleep(1000);
//		} else {
//			SeleniumUtil.waitForJStoLoad(driver);	
//		}
//	}
//
//	@When("^\"(.*)\" \"(.*)\" 을 입력하고 로그인버튼 클릭하기$")
//	public void Login(String id, String password)
//			throws Exception {
//
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {	
//			WebElement inputId = SeleniumUtil.waitForElementToBePresent(driver, By.id("inputId"));
//
//			inputId.clear();
//			inputId.sendKeys(id);
//
//			WebElement inputPw = SeleniumUtil.waitForElementToBePresent(driver, By.id("inputPw"));
//			
//			inputPw.clear();
//			inputPw.sendKeys(password);		
//		} else {
//			driver.findElement(By.id("inputId")).clear();
//			driver.findElement(By.id("inputId")).sendKeys(id);
//			driver.findElement(By.id("inputPw")).clear();
//			driver.findElement(By.id("inputPw")).sendKeys(password);
//		}
//		
//		
//		//로그인 버튼
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, 
//					By.xpath("/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button"));
//			JavascriptExecutor executor = (JavascriptExecutor)driver;
//			executor.executeScript("arguments[0].click();", login);
//		} else {
//			driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div[2]/div/div[1]/div/fieldset/button")).click();
//			//IE에서는 동작하지 않아서 수정됨 
//		}
//		
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			Thread.sleep(1000);
//		} else {
//			SeleniumUtil.waitForJStoLoad(driver);
//		}
//		
//	}
//
//	//로그인 된 화면이 나와야 함 
//	@Then("^로그인이 정상적으로 되었다는 표시로 \"(.*)\" 프로파일버튼이 출력되어야 함$")
//	public void checkLogInResult0(String name) throws Exception {
//
////		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
////				By.xpath("html/body/div[1]/div/div[1]/div[1]/div[2]/div[1]/div[2]/div/div[6]/div[1]/button"));
//
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.cssSelector(".btn.btn_userprofile"), 20);
//				
//		Thread.sleep(1000);
//		
//		String contents = webElement.getText();
//
//		System.out.println("contents=" + contents);
//		
//		if (contents.length() > 3) { //이름만 비교하기 위해서 
//			contents = contents.substring(0,3);
//		}
//		
//		if(!name.equals(contents)) {
//			Thread.sleep(2000);
////			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
////					By.xpath("html/body/div[1]/div/div[1]/div[1]/div[2]/div[1]/div[2]/div/div[6]/div[1]/button"));
//
//			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
//				By.cssSelector(".btn.btn_userprofile"));
//					
//			contents = webElement2.getText();
//			if (contents.length() > 3) { //이름만 비교하기 위해서 
//				contents = contents.substring(0,3);
//			}			
//		}
//		
//		//assertEquals(name, contents);
//
//	}
//	
//	
//	@Given("^경비청구-개인카드 화면$")
//	public void openPersonalCardPage() throws Exception {
//		driver.get(BASE_URL + "/#/expensepersonalcard/");
//		//SeleniumUtil.waitForJStoLoad(driver);
//		Thread.sleep(2000);
//		
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			Thread.sleep(1000);
//		}
//		
//	}	
//
//	@When("^카드사용내역 메뉴 클릭하기$")
//	public void openCardUsageStatement()
//			throws Exception {
//		SeleniumUtil.clickSubMenu(driver,"경비청구", "카드사용내역");
//		//SeleniumUtil.waitForJStoLoad(driver);
//		Thread.sleep(1000);
//		
//		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			Thread.sleep(1000);
//		}
//		
//	}
//	
//	
//	//등록된 카드 리스트가 나와야 함
//	@Then("^등록된 카드 리스트가 나와야 함$")
//	public void checkCardListResult0() throws Exception {
//
//		WebElement webElement0 = SeleniumUtil.waitForElementToBePresent(driver,
//				By.id("personalCardList0"));		
//		
//		WebElement webElement1 = SeleniumUtil.waitForElementToBePresent(driver,
//				By.id("personalCardList1"));
//		
//		WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
//				By.id("personalCardList2"));		
//		
//		//WebElement webElement3 = driver.findElement(By.id("personalCardList3"));	
//		
//		assertNotNull(webElement0);
//		assertNotNull(webElement1);
//		assertNotNull(webElement2);
//		
//		Thread.sleep(1000);
//		
////		WebElement cardList = SeleniumUtil.waitForElementToBePresent(driver,
////		By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[7]/div[1]"));
//
//		
//		//String actualImageFileName = SeleniumUtil.captureScreen(driver, DEFAULT_SCREENSHOT_FOLDER, "등록된 카드 리스트가 나와야 함"); 
//
//		//해당 컴포넌트만 비교하기 
////		String actualImageFileName = SeleniumUtil.captureScreenWebElement(driver, webElement0, DEFAULT_SCREENSHOT_FOLDER, "등록된 카드 리스트가 나와야 함");
////		boolean hasDiff1 = SeleniumUtil.compare2Images(DEFAULT_SUCCESS_SCREENSHOT_FOLDER + "등록된 카드 리스트가 나와야 함.jpeg",actualImageFileName);
////		boolean hasDiff2 = SeleniumUtil.compare2Images(DEFAULT_SUCCESS_SCREENSHOT_FOLDER + "등록된 카드 리스트가 나와야 함2.jpeg",actualImageFileName);
////		boolean hasDiff = hasDiff1 && hasDiff2;
//		//하나라도 같으면 Okay, 다 차이가 나면 False
//		
////		assertFalse(hasDiff);
//		
//		
//		
//	}
//	
//	
//	@Given("^개인카드-개인사용자-카드사용내역 화면$")
//	public void openCardUsageStatement2() throws Exception {
//		
//		System.out.println(driver.getCurrentUrl());
//		
//		if(driver.getCurrentUrl().equals(BASE_URL + "/#/expensepersonalcard/cardusagestatement")) {
//			return;
//		} else {
//			SeleniumUtil.clickSubMenu(driver,"경비청구", "카드사용내역");
//			SeleniumUtil.waitForJStoLoad(driver);
//		}
//	}		
//	
//	
//	@When("^모든 카드에 대하여 사용내역업데이트 하기$")
//	public void requestCardScrapping()
//			throws Exception {
//		
//		//첫번째 카드 선택하기 
//		SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[7]/div[1]/div[4]/a")).click();
//		
//		//사용내역업데이트 클릭하기
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[6]/div/button[1]"));
//		
//		System.out.println(webElement.getText());
//		System.out.println(webElement.getText().substring(0, 7));
//		//업데이트 가능시간 --:--)
//		if("업데이트 가능".equals(webElement.getText().substring(0, 7))) {
//			SeleniumUtil.waitFor(driver,
//					By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[6]/div/button[1]"), "사용내역업데이트", 180 );
//		}
//		webElement.click();
//	}	
//	
//	@When("^첫번째 카드에 대하여 사용내역업데이트 하기$")
//	public void requestCardScrapping0()
//			throws Exception {
//		
//		//첫번째 카드 선택하기 
//		SeleniumUtil.waitForElementToBePresent(driver,
//				By.id("personalCardList0")).findElement(By.tagName("a")).click();
//		
//		//사용내역업데이트 클릭하기
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[6]/div/button[1]"));
//		
//		System.out.println(webElement.getText());
//		System.out.println(webElement.getText().substring(0, 7));
//		//업데이트 가능시간 --:--)
//		if("업데이트 가능".equals(webElement.getText().substring(0, 7))) {
//			SeleniumUtil.waitFor(driver,
//					By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[6]/div/button[1]"), "사용내역업데이트", 180 );
//		}
//		webElement.click();
//	}
//
//	
//	@When("^카드사용내역이 나와야 함$")
//	public void checkCardUsageStatement0()
//			throws Exception {
//
//		//사용내역을 업데이트 중입니다.
//		String updating = "사용내역을 업데이트 중입니다.";
//		String xPath = "/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[2]/div[2]/div/div/div[2]/div"; 
//		
//		String alreadyUpdated = "최근 사용내역 업데이트 이력이 존재합니다."; 
//		// 최근 사용내역 업데이트 이력이 존재합니다. 최대 3분 이후 재시도 가능합니다.
//		String xPath2 = "/html/body/div[1]/div/div[1]/div[2]/div[2]/div/div/div[1]/div/div";
//		
//		WebElement webElement = null;
//		try {
//			//사용내역을 업데이트 중입니다.
//			webElement = SeleniumUtil.waitForElementToBePresent(driver,By.xpath(xPath), 2);
//			
//			System.out.println(webElement);
//			
//			System.out.println(SeleniumUtil.getElementText(driver, 
//					driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[2]"))));
//		
//		} catch(Exception e) {
//			
//			assertNotNull(webElement);
//			
//			// 사용내역업데이트 실패
//			// <h1 style="margin-bottom: 10px;">사용내역업데이트 실패</h1>
//			// /html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[1]/h1	
//			
//			webElement = driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[1]/h1"));
//			
//			if("사용내역업데이트 실패".equals(webElement.getText())) {
//				fail("사용내역업데이트 실패");
//				return;
//			}
//			
//			return;
//			
//		}
//		
//		
//		String contents = webElement.getText();
//		System.out.println("contents=" + contents);
//		//"사용내역을 업데이트 중입니다."
//		if(updating.equals(contents.substring(0,updating.length()))) {
//			
//			//String xpath = "//*[name()='svg']//*[name()='circle']";
//			
//			SeleniumUtil.waitForNotFound(driver,By.xpath(xPath), 60 );
//			
//			// 카드사용내역 
//
////			String actualImageFileName = SeleniumUtil.captureScreen(driver, DEFAULT_SCREENSHOT_FOLDER, "카드사용내역"); 
////			boolean hasDiff = SeleniumUtil.compare2Images(DEFAULT_SCREENSHOT_FOLDER + "카드사용내역.jpeg",actualImageFileName);
//			//assertFalse(hasDiff);		
//			
//			WebElement webElement0 = SeleniumUtil.waitForElementToBePresent(driver,
//					By.id("gridCheckBox"));
//			
//			
//			assertNotNull(webElement0);
//	
//		}
//	
//	}	
//	
//	
//	//사용내역업데이트
//	@And("^\\[사용내역업데이트 실패\\]라는 메시지가 출력되면 안 됨$")
//	public void checkCardUsageStatement1() throws Exception {
//
//		WebElement element = null;
//		
//		try {
//			element = SeleniumUtil.waitForElementToBePresent(
//				driver,By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[2]/div/div/div[1]/div/div[2]/div/div/div/div/div[1]/h1"),1);
//		
//		} catch(Exception e) {
//			assertNull(element);
//			return;
//		}
//		
//		if(element != null) {
//			assertNotEquals("사용내역업데이트 실패!", element.getText());	
//			
//			SeleniumUtil.captureScreen(driver, DEFAULT_SCREENSHOT_FOLDER, "사용내역업데이트 실패"); 
//			
//		}
//
//		assertNull(element);
//
//	}
//
//	
////Scenario: 개인카드-개인사용자-카드사용내역-경비청구관리전송 	
////	  Given 개인카드-개인사용자-카드사용내역 화면
////	  When 카드사용내역에서 첫번째, 두번째 사용내역의 체크박스를 클릭한다.
////	  And [경비청구관리 전송] 버튼을 클릭한다.
////	  Then 카드사용내역화면에서 해당 사용내역이 안 보여야 한다.
////	  And 경비청구관리 메뉴에서 해당 사용내역이 보여야 한다.	
//	
//	
//	@When("^카드사용내역에서 첫번째, 두번째 사용내역의 체크박스를 클릭한다.$")
//	public void check12InList0()
//			throws Exception {
//
//		//처음 사용내역 캔버스 
////		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
////				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[7]/div[2]/div[2]/div/div[1]/div/canvas"));			
//
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.tagName("canvas"));			
//		
//		
//		// 처음의 사용내역 캔버스를 그림으로 저장해 놓고 나중에 비교한다  
//		String actualImageFileName = SeleniumUtil.captureScreenshotCanvas(driver, DEFAULT_SCREENSHOT_FOLDER, "사용내역리스트처음"); 
//
//		System.out.println(actualImageFileName);
//		
//		Thread.sleep(2000);
//		
//		
//		//첫번째 사용내역의 체크박스를 클릭한다
//		//Canves
//		Actions clickAt = new Actions(driver);
//		clickAt.moveToElement(webElement, 15, 60).click();
//		Thread.sleep(1000);
////		clickAt.moveToElement(webElement, 15, 90).click();
//		clickAt.build().perform();	
//		Thread.sleep(1000);
////		Thread.sleep(1000);
////		Thread.sleep(1000);
//		
//	}	
//	
//	@And("^\\[경비청구관리 전송\\] 버튼을 클릭한다.$")
//	public void clickButton1()
//			throws Exception {
//
//		//And [경비청구관리 전송] 버튼을 클릭한다.
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[2]/div[2]/div[1]/div/button[1]"));		
//		
//		webElement.click();
//	}	
//	
//	
//	@And("^귀속월을 선택한다.$")	
//	public void clickButton2()
//			throws Exception {
//
//		//And [경비청구관리 전송] 버튼을 클릭한다.
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[11]/div/div[2]/div/div/div[2]/button[2]"));		
//		
//		webElement.click();
//		Thread.sleep(1000);
//	}	
//	
//	@Then("^\\[경비청구 전송완료\\] 팝업이 보여야 한다.$")
//	public void showPopup0()
//			throws Exception {
//
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[6]/div[2]/div/div/div[1]/h1"));		
//
//		if("경비청구 전송완료".equals(webElement.getText())) {
//			WebElement btn = SeleniumUtil.waitForElementToBePresent(driver,
//					By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[6]/div[2]/div/div/div[2]/button[1]"));				
//			btn.click(); //페이지 유지
//			Thread.sleep(1000);
//			assertTrue(true);
//		} else {
//			fail("경비청구 전송완료 실패");
//			Thread.sleep(1000);
//		}
//	}	
//	
//	// TODO 캠퍼스에서 데이타 비교하기 ?
//	//https://stackoverflow.com/questions/43609429/how-to-get-text-inside-a-canvas-using-webdriver-or-protractor/43612408
//	//Canvas 로는 데이타 확인 불가 그래서 이미지로 비교해야 함 
//	@And("^카드사용내역화면에서 해당 사용내역이 안 보여야 한다.$")	
//	public void showStatement()
//			throws Exception {
//
//		//2번째 사용내역 캔버스 
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[2]/div[7]/div[2]/div[2]/div/div[1]/div/canvas"));			
//		
//		
//		String actualImageFileName = SeleniumUtil.captureScreenshotCanvas(driver, DEFAULT_SCREENSHOT_FOLDER, "사용내역리스트-경비청구관리 전송"); 
//		
//		Thread.sleep(1000);
//		boolean hasDiff = SeleniumUtil.compare2Images(DEFAULT_SUCCESS_SCREENSHOT_FOLDER + "사용내역리스트처음.jpeg",actualImageFileName);
//		
//		assertTrue(hasDiff);
//		
//	}		
//
//	//TODO 미작업됨 
//	@And("^경비청구관리 메뉴에서 해당 사용내역이 보여야 한다.$")	
//	public void showNotStatement()
//			throws Exception {
//
//		//And [경비청구관리 전송] 버튼을 클릭한다.
//		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
//				By.xpath("/html/body/div[1]/div/div[1]/div[1]/div[3]/div/div/div/div/div/div[11]/div/div[2]/div/div/div[2]/button[2]"));		
//
//		
//		
//		
//		webElement.click();
//	}		
//	
//	
//	  
//	
//	
//	
//	
//	
//	
//	
//	
//	
//
////  setUp(Scenario scenario) 에 있는
////	Runtime.getRuntime().addShutdownHook 로 대신 처리함
////	@After
////	public void tearDown() throws Exception {
////		if (driver != null)
////			driver.quit();
////	}
//}
