/**
 * SeleniumUtil.java        2015. 11. 16.
 *
 * ⓒ 2014 Accenture. All Rights Reserved.
 * The trademarks used in these materials are the properties of their respective owners.
 * This work is protected by copyright law and contains valuable trade secrets and 
 * confidential information.
 */
package selenium_test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.comparison.PointsMarkupPolicy;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class SeleniumUtil {
	
	private static final String DEFAULT_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/"; 	

	private static final long DEFAULT_DELAY_IN_MILLIS = 300;
	// WebDriverWait by default calls the ExpectedCondition every 50
	// milliseconds until it returns successfully

	// private static final String DEFAULT_TIMEOUT_IN_SECONDS = "60"; // One
	// Minute.
	private static final long DEFAULT_TIMEOUT_IN_SECONDS = 5; // 5 seconds.
	// timeOutInSeconds : The timeout in seconds when an expectation is called'
	

	public static WebElement waitForElementToBePresent(WebDriver driver,
			final By locator) {
		WebElement webElement = (new WebDriverWait(driver,
				DEFAULT_TIMEOUT_IN_SECONDS, DEFAULT_DELAY_IN_MILLIS)).until(
						ExpectedConditions.presenceOfElementLocated(locator));
		return webElement;
	}
	
	public static WebElement waitForElementToBePresent(WebDriver driver,
			final By locator, int maxTimeOutSecond) {
		WebElement webElement = (new WebDriverWait(driver,
				maxTimeOutSecond, DEFAULT_DELAY_IN_MILLIS)).until(
						ExpectedConditions.presenceOfElementLocated(locator));
		return webElement;
	}	

	public static WebElement waitForElementToBeClickable(WebDriver driver,
			final By locator) {
		WebElement webElement = (new WebDriverWait(driver,
				DEFAULT_TIMEOUT_IN_SECONDS, DEFAULT_DELAY_IN_MILLIS)).until(
						ExpectedConditions.elementToBeClickable(locator));
		return webElement;
	}

	//Page을 Loading할 때 Java Script까지 다 Loading될 때까지 기다리기
	public static boolean waitForJStoLoad(WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver,
				DEFAULT_TIMEOUT_IN_SECONDS, DEFAULT_DELAY_IN_MILLIS);

		// wait for jQuery to load
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				try {
					return ((Long)((JavascriptExecutor)driver)
							.executeScript("return jQuery.active") == 0);
				} catch (WebDriverException e) {
					return true;
				}
			}
		};

		// wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor)driver)
						.executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};

		return wait.until(jQueryLoad) && wait.until(jsLoad);
	}
	
	
	public static boolean waitFor(WebDriver driver, final By locator, String text, int maxTimeOutSecond) {

		WebDriverWait wait = new WebDriverWait(driver,
				maxTimeOutSecond, DEFAULT_DELAY_IN_MILLIS);

		// wait for jQuery to load
		ExpectedCondition<Boolean> needToWait = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				
				WebElement webElement = null;
				try {
					webElement = waitForElementToBePresent(driver, locator, 2);
					
					System.out.println(SeleniumUtil.getElementText(driver,webElement));
					 	
					return webElement.getText().equals(text) || (webElement == null);
					
					
				} catch (WebDriverException e) {
					e.printStackTrace();
					return true;
				}
			}
		};

		return wait.until(needToWait);
	}	
	
	
	public static boolean waitForNotFound(WebDriver driver, final By locator, int maxTimeOutSecond) {

		WebDriverWait wait = new WebDriverWait(driver,
				maxTimeOutSecond, DEFAULT_DELAY_IN_MILLIS);

		// wait for jQuery to load
		ExpectedCondition<Boolean> needToWait = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				
				WebElement webElement = null;
				try {
					webElement = waitForElementToBePresent(driver, locator, 2);
					
					System.out.println(SeleniumUtil.getElementText(driver,webElement));
					
					return (webElement == null);
					
					
				} catch (WebDriverException e) {
//					e.printStackTrace();
					return true;
				}
			}
		};

		return wait.until(needToWait);
	}	
		
	
	

	public static String getElementText(WebDriver driver, WebElement element) {
		return (String)((JavascriptExecutor)driver)
				.executeScript("return arguments[0].innerHTML;", element);
	}
	
	public static String getElementValue(WebDriver driver, WebElement element) {
		return (String)((JavascriptExecutor)driver)
				.executeScript("return arguments[0].value;", element);
	}

	public static boolean switchToWebDriverWindow(WebDriver driver,
			String windowTitle) {

		boolean isWindowChanged = false;
		
		try {
			if (driver.getTitle().equals(windowTitle)) {
				return isWindowChanged;
			}

		} catch (NoSuchWindowException e) {
			//No more current window in screen.
			//Current window will be changed in below routine.

		} finally {

			for (String windowHandle : driver.getWindowHandles()) {
				driver.switchTo().window(windowHandle);
				if (driver.getTitle().equals(windowTitle)) {
					isWindowChanged = true;
					return isWindowChanged;
				}
			}
		}
		return isWindowChanged;

	}
	
	public static void clickSubMenu(WebDriver driver, String mainMenu, String subMenu) throws Exception {
		Actions builder = new Actions(driver);
		
		
		builder.moveToElement(waitForElementToBeClickable(driver,By.linkText(mainMenu))).perform();	
		Thread.sleep(500);
		builder.moveToElement(waitForElementToBeClickable(driver,By.linkText(subMenu))).click().perform();
				
	}
	
	

    
	
    
	public static String captureScreenAllPage(WebDriver driver,String folderName, String fileName) {

		File f = null;	
		
		try {
			//http://www.softwaretestingmaterial.com/how-to-capture-full-page-screenshot-using-selenium-webdriver/
			
		    Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
			
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		      // extracting date for folder name.
            SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            Date now1 = new Date();
            String strDate1 = sdfDate1.format(now1);

            // extracting date and time for snapshot file
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//dd/MM/yyyy
            Date now = new Date();
            String strDate = sdfDate.format(now);

            String filefolder=folderName+strDate1+"/";  // create a folder as snap in  your project directory

            // Creating folders and files
            f = new File(filefolder + strDate + fileName + ".jpeg");	
 
            ImageIO.write(fpScreenshot.getImage(),"PNG",new File(f.getPath()));
               
            
            System.out.println(f.getPath());
			
			FileUtils.copyFile(scrFile, new File(f.getPath()));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f.getPath();		

	}    
	
	public static String captureScreenWebElement(WebDriver driver, WebElement element, String folderName, String fileName) {

		File f = null;		
		
		try {


		    Screenshot fpScreenshot = new AShot().coordsProvider(new WebDriverCoordsProvider()).takeScreenshot(driver, element);
//		    Screenshot fpScreenshot = new AShot().takeScreenshot(driver, element);
		    
			//File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		      // extracting date for folder name.
            SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            Date now1 = new Date();
            String strDate1 = sdfDate1.format(now1);

            // extracting date and time for snapshot file
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//dd/MM/yyyy
            Date now = new Date();
            String strDate = sdfDate.format(now);

            String filefolder=folderName+strDate1+"/";  // create a folder as snap in  your project directory

            // Creating folders and files
    
            f = new File(filefolder + strDate + fileName + ".jpeg");	
 
            ImageIO.write(fpScreenshot.getImage(),"PNG",new File(f.getPath()));
               
            System.out.println(f.getPath());
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f.getPath();


	}  
	
	public static String captureScreenshotCanvas(WebDriver driver, String folderName, String fileName){

	    String actualOutputImagePath=folderName + fileName;
	    WebElement canvasElement = driver.findElement(By.tagName("canvas"));
	    Screenshot screenshot = new AShot().coordsProvider(
	            new WebDriverCoordsProvider()).takeScreenshot(driver,canvasElement);
	    BufferedImage actualImage = screenshot.getImage();

	      // extracting date for folder name.
        SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Date now1 = new Date();
        String strDate1 = sdfDate1.format(now1);

        // extracting date and time for snapshot file
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);

        String filefolder=folderName+strDate1+"/";  // create a folder as snap in  your project directory
	    
        
        try {
			new File(filefolder).mkdir();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        // Creating folders and files
        File f = new File(filefolder + strDate + fileName + ".jpeg");		    
        System.out.println("captureScreenshotCanvas=" + filefolder + strDate + fileName + ".jpeg");
	    
	    try{
	        ImageIO.write(actualImage, "PNG", f);

	    }catch (Exception e){
	        System.out.println(e.getStackTrace());
	    }
	    
	    return f.getPath();
	    
	}	
	
	
	
	
	public static String captureScreen(WebDriver driver,String folderName, String fileName) {
		
		File scrFile = null;
		File file = null;
		
		try {
			
			scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		      // extracting date for folder name.
            SimpleDateFormat sdfDate1 = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
            Date now1 = new Date();
            String strDate1 = sdfDate1.format(now1);

            // extracting date and time for snapshot file
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");//dd/MM/yyyy
            Date now = new Date();
            String strDate = sdfDate.format(now);

            String filefolder= folderName + strDate1 +"/";  // create a folder as snap in  your project directory

            // Creating folders and files
            file = new File(filefolder + strDate + fileName + ".jpeg");	
            
            System.out.println(file.getPath());
			
			FileUtils.copyFile(scrFile, new File(file.getPath()));

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file.getPath();
	
	
	}
	
	public static boolean compare2Images(String expectedImageFileName, String actualImageFileName) {
		
		//https://github.com/pazone/ashot
		
		File expectedImageFile = new File(expectedImageFileName);
		File actualImageFile = new File(actualImageFileName);
		
		BufferedImage expectedImage = null, actualImage = null;
		try {
			expectedImage = ImageIO.read(expectedImageFile);
			actualImage = ImageIO.read(actualImageFile);			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	    ImageDiffer imageDiffer = new ImageDiffer();
	    ImageDiff diff = imageDiffer
	            .makeDiff(expectedImage, actualImage);

	    
	    if(diff.hasDiff()) {
	    	saveImage(diff.getDiffImage(), actualImageFileName);
	    }
	    
	    // areImagesDifferent will be true if images are different, false - images the same
	    return diff.hasDiff();
	}
	
	
	private static void saveImage(BufferedImage image, String imageName) {
	    // Path where you are going to save image
		
		String fileName =  imageName.substring(imageName.lastIndexOf(File.separator));
		
	    String outputFilePath = String.format(DEFAULT_SCREENSHOT_FOLDER + "%s" , fileName);
	    File outputFile = new File(outputFilePath);
	    try {
	        ImageIO.write(image, "png", outputFile);
	    } catch (IOException e) {
	        // Some code in case of failure
	    }
	}	
	
	


}
