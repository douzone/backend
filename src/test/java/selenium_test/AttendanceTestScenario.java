package selenium_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class AttendanceTestScenario {

	//private static final String DEFAULT_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/";

	//private static final String DEFAULT_SUCCESS_SCREENSHOT_FOLDER = "/Users/myoungil.lim/selenium/selenium_test/0_경비청구-개인카드-카드사용자-카드사용내역 접근하기/";

	private static final String BASE_URL = AttendenceTest.BASE_URL_DEV;
	//private static final String MAIN_WINDOW_TITLE = "위하고";

	private static final String FULL_SCREEN_YN = "Y";

	private static final String BROWSER_TYPE_IE = "webdriver.ie.driver";
	private static final String BROWSER_TYPE_IE_DRIVER = "C:\\selenium_test\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe";

	private static final String BROWSER_TYPE_CHROME = "webdriver.chrome.driver";
	private static final String BROWSER_TYPE_CHROME_DRIVER = "/Users/PSH/Desktop/chromedriver_win32/chromedriver.exe";// 성혜
	//private static final String BROWSER_TYPE_CHROME_DRIVER = "/Users/BIT/Downloads/chromedriver_win32/chromedriver.exe";// 영돈
	// https://sites.google.com/a/chromium.org/chromedriver/ 에서 최신의 크롬 드라이버를 설치하세요

	// https://www.seleniumhq.org/download/ 에서 각각의 브라우져 드라이버를 다운 받을 수 있습니다.

	// private static final String BROWSER_TYPE_CHROME_DRIVER =
	// "C:\\selenium_test\\chromedriver_win32\\chromedriver.exe";

//	private static final String BROWSER_TYPE = "webdriver.ie.driver";
	private static final String BROWSER_TYPE = "webdriver.chrome.driver";

	private static WebDriver driver;

	// @FirstScenario 라는 TAG가 있는 시나리오에서만 실행은 한다.
	@Before("@FirstScenario")
	public void setUp(Scenario scenario) {

		if (BROWSER_TYPE.equals(BROWSER_TYPE_CHROME)) {
			System.setProperty("webdriver.chrome.driver", BROWSER_TYPE_CHROME_DRIVER);
			
			driver = new ChromeDriver();
			
		} else if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {

			// https://stackoverflow.com/questions/27985300/selenium-webdriver-typing-very-slow-in-text-field-on-ie-browser
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			// capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			// capabilities.setCapability("requireWindowFocus", true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

			InternetExplorerOptions options = new InternetExplorerOptions(capabilities);

			System.setProperty("webdriver.ie.driver", BROWSER_TYPE_IE_DRIVER);
			driver = new InternetExplorerDriver(options);

		}

		// @After 대신에 addShutdownHook 을 이용해서 tearDown()을 실행함
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				// 이 부분은 실행이 되지만 EclEmma(JaCoCo) 라는
				// Java Code Coverage Tool에서는 실행이 안 된다고 나옴.
				System.out.println("########## close #########");
				if (driver != null) {
					driver.quit();
				}
			}
		});
	}
	

	@Given("^로그인 화면$")
	public void openLoginPage()  throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		if("Y".equals(FULL_SCREEN_YN)) {
			driver.manage().window().fullscreen();
		}
	
		driver.get(BASE_URL + "/login/");
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);	
		}
	}

	@When("^\"(.*)\" \"(.*)\" 을 입력하고 로그인버튼 클릭하기$")
	public void Login(String id, String password)
			throws Exception {

		System.out.println(id + " " + password);
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {	
			WebElement inputId = SeleniumUtil.waitForElementToBePresent(driver, By.id("email"));

			inputId.clear();
			inputId.sendKeys(id);

			WebElement inputPw = SeleniumUtil.waitForElementToBePresent(driver, By.id("password"));
			
			inputPw.clear();
			inputPw.sendKeys(password);
		} else {
			driver.findElement(By.id("email")).clear();
			driver.findElement(By.id("email")).sendKeys(id);
			
			Thread.sleep(1000);
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys(password);
		}
		
		
		//로그인 버튼
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnLogin"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", login);
		} else {
			driver.findElement(By.id("btnLogin")).click();
			//IE에서는 동작하지 않아서 수정됨 
			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}

	//로그인 된 화면이 나와야 함 
	@Then("^로그인이 정상적으로 되었다는 표시로 \"(.*)\" 사용자 정보가 출력되어야 함$")
	public void checkLogInResult0(String name) throws Exception {
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver,
				By.id("headerName"), 20);
				
		Thread.sleep(1000);
		
		String contents = webElement.getText();
		System.out.println("contents=" + contents);
		
		if (contents.length() > 3) { //이름만 비교하기 위해서 
			contents = contents.substring(0,3);
		}
		
		System.out.println(contents);
		
		if(!name.equals(contents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver,
					By.id("headerName"));
					
			contents = webElement2.getText();
			if (contents.length() > 3) { //이름만 비교하기 위해서 
				contents = contents.substring(0,3);
			}			
		}
		
		assertEquals(name, contents);
	}


	@Given("^출퇴근 신청 화면$")
	public void openCommutePage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]/span[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			Thread.sleep(1500);
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/div[2]/time"));
			
			String day = webElement.getText();
			String data = LocalDate.now().format(DateTimeFormatter.ofPattern("YYYY.MM.d"));
			
			System.out.println(day);
			System.out.println(data);
			
			assertEquals(data, day);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}

	@When("^출근 버튼 클릭하기$")
	public void goToCommute() throws Exception {
		
		// 출근 버튼
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement goTo = SeleniumUtil.waitForElementToBePresent(driver, By.id("btnGoTo"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", goTo);
		} else {
			Thread.sleep(4000);
			driver.findElement(By.id("btnGoTo")).click();
			// IE에서는 동작하지 않아서 수정됨
			Thread.sleep(2000);
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}

	// 출근 완료 정보가 표시되어야함
	@Then("^출근이 정상적으로 되었다는 표시로 \"(.*)\" 문구가 표시되어야함$")
	public void checkGoToCommute(String info) throws Exception {
		Thread.sleep(3000);
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteText"), 30);
				
		Thread.sleep(1000);
		
		String contents = webElement.getText();
		System.out.println("contents=" + contents);
		
		if(!info.equals(contents)) {
			Thread.sleep(2000);
			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteText"));
					
			contents = webElement2.getText();
		}
		
		assertEquals(info, contents);
	}
	
	@Given("^출퇴근 리스트 조회 화면 \"(.*)\" 문구가 떠야함$")
	public void openCommuteListPage(String info) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement login = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteTitle"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", login);
		} else {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteTitle"));
			
			String contents = webElement.getText();
			System.out.println(contents);
			
			if(!info.equals(contents)) {
				WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("commuteTitle"));
				
				contents = webElement2.getText();
			}
			
			assertEquals(info, contents);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^검색할 날짜를 선택한다.$")
	public void searchCommute() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[2]/table/thead/tr[1]/th[2]/div[1]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[2]/table/thead/tr[1]/th[2]/div[1]/div[2]/div/div[2]/div[4]/div[2]")).click(); // 시작 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[2]/table/thead/tr[1]/th[2]/div[3]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[2]/table/thead/tr[1]/th[2]/div[3]/div[2]/div/div[2]/div[4]/div[2]")).click(); // 끝 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.id("btnUserListSearch")).click();
			
			// IE에서는 동작하지 않아서 수정됨
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^리스트 정보가 정상적으로 되었다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시되어야함$")
	public void checkGoToCommute(String userNo, String name) throws Exception {

		WebElement webElementUserNo = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"), 20);
		WebElement webElementName = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"), 20);
		
		String contentsUserNo = webElementUserNo.getText();
		String contentsName = webElementName.getText();
		
		System.out.println("contentsUserNo=" + contentsUserNo);
		System.out.println("contentsName=" + contentsName);
		
		if(!userNo.equals(contentsUserNo)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"));
					
			contentsUserNo = webElement2.getText();			
		}
		
		if(!name.equals(contentsName)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"));
					
			contentsName = webElement2.getText();			
		}
		
		assertEquals(userNo, contentsUserNo);
		assertEquals(name, contentsName);
	}
	
	@And("^출퇴근 리스트 데이터 클릭하기$")
	public void showModal() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement listData = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"commuteRecord\"]/tr"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", listData);
		} else {
			// IE에서는 동작하지 않아서 수정됨

			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"commuteRecord\"]/tr")).click();			
		}
	}
	
	@And("^모달이 정상적으로 띄워졌다는 표시로 \"(.*)\" 정보가 표시되어야함$")
	public void checkModal(String info) throws Exception {
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[3]/div/div/div[1]/div"), 20);
		
		Thread.sleep(1000);
		
		String contents = webElement.getText();
		System.out.println("contents=" + contents);
		
		if (contents.length() > 5) { //이름만 비교하기 위해서 
			contents = contents.substring(0,5);
		}
		
		System.out.println(contents);
		
		if(!info.equals(contents)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[3]/div/div/div[1]/div"));
					
			contents = webElement2.getText();
		}
		
		Thread.sleep(2000);
	
		driver.findElement(By.id("btnClose")).click();
		
		assertEquals(info, contents);
	}
	
	@Given("^출퇴근 달력 조회 화면$")
	public void openCommuteCalendarPage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement radioCalendar = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[2]/div/span/div/label[2]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", radioCalendar);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[2]/div/span/div/label[2]")).click();
			//IE에서는 동작하지 않아서 수정됨 
			
			Thread.sleep(4000);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			
			Thread.sleep(2000);
			// 현재 날짜
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[4]/div[2]/table/tbody/tr/td[2]/a/div/span"));
			
			System.out.println(webElement.getText());
			assertNotNull(webElement.getText());
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^검색할 달을 선택한다.$")
	public void searchCommuteCalendar() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[1]/table/thead/tr[1]/th[2]/div/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(2000); 
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[1]/table/thead/tr[1]/th[2]/div/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[3]/div[1]/div[1]/table/thead/tr[1]/th[2]/div/div[2]/div/div[2]/div[2]/div[2]")).click(); // 달 선택
			
			Thread.sleep(1000);
			driver.findElement(By.id("btnUserCalendarSearch")).click();
		
			// IE에서는 동작하지 않아서 수정됨
		}
	}
	
	@Then("^달력 정보가 정상적으로 나타났다는 표시로 해당 날짜에 정보가 표시되어야함$")
	public void checkGoToCommuteCalendar() throws Exception {
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		Thread.sleep(2000);
		// 검색한 날짜의 데이터
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[4]/div[2]/table/tbody/tr/td[2]/a/div/span"), 20);

		System.out.println(webElement.getText());
		assertNotNull(webElement.getText());
	}
	
	@Given("^근태 신청 화면$")
	public void openWorkAttitudePage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[2]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", webElement);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[2]")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[2]")).click();
			//IE에서는 동작하지 않아서 수정됨 
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^\"(.*)\" \"(.*)\"을 입력하고 등록 버튼 클릭하기$")
	public void addWorkAttitude(String title, String content) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
//			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div/div/div"));
//			JavascriptExecutor executor = (JavascriptExecutor) driver;
//			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/table/thead/tr[2]/th[2]/div[1]/div/div")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/table/thead/tr[2]/th[2]/div[1]/div[2]/div/div[2]/div[3]/div[5]")).click(); // fromDate
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/table/thead/tr[2]/th[2]/div[3]/div/div")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/table/thead/tr[2]/th[2]/div[3]/div[2]/div/div[2]/div[3]/div[6]")).click(); // endDate
			
			Thread.sleep(2000);
			driver.findElement(By.id("title")).clear();
			driver.findElement(By.id("title")).sendKeys(title);
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/table/thead/tr[4]/th[2]/button")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("seminar")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.id("content")).clear();
			driver.findElement(By.id("content")).sendKeys(content);
			
			Thread.sleep(2000);
			driver.findElement(By.id("btnSubmit")).click();
		}
	}
	
	@Then("^신청이 정상적으로 되었다는 표시로 \"(.*)\" alert 창이 출력되어야함$")
	public void checkAddWorkAttitude(String text) throws Exception {
		//Thread.sleep(2000);
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("/html/body/div[2]/div"), 20);
		//new WebDriverWait(driver, 3).until(ExpectedConditions.alertIsPresent());
		///html/body/div[5]/div
		//Alert alert = driver.switchTo().alert();
		//System.out.println(alert.getText());
		//System.out.println(webElement.getAttribute("class"));
		System.out.println(webElement.getText());
		System.out.println(text);

		// assertEquals(text, webElement.getText());
		
		Thread.sleep(2000);
		//alert.accept();
	}
	
	@Given("^근태 리스트 조회 화면 \"(.*)\" 문구가 떠야함$")
	public void openWorkAttitudeListPage(String info) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[1]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", webElement);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/div/div/div/div/a[1]")).click();
			
			Thread.sleep(2000);
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.id("workAttitudeTitle"));
			
			String contents = webElement.getText();
			System.out.println(contents);
			
			if(!info.equals(contents)) {
				WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("workAttitudeTitle"));
				
				contents = webElement2.getText();
			}
			
			assertEquals(info, contents);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^검색할 근태의 날짜를 선택한다.$")
	public void searchWorkAttitude() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[1]/th[2]/div[1]/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[1]/th[2]/div[1]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[1]/th[2]/div[1]/div[2]/div/div[2]/div[1]/div[7]")).click(); // 시작 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[1]/th[2]/div[3]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[1]/th[2]/div[3]/div[2]/div/div[2]/div[6]/div[1]")).click(); // 끝 날짜
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/table/thead/tr[2]/th[2]/button")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("seminar")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("btnSearch")).click();
			
			// IE에서는 동작하지 않아서 수정됨
		}

		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^근태 리스트 정보가 정상적으로 되었다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시되어야함$")
	public void checkWorkAttitudeList(String userNo, String name) throws Exception {
		WebElement webElementUserNo = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"), 20);
		WebElement webElementName = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"), 20);

		String contentsUserNo = webElementUserNo.getText();
		String contentsName = webElementName.getText();
		
		System.out.println("contentsUserNo=" + contentsUserNo);
		System.out.println("contentsName=" + contentsName);
		
		if(!userNo.equals(contentsUserNo)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("userNo"));
					
			contentsUserNo = webElement2.getText();			
		}
		
		if(!name.equals(contentsName)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("name"));
					
			contentsName = webElement2.getText();			
		}
		
		assertEquals(userNo, contentsUserNo);
		assertEquals(name, contentsName);
	}
	
	@And("^근태 리스트 데이터 클릭하기$")
	public void showWorkAttitudeModal() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement listData = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"workAttitudeTable\"]/tbody/tr"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", listData);
		} else {
			// IE에서는 동작하지 않아서 수정됨

			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id=\"workAttitudeTable\"]/tbody/tr")).click();	
			
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.id("modal"), 20);
			
			Thread.sleep(1000);
			
			String contents = webElement.getText();
			System.out.println("contents=" + contents);
			
			if(!"상세보기".equals(contents)) {
				Thread.sleep(2000);

				WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("modal"));
						
				contents = webElement2.getText();
			}
			
			Thread.sleep(2000);
			
			driver.findElement(By.id("btnClose")).click();
			
			assertEquals("상세보기", contents);
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1000);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Given("^근태 달력 조회 화면$")
	public void openWorkAttitudeCalendarPage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement radioCalendar = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[2]/div/span/div/label[2]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", radioCalendar);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[2]/div/span/div/label[2]")).click();
			//IE에서는 동작하지 않아서 수정됨 
			
			Thread.sleep(4000);
			((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
			
			Thread.sleep(2000);
			// 현재 날짜
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[5]/a/div"));
			
			System.out.println(webElement.getText());
			assertNotNull(webElement.getText());
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^검색할 근태 달을 선택한다.$")
	public void searchWorkAttitudeCalendar() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement fromDate = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/table/thead/tr[1]/th[2]/div/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", fromDate);
		} else {
			Thread.sleep(2000); 
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/table/thead/tr[1]/th[2]/div/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/table/thead/tr[1]/th[2]/div/div[2]/div/div[2]/div[2]/div[2]")).click(); // 달 선택
		
			Thread.sleep(1000);
			driver.findElement(By.id("btnUserCalendarSearch")).click();
			// IE에서는 동작하지 않아서 수정됨
		}
	}
	
	@Then("^달력 정보가 정상적으로 나타났다는 표시로 해당 날짜에 근태 정보가 표시되어야함$")
	public void checkWorkAttitudeCalendar() throws Exception {
		Thread.sleep(3000);
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		Thread.sleep(2000);
		// 검색한 날짜의 데이터
		WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"your-custom-ID\"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/div[2]/table/tbody/tr/td[5]/a/div"), 20);

		System.out.println(webElement.getText());
		assertNotNull(webElement.getText());
	}
	
	@Given("^활동기록 조회 화면$")
	public void openRecordPage() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[3]"));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", webElement);
		} else {
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[2]")).click();
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[1]/div/ul/a[3]")).click();
			((JavascriptExecutor) driver).executeScript("window.scrollTo(document.body.scrollHeight, 0)");
			//IE에서는 동작하지 않아서 수정됨 
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@When("^다국어 버튼 클릭하기$")
	public void globalRecord() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", webElement);
		} else {
			Thread.sleep(2000); 
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/button[1]")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"language-menu\"]/div[2]/ul/div[2]/li")).click();
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^활동기록 리스트 조건 설정 및 \"(.*)\" 입력 후 검색하기$")
	public void searchRecord(String content) throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement webElement = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[1]/th[2]/div[1]/div/div"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", webElement);
		} else {
			Thread.sleep(2000); 
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[1]/th[2]/div[1]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[1]/th[2]/div[1]/div[2]/div/div[2]/div[1]/div[7]")).click(); // fromDate
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[1]/th[2]/div[3]/div/div")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[1]/th[2]/div[3]/div[2]/div/div[2]/div[6]/div[1]")).click(); // endDate
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[2]/th[2]/button")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("goToWork")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div[2]/div[2]/div/table/thead/tr[3]/th[2]/button")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("true")).click();
			
			Thread.sleep(1000);
			driver.findElement(By.id("userContent")).clear();
			driver.findElement(By.id("userContent")).sendKeys(content);
			
			Thread.sleep(1000);
			driver.findElement(By.id("btnUserSearch")).click();
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@Then("^활동기록 리스트 정보가 정상적으로 되었다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시 되어야함$")
	public void checkRecordList(String actor, String id) throws Exception {
		WebElement webElementActor = SeleniumUtil.waitForElementToBePresent(driver, By.id("actor"), 20);
		WebElement webElementId = SeleniumUtil.waitForElementToBePresent(driver, By.id("id"), 20);
		
		Thread.sleep(1000);
		
		String contentsActor = webElementActor.getText();
		String contentsId = webElementId.getText();
		
		System.out.println("contentsActor=" + contentsActor);
		System.out.println("contentsId=" + contentsId);
		
		if(!actor.equals(contentsActor)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("actor"));
					
			contentsActor = webElement2.getText();			
		}
		
		if(!id.equals(contentsId)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("id"));
					
			contentsId = webElement2.getText();			
		}
		
		assertEquals(actor, contentsActor);
		assertEquals(id, contentsId);
	}
	
	@And("^활동기록 리스트 데이터 클릭하기$")
	public void showRecordModal() throws Exception {
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			WebElement listData = SeleniumUtil.waitForElementToBePresent(driver, By.xpath("//*[@id=\"recordTable\"]/tbody/tr[1]"));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", listData);
		} else {
			// IE에서는 동작하지 않아서 수정됨

			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"recordTable\"]/tbody/tr[1]")).click();			
		}
		
		if (BROWSER_TYPE.equals(BROWSER_TYPE_IE)) {
			Thread.sleep(1500);
		} else {
			SeleniumUtil.waitForJStoLoad(driver);
		}
	}
	
	@And("^모달이 정상적으로 나타났다는 표시로 \"(.*)\", \"(.*)\" 정보가 표시되어야함$")
	public void checkRecordModal(String actor, String Id) throws Exception {
		WebElement webElementActor = SeleniumUtil.waitForElementToBePresent(driver, By.id("actor"), 20);
		WebElement webElementId = SeleniumUtil.waitForElementToBePresent(driver, By.id("id"), 20);
		
		Thread.sleep(2000);
		
		String contentsActor = webElementActor.getText();
		String contentsId = webElementId.getText();
		
		System.out.println("contentsActor=" + contentsActor);
		System.out.println("contentsId=" + contentsId);
		
		if(!actor.equals(contentsActor)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("actor"));
					
			contentsActor = webElement2.getText();			
		}
		
		if(!Id.equals(contentsId)) {
			Thread.sleep(2000);

			WebElement webElement2 = SeleniumUtil.waitForElementToBePresent(driver, By.id("Id"));
					
			contentsId = webElement2.getText();			
		}
		
		assertEquals(actor, contentsActor);
		assertEquals(Id, contentsId);
		
		driver.findElement(By.id("btnClose")).click();
	}
}
