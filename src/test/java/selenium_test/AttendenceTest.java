package selenium_test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions( 
		monochrome = true,
		plugin = {"junit:target/cucumber_summary","html:target/cucumber-html-report", "json:target/cucumber-json-report.json" },
		features={"classpath:test_scenario/0_근태관리-출근처리.feature"},
		glue = {"classpath:selenium_test"}
)
public class AttendenceTest {
	
	public static final String BASE_URL = "http://218.39.221.82:3000"; //"http://dev.wehago.com"; 
	
	public static final String BASE_URL_DEV = "http://218.39.221.82:3000"; //"http://dev.wehago.com";
	
	public static final String BASE_URL_PROD = "http://218.39.221.82:3000"; //"https://www.wehago.com";

}
