package backend;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.service.CommuteService;
import com.douzone.smartchecker.vo.CommuteVo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class CommuteTest {
	
	@Autowired
	CommuteService commuteService;
	
	@Test
	public void test() throws ParseException {
//		CommuteVo commuteVo = new CommuteVo();
//		commuteVo.setDay("2019-04-25 10:22:14");
//		commuteVo.setCommute("출근");
//		commuteVo.setNo(125);
//		System.out.println(commuteService.modify(commuteVo,"admin"));
//		
//		CommuteVo commuteVo2 = new CommuteVo();
//		commuteVo2.setUserNo(1);
//		commuteVo2.setNo(126);
//		commuteVo2.setDay("2019-04-24 16:22:14");
//		commuteVo2.setCommute("퇴근");
//		System.out.println(	commuteService.modify(commuteVo2,"admin"));
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 출퇴근 현황 페이지별 리스트 테스트
	 * @param
	 * @return
	 */
	@Test
	public void getList() {
		List<CommuteVo> commuteList = commuteService.getList(1);
		assertEquals(30, commuteList.size());
		
		List<CommuteVo> commuteList2 = commuteService.getList(2);
		assertEquals(30, commuteList2.size());
		
		assertNotEquals(commuteList, commuteList2);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 출퇴근 현황 검색 기능 테스트
	 * @param
	 * @return
	 */
	@Test
	public void getSearch() {
		/*
		List<CommuteVo> commuteList = commuteService.getSearch(1, 1, "2019-04-29", "2019-04-29");
		List<CommuteVo> commuteList2 = commuteService.getSearch(1, 1011, "", "");
		List<CommuteVo> commuteList3 = commuteService.getSearch(1, 0, "2019-04-29", "2019-04-29");
		
		assertFalse(commuteList.isEmpty());
		assertFalse(commuteList.isEmpty());
		assertFalse(commuteList.isEmpty());
		*/
	}
}
