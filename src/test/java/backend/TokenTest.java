package backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.repository.TokenDao;
import com.douzone.smartchecker.repository.UserRepository;
import com.douzone.smartchecker.vo.TokenVo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class TokenTest {
	
	  @Autowired
	    private UserRepository userRepository;
	  
	  @Autowired
	  private TokenDao tokenDao;

	 @Test
	  public void test() throws Exception {
		
		 //admin 토큰
		 String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTU1NzM4MDc4MywiaWF0IjoxNTU2Nzc1OTgzfQ.uH5xneHTXkPl0gPTtFvsw-g8bHjnh8pJ1rfKvAlSlLC7ernMHQA4-pUTKN0p4K_mQZI3liv8WbZqnW8_LqyaEA";
		 TokenVo vo = new TokenVo();
		 System.out.println(tokenDao.count()+"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		 vo.setToken(token);
		 
		 //입력 예제
		 tokenDao.insert(vo);
		 
		 
		 
		 //삭제 예제
		 for(TokenVo vo2 :tokenDao.findAll()) {
			 System.out.println(vo2);
			 if(vo2.getToken().equals(token)) {
				 tokenDao.deleteById(vo2.getId());
				 break;
			 }
		 }
		 
		 
		 System.out.println(tokenDao.count()+"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		 
	  }

}
