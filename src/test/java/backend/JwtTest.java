package backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer.JwtConfigurer;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.security.JwtTokenUtil;
import com.douzone.security.JwtUser;
import com.douzone.security.JwtUserFactory;
import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.repository.UserRepository;
import com.douzone.smartchecker.service.JwtUserDetailsService;
import com.douzone.smartchecker.vo.UserVo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class JwtTest {
	
	  @Autowired
	    private UserRepository userRepository;

	 @Test
	  public void test() throws Exception {
		
		 //admin 토큰
		 String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTU1NzM4MDc4MywiaWF0IjoxNTU2Nzc1OTgzfQ.uH5xneHTXkPl0gPTtFvsw-g8bHjnh8pJ1rfKvAlSlLC7ernMHQA4-pUTKN0p4K_mQZI3liv8WbZqnW8_LqyaEA";
				 
		 //토큰으로 사용자 정보 조회 예제
		 JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();
	        String username = jwtTokenUtil.getUsernameFromToken(token);
	     UserDetailsService userDetailsService = new JwtUserDetailsService();
	     
	     //오류이유 모르겠음
	        //JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);
	     UserVo vo = userRepository.findByUsername(username);
	     
	     JwtUser user =  JwtUserFactory.create(vo);
	        System.out.println(user);

	     //JwtUser 반환 데이터 처리 예제 -> client.js 파일
	   
	     
	     //비밀번호 반환 예제
	     UserDetails userDetails = user;
	     UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
	     JwtUser jwtUser = (JwtUser)authentication.getPrincipal();
	     System.out.println(jwtUser);
		 
	  }

}
