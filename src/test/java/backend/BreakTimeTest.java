package backend;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.repository.BreakTimeDao;
import com.douzone.smartchecker.service.BreakTimeService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class BreakTimeTest {

	@Autowired
	private BreakTimeService breakTimeService;
	
	@Autowired
	private BreakTimeDao breakTimeDao;
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 휴게 시간 등록 테스트
	 * @param
	 * @return true | false
	 */
	@Test
	public void insert() {
		 // System.out.println(breakTimeDao.getCount());
		 // System.out.println(breakTimeDao.updateBreakTime(breakTimeDao.getCount()));
		 // System.out.println(breakTimeDao.insertBreakTime("12:00", "13:00", "점심시간", "admin"));
		
		//assertTrue(breakTimeService.insertBreakTime("12:00", "13:00", "점심시간", "admin"));
	}
}
