package backend;
import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.service.CommuteService;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
@AutoConfigureMockMvc
public class WorkControllerTest {
	
	@Autowired
	private CommuteService commuteService;
	
	@Test
	public void goToTest() {
		//assertEquals(true, commuteService.insertCommute(1011, "user", "출근", "출근입력", LocalDate.now().toString()));
	}
	
	@Test
	public void goOffTest() {
		//assertEquals(true, commuteService.insertCommute(1011, "user", "퇴근", "퇴근입력", LocalDate.now().toString()));
	}
}

