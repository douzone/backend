package backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.service.TranslationService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class TransTest {
	
	@Autowired
	private TranslationService service;
	
	

	 @Test
	  public void test() throws Exception {
		
		 System.out.println(service.koToEn("오늘 날씨 좋군요~"));
		 System.out.println(service.enToKo("what are you talking about?"));
	  }

}
