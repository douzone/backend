package backend;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.repository.TokenDao;
import com.douzone.smartchecker.repository.UserRepository;
import com.douzone.smartchecker.service.TimeService;
import com.douzone.smartchecker.vo.TokenVo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class TimeTest {
	
	  @Autowired
	    private TimeService timeService;

	 @Test
	  public void test() throws Exception {
		
		 timeService.time((long)1038);
		 
	  }

}
