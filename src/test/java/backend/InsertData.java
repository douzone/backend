package backend;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.douzone.smartchecker.BootApplication;
import com.douzone.smartchecker.repository.CommuteDao;
import com.douzone.smartchecker.repository.UserDao;
import com.douzone.smartchecker.repository.WorkAttitudeDao;
import com.douzone.smartchecker.service.RecordService;
import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.UserVo;
import com.douzone.smartchecker.vo.WorkAttitudeVo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BootApplication.class)
public class InsertData {
	
	  @Autowired
	   private UserDao userDao;
	  
	  @Autowired
	  private CommuteDao commuteDao;
	  
	  @Autowired
		private RecordService recordService;
	  
	  @Autowired
	  private WorkAttitudeDao workAttitudeDao;
	  	//유저 입력
//	  @Test
//	  public void InsertUser() {
//		  
//		String id;
//		String name;
//		
//		
//		for(int i=0;i<100;i++) {
//			id = "user"+i;
//			name = randomHangulName();
//			userDao.InsertUser(id, name);
//		}
		  
		  
//	  }
	//  출근입력
//	  @Test	
//	  public void InsertCommute() {
//		  
//		  LocalDate localDateTime = LocalDate.of(2019,03,01);
//		  String Date;
//		  long userNo;
//		  
//		  String state;
//		  String day;
//		  long groupNo;
//		  
//		  List <UserVo> list = userDao.getTestData();
//		  
//		  //userNo = userDao.getTestData().get(0).getNo();
//		  
//		  for(int j =0;j<list.size();j++) {
//			  userNo = list.get(j).getNo();
//		  for(int i=0;i<90;i++) {
//			  
//			  Date = localDateTime.plusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//		  //int a = randomRange(0,1);
//			  int a =0;
//		  int b =0;
//		  if(a==1)
//			  b = randomRange(0,1);
//		  else 
//			  b = randomRange(7,9);
//		  int c=0;
//		  int d=0;
//		  int e=0;
//		  int f=0;
//		  c= randomRange(0,5);
//		  d=randomRange(0,9);
//		  e= randomRange(0,5);
//		  f=randomRange(0,9);
//		  if(b==9 && (c!=0 || d!=0 || e!=0 || f!=0))
//			  state="지각";
////		  else if(a==0&&b==9 && (c!=0 || d!=0 || e!=0 || f!=0))
////				  state="지각";
//				  else 
//					  state="정상";
//		  day = Date+" "+String.valueOf(a)+String.valueOf(b)+":"+String.valueOf(c)+String.valueOf(d)+":"+String.valueOf(e)+String.valueOf(f);
//		  groupNo = i+1;
//		  System.out.println(userNo+":"+day +" // "+state +" // "+ groupNo);
//		  
//		  CommuteVo vo = new CommuteVo();
//		  vo.setUserNo(userNo);
//		  vo.setDay(day);
//		  vo.setState(state);
//		  vo.setGroupNo(groupNo);
//		  
//		  
//		  RecordVo recordVo = new RecordVo();
//			recordVo.setNo(recordService.getLastNo() + 1);
//			recordVo.setId(String.valueOf(userNo));
//			recordVo.setActor( list.get(j).getName());
//			recordVo.setDay(Date);
//			recordVo.setRecordType("출근 입력");
//			recordVo.setRecordTypeEn("Enter attendance record");
//				if(state.equals("정상"))
//					recordVo.setContentEn(list.get(j).getName() + " has come to work normally");
//				else if(state.equals("지각"))
//					recordVo.setContentEn(list.get(j).getName() + " has came to work late");
//		
//			recordVo.setContent(list.get(j).getName() + "님이 " + state + " " + "출근" + " 하였습니다.");
//			recordVo.setRead(true);
//			recordVo.setUpdateTime(LocalDateTime.now().toString());
//			recordVo.setInsertTime(LocalDateTime.now().toString());
//			recordVo.setUpdateUserId(list.get(j).getUsername());
//			recordVo.setInsertUserId(list.get(j).getUsername());
//			recordVo.setUserNo(userNo);
//			
//			commuteDao.testDataInput(vo);
//			recordVo.setKeyNo(commuteDao.getLastInsertId());
//			 recordService.insert(recordVo);
//		  }
//		  
//		  }
//		  
//	  }
	  
	 // 퇴근입력
//	  @Test
//	  public void InsertCommute2() throws ParseException {
//		  
//		  LocalDate localDateTime = LocalDate.of(2019,03,01);
//		  String Date;
//		  long userNo;
//		  
//		  String state;
//		  String day;
//		  long groupNo;
//		  
//		  List <UserVo> list = userDao.getTestData();
//		  
//		  //userNo = userDao.getTestData().get(0).getNo();
//		  
//		  for(int j =0;j<list.size();j++) {
//			  userNo = list.get(j).getNo();
//		  for(int i=0;i<90;i++) {
//			  
//			  Date = localDateTime.plusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//		  int a = randomRange(1,1);
//		  int b =0;
//			  b = randomRange(6,9);
//		  int c=0;
//		  int d=0;
//		  int e=0;
//		  int f=0;
//		  c= randomRange(0,5);
//		  d=randomRange(0,9);
//		  e= randomRange(0,5);
//		  f=randomRange(0,9);
//		  if(b>=8)
//			  state="정상";
//		  else
//			  state="조퇴";
//		  day = Date+" "+String.valueOf(a)+String.valueOf(b)+":"+String.valueOf(c)+String.valueOf(d)+":"+String.valueOf(e)+String.valueOf(f);
//		  groupNo = i+1;
//		  		
//		  CommuteVo getstartData= commuteDao.getTestDate(userNo, groupNo);
//		  String s[] = getstartData.getDay().split(" ");
//		  
//		  CommuteVo vo = new CommuteVo();
//		  vo.setUserNo(userNo);
//		  vo.setDay(day);
//		  vo.setState(state);
//		  vo.setGroupNo(groupNo);
//
//		  String fromState=getstartData.getState();
//		  String startDate=s[0];
//		  String endDate=Date;
//		  String endTime=String.valueOf(a)+String.valueOf(b)+":"+String.valueOf(c)+String.valueOf(d)+":"+String.valueOf(e)+String.valueOf(f);
//		  String startTime=s[1];
//		  
//		  vo.setFromState(fromState);
//		  vo.setStartDate(startDate);
//		  vo.setEndDate(endDate);
//		  vo.setEndTime(endTime);
//		  vo.setStartTime(startTime);
//		  
//		  String totalWorktime = commuteDao.getEditTotalWorkTime(vo);
////		  map.put("no", vo.getUserNo());
////			map.put("fromState", vo.getFromState());
////			map.put("startDate", vo.getStartDate());
////			map.put("endDate", vo.getEndDate());
////			map.put("endTime", vo.getEndTime());
////			map.put("startTime", vo.getStartTime());
//
//			totalWorktime = commuteDao.getSubBreakTime(totalWorktime,endTime,userNo,startDate,startTime);
//			
//			vo.setTotalWorktime(totalWorktime);
//		  System.out.println(userNo+":"+day +" // "+state +" // "+ groupNo+" // "+startDate+" "+startTime+" // "+totalWorktime);
//		  
//		  
//		  RecordVo recordVo = new RecordVo();
//			recordVo.setNo(recordService.getLastNo() + 1);
//			recordVo.setId(String.valueOf(userNo));
//			recordVo.setActor( list.get(j).getName());
//			recordVo.setDay(Date);
//			recordVo.setRecordType("퇴근 입력");
//				recordVo.setRecordTypeEn("Enter attendance record");
//				 if(state.equals("정상"))
//					recordVo.setContentEn(list.get(j).getName() + " is off work normally.");
//				else if(state.equals("조퇴"))
//					recordVo.setContentEn( list.get(j).getName() + " has left early");
//			
//			recordVo.setContent( list.get(j).getName() + "님이 " + state + " " + "퇴근" + " 하였습니다.");
//			recordVo.setRead(true);
//			recordVo.setUpdateTime(LocalDateTime.now().toString());
//			recordVo.setInsertTime(LocalDateTime.now().toString());
//			recordVo.setUpdateUserId(list.get(j).getUsername());
//			recordVo.setInsertUserId(list.get(j).getUsername());
//			recordVo.setUserNo(userNo);
//			
//			
//			commuteDao.testDataInput2(vo);
//			recordVo.setKeyNo(commuteDao.getLastInsertId());
//			recordService.insert(recordVo);
//		  }
//		  
//		  }
//		  
//	  }
	  
	  //	근태 입력
	  @Test
	  public void InsertUser() {
		  List<String> list = Arrays.asList("출장","외근","연차","교육");
		  List<String> listEn = Arrays.asList("Business trip","Work outside","Annual leave","Education");
		  

		  	//a- a+b / a+b+n - a+b+n+m  //   a+n+b+m <30
		 
		
          List <UserVo> userList = userDao.getTestData();
          
          for(int j =0;j<userList.size();j++) {
        	  
        	  int a = randomRange(1,7);
    		  int b = randomRange(1,7);
              int n = randomRange(1,7);
              int m = randomRange(1,7);
              int c = randomRange(0,3);
              int d = randomRange(0,3);
              
              WorkAttitudeVo workAttitudeVo = new WorkAttitudeVo();
              WorkAttitudeVo workAttitudeVo2 = new WorkAttitudeVo();
              
              int year = 2019;
              int month = 3;
              
              LocalDate localDateTime = LocalDate.of(year,month,a);
              String startDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
             //System.out.println(startDate);
             localDateTime = LocalDate.of(year,month,a+b);
             String endDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            //System.out.println(endDate);
            
        	workAttitudeVo.setEndDay(endDate+" 23:59:59");
			workAttitudeVo.setStartDay(startDate+" 00:00:00");
			
			localDateTime = LocalDate.of(year,month,a+b+n);
             startDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
           //System.out.println(startDate);
           localDateTime = LocalDate.of(year,month,a+b+n+m);
            endDate = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
          //System.out.println(endDate);
          
      	workAttitudeVo2.setEndDay(endDate+" 23:59:59");
			workAttitudeVo2.setStartDay(startDate+" 00:00:00");
             
            
			workAttitudeVo.setUserNo(userList.get(j).getNo());
			workAttitudeVo.setInsertUserId("admin");
			workAttitudeVo.setUpdateUserId("admin");
			
			workAttitudeVo2.setUserNo(userList.get(j).getNo());
			workAttitudeVo2.setInsertUserId("admin");
			workAttitudeVo2.setUpdateUserId("admin");
			
			workAttitudeVo.setTitle(list.get(c));
			workAttitudeVo.setTitleEn(listEn.get(c));
			workAttitudeVo.setContent(list.get(c));
			workAttitudeVo.setContentEn(listEn.get(c));
			workAttitudeVo.setState("정상");
			workAttitudeVo.setWorkAttitudeList(list.get(c));
			
			workAttitudeVo2.setTitle(list.get(c));
			workAttitudeVo2.setTitleEn(listEn.get(c));
			workAttitudeVo2.setContent(list.get(c));
			workAttitudeVo2.setContentEn(listEn.get(c));
			workAttitudeVo2.setState("정상");
			workAttitudeVo2.setWorkAttitudeList(list.get(c));

			
			 localDateTime = LocalDate.of(2019,05,a);
             String applicationDate = localDateTime.minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			workAttitudeVo.setApplicationDate(applicationDate);
			
			localDateTime = LocalDate.of(2019,05,a+b+n);
            applicationDate = localDateTime.minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			workAttitudeVo2.setApplicationDate(applicationDate);

			//#{userNo}, #{title},#{titleEn}, #{startDay}, #{endDay}, current_timestamp(),
			//#{content},#{contentEn}, #{state}, #{workAttitudeList}, current_timestamp(),
			//current_timestamp(),#{updateUserId},#{insertUserId})	 
          if (workAttitudeDao.checkInsert(workAttitudeVo) == 0) {
        	  
  			if (workAttitudeDao.insertTestData(workAttitudeVo))  { //workAttitudeDao.insert(workAttitudeVo)
  				RecordVo recordVo = new RecordVo();

  				recordVo.setNo(recordService.getLastNo() + 1);
  				recordVo.setId(String.valueOf(userList.get(j).getNo()));
  				recordVo.setActor(userList.get(j).getName());
  				recordVo.setDay(workAttitudeVo.getApplicationDate());
  				recordVo.setRecordType("근태 신청");
  				recordVo.setRecordTypeEn("workattitude application");
  				recordVo.setContent(userList.get(j).getName() + "님이 근태 신청 하였습니다.");
  				recordVo.setContentEn(userList.get(j).getName() + "has workattitude application");
  				recordVo.setRead(false);
  				recordVo.setUpdateTime(LocalDateTime.now().toString());
  				recordVo.setInsertTime(LocalDateTime.now().toString());
  				recordVo.setUpdateUserId("admin");
  				recordVo.setInsertUserId(userList.get(j).getUsername());
  				recordVo.setKeyNo(workAttitudeDao.getLastInsertId());
  				recordVo.setUserNo(userList.get(j).getNo());
  				recordService.insert(recordVo);
  				//if (recordService.insert(recordVo) != null) {
//          
//  			
//  			}
          }
          }
          
          if (workAttitudeDao.checkInsert(workAttitudeVo2) == 0) {
        	  
    			if (workAttitudeDao.insertTestData(workAttitudeVo2))  { //workAttitudeDao.insert(workAttitudeVo)
    				RecordVo recordVo = new RecordVo();

    				recordVo.setNo(recordService.getLastNo() + 1);
    				recordVo.setId(String.valueOf(userList.get(j).getNo()));
    				recordVo.setActor(userList.get(j).getName());
    				recordVo.setDay(workAttitudeVo2.getApplicationDate());
    				recordVo.setRecordType("근태 신청");
    				recordVo.setRecordTypeEn("workattitude application");
    				recordVo.setContent(userList.get(j).getName() + "님이 근태 신청 하였습니다.");
    				recordVo.setContentEn(userList.get(j).getName() + "has workattitude application");
    				recordVo.setRead(false);
    				recordVo.setUpdateTime(LocalDateTime.now().toString());
    				recordVo.setInsertTime(LocalDateTime.now().toString());
    				recordVo.setUpdateUserId("admin");
    				recordVo.setInsertUserId(userList.get(j).getUsername());
    				recordVo.setKeyNo(workAttitudeDao.getLastInsertId());
    				recordVo.setUserNo(userList.get(j).getNo());
    				recordService.insert(recordVo);
    				//if (recordService.insert(recordVo) != null) {
            
    			
    			}
            }
          
    	        
          }
	
	}
	  
	  
	  
	  public  int randomRange(int n1, int n2) {
		    return (int) (Math.random() * (n2 - n1 + 1)) + n1;
		  }
	  public static String randomHangulName() {
		    List<String> 성 = Arrays.asList("김", "이", "박", "최", "정", "강", "조", "윤", "장", "임", "한", "오", "서", "신", "권", "황", "안",
		        "송", "류", "전", "홍", "고", "문", "양", "손", "배", "조", "백", "허", "유", "남", "심", "노", "정", "하", "곽", "성", "차", "주",
		        "우", "구", "신", "임", "나", "전", "민", "유", "진", "지", "엄", "채", "원", "천", "방", "공", "강", "현", "함", "변", "염", "양",
		        "변", "여", "추", "노", "도", "소", "신", "석", "선", "설", "마", "길", "주", "연", "방", "위", "표", "명", "기", "반", "왕", "금",
		        "옥", "육", "인", "맹", "제", "모", "장", "남", "탁", "국", "여", "진", "어", "은", "편", "구", "용");
		    List<String> 이름 = Arrays.asList("가", "강", "건", "경", "고", "관", "광", "구", "규", "근", "기", "길", "나", "남", "노", "누", "다",
		        "단", "달", "담", "대", "덕", "도", "동", "두", "라", "래", "로", "루", "리", "마", "만", "명", "무", "문", "미", "민", "바", "박",
		        "백", "범", "별", "병", "보", "빛", "사", "산", "상", "새", "서", "석", "선", "설", "섭", "성", "세", "소", "솔", "수", "숙", "순",
		        "숭", "슬", "승", "시", "신", "아", "안", "애", "엄", "여", "연", "영", "예", "오", "옥", "완", "요", "용", "우", "원", "월", "위",
		        "유", "윤", "율", "으", "은", "의", "이", "익", "인", "일", "잎", "자", "잔", "장", "재", "전", "정", "제", "조", "종", "주", "준",
		        "중", "지", "진", "찬", "창", "채", "천", "철", "초", "춘", "충", "치", "탐", "태", "택", "판", "하", "한", "해", "혁", "현", "형",
		        "혜", "호", "홍", "화", "환", "회", "효", "훈", "휘", "희", "운", "모", "배", "부", "림", "봉", "혼", "황", "량", "린", "을", "비",
		        "솜", "공", "면", "탁", "온", "디", "항", "후", "려", "균", "묵", "송", "욱", "휴", "언", "령", "섬", "들", "견", "추", "걸", "삼",
		        "열", "웅", "분", "변", "양", "출", "타", "흥", "겸", "곤", "번", "식", "란", "더", "손", "술", "훔", "반", "빈", "실", "직", "흠",
		        "흔", "악", "람", "뜸", "권", "복", "심", "헌", "엽", "학", "개", "롱", "평", "늘", "늬", "랑", "얀", "향", "울", "련");
		    Collections.shuffle(성);
		    Collections.shuffle(이름);
		    return 성.get(0) + 이름.get(0) + 이름.get(1);
		  }


}
