Feature: 근태관리-출근처리

@FirstScenario
Scenario Outline: 일반사용자로 로그인하기
  Given 로그인 화면
  When <ID> <Password> 을 입력하고 로그인버튼 클릭하기
  Then 로그인이 정상적으로 되었다는 표시로 "테스트" 사용자 정보가 출력되어야 함
  Examples:  
    | ID         | Password       |
    | "test"     | "1"            |


Scenario: 출근 등록 버튼 클릭하기
  Given 출퇴근 신청 화면
  When 출근 버튼 클릭하기
  Then 출근이 정상적으로 되었다는 표시로 "출근 등록 되었습니다" 문구가 표시되어야함


Scenario: 출퇴근 리스트 조회하기
  Given 출퇴근 리스트 조회 화면 "출퇴근 조회" 문구가 떠야함
  When 검색할 날짜를 선택한다.
  Then 리스트 정보가 정상적으로 되었다는 표시로 "1051", "테스트" 정보가 표시되어야함
  And 출퇴근 리스트 데이터 클릭하기
  And 모달이 정상적으로 띄워졌다는 표시로 "상세보기" 정보가 표시되어야함


Scenario: 출퇴근 달력 조회하기
  Given 출퇴근 달력 조회 화면
  When 검색할 달을 선택한다.
  Then 달력 정보가 정상적으로 나타났다는 표시로 해당 날짜에 정보가 표시되어야함
  
  
Scenario Outline: 근태 신청하기
  Given 근태 신청 화면
  When <Title> <Content>을 입력하고 등록 버튼 클릭하기
  Then 신청이 정상적으로 되었다는 표시로 "성공적으로 신청하였습니다." alert 창이 출력되어야함
  Examples:
    | Title    | Content     |
    | "교육 신청" | "세미나 참석"   |  


Scenario: 근태 리스트 조회하기
  Given 근태 리스트 조회 화면 "근태 조회" 문구가 떠야함
  When 검색할 근태의 날짜를 선택한다.
  Then 근태 리스트 정보가 정상적으로 되었다는 표시로 "1051", "테스트" 정보가 표시되어야함
  And 근태 리스트 데이터 클릭하기
  
  
Scenario: 근태 달력 조회하기
  Given 근태 달력 조회 화면
  When 검색할 근태 달을 선택한다.
  Then 달력 정보가 정상적으로 나타났다는 표시로 해당 날짜에 근태 정보가 표시되어야함


Scenario Outline: 활동기록 조회하기
  Given 활동기록 조회 화면
  When 다국어 버튼 클릭하기
  And 활동기록 리스트 조건 설정 및 <Content> 입력 후 검색하기
  Then 활동기록 리스트 정보가 정상적으로 되었다는 표시로 "테스트", "test" 정보가 표시 되어야함
  And 활동기록 리스트 데이터 클릭하기
  And 모달이 정상적으로 나타났다는 표시로 "테스트", "test" 정보가 표시되어야함
  Examples:
    | Content 	     |
    | "came to work" |
  

# Scenario: 개인카드-개인사용자-카드사용내역 접근하기
#  Given 경비청구-개인카드 화면
#  When 카드사용내역 메뉴 클릭하기
#  Then 등록된 카드 리스트가 나와야 함

  
# 출근 등록 시나리오

# Examples:
#	| no	| id	    | commute	| recordType	|
# 	| 1011	| "sunghye"	| "출근"	    | "출근 입력"	    |
 	
 	 

  
