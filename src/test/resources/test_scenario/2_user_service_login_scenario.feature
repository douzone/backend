Feature: 로그인/로그아웃

Scenario: 관리자로 로그인하기
  Given 로그인 화면
  When "admin" 와 "success" 을 입력하고 Sign in 버튼 클릭하기
  Then 로그인이 정상적으로 되었다는 표시로 "logout" 링크가 출력되어야 함
  
Scenario: 관리자에서 로그아웃하기  
  Given 로그인된 화면
  When "logout" 링크 클릭하기
  Then 로그인 화면이 나와야 함

Scenario: 관리자로 로그인하기(잘못된 Password로)
  Given 로그인 화면
  When "admin" 와 "success0" 을 입력하고 Sign in 버튼 클릭하기
  Then "로그인에 실패하였습니다."라는 메시지가 출력되어야 함


Scenario Outline: 일반사용자로 로그인하기
  Given 로그인 화면
  When <ID> <password> 을 입력하고 Sign in 버튼 클릭하기
  Then 로그인이 정상적으로 되었다는 표시로 "logout" 링크가 출력되어야 함
  Examples:  
    | ID                   | password   |
    | "registered_user001" | "success"  |

Scenario: 일반사용자에서 로그아웃하기  
  Given 로그인된 화면
  When "logout" 링크 클릭하기
  Then 로그인 화면이 나와야 함

Scenario Outline: 일반사용자로 로그인하기(잘못된 Password로)
  Given 로그인 화면
  When <ID> <password> 을 입력하고 Sign in 버튼 클릭하기
  Then "로그인에 실패하였습니다."라는 메시지가 출력되어야 함
  Examples:  
    | ID                   | password   |
    | "registered_user001" | "success0"  |
    | "registered_user002" | "success0"  |

Scenario Outline: 일반사용자로 로그인하기(등록되지 않은 사용자로)
  Given 로그인 화면
  When <ID> <password> 을 입력하고 Sign in 버튼 클릭하기
  Then "로그인에 실패하였습니다."라는 메시지가 출력되어야 함
  Examples:  
    | ID                   | password   |
    | "unregistered_user003" | "success"  |
    | "unregistered_user004" | "success"  |


Scenario Outline: User ID 이나 password가 공백일 경우
  Given 로그인 화면
  When <ID> <password> 을 입력하고 Sign in 버튼 클릭하기
  Then "로그인에 실패하였습니다."라는 메시지가 출력되어야 함
  Examples:  
    | ID                   | password   |
    | ""                   | ""         |
    | "registered_user001" | ""         |
    | ""                   | "success"  |

