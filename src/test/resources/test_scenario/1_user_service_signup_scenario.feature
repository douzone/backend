Feature: 회원 가입 

@FirstScenario
Scenario: 회원 가입 화면으로 들어가기
  Given 로그인 화면
  When Join Us 링크 클릭하기
  Then 회원 가입을 할 수 있는 PopUp 화면이 나와야 함

#Background:
#  Given 로그인 화면
#  When Join Us 링크 클릭하기
#  Then 회원 가입을 할 수 있는 PopUp 화면이 나와야 함

Scenario Outline: 처음 가입하는 사용자로 회원 가입하기
  Given 회원 가입 화면
  When <ID> <password1> <password2> 을 입력하고 Register 버튼 클릭하기
  Then "등록이 완료되었습니다!"라는 메시지가 출력되어야 함
  And OK 버튼 클릭하기
  Examples:  
    | ID                   | password1  | password2 |
    | "registered_user0001" | "success"  |  "success"|
    | "registered_user0002" | "success"  |  "success"|


Scenario Outline: 이미 가입된 사용자로 회원 가입하기
  Given 회원 가입 화면
  When <ID> <password1> <password2> 을 입력하고 Register 버튼 클릭하기
  Then "이미 등록되어 있는 사용자입니다!"라는 메시지가 출력되어야 함
  And OK 버튼 클릭하기
  Examples:  
    | ID                   | password1  | password2 |
    | "registered_user0001" | "success" |  "success" |
    | "registered_user0002" | "success" |  "success" |
    

Scenario Outline: 첫번째 password와 두번째 password가 다른 경우
  Given 회원 가입 화면
  When <ID> <password1> <password2> 을 입력하고 Register 버튼 클릭하기
  Then "입력하신 패스워드가 정확하지 않습니다."라는 메시지가 출력되어야 함
  And OK 버튼 클릭하기
  Examples:  
    | ID                   | password1  | password2 |
    | "registered_user0001" | "success" |  "success1" |
    | "registered_user0001" | "success1" |  "success" |
    | "registered_user0001" | "success1" |  "" |    
    | "registered_user0001" | "" |  "success1" |    
    
    
Scenario Outline: User ID이 공백일 경우
  Given 회원 가입 화면
  When <ID> <password1> <password2> 을 입력하고 Register 버튼 클릭하기
  Then "필수 항목을 입력하지 않았습니다!"라는 메시지가 출력되어야 함
  And OK 버튼 클릭하기
  Examples:  
    | ID                    | password1  | password2 |
    | ""  | "success"  |  "success" |
    | " " | "success"  |  "success" |
    

Scenario Outline: 첫번째 password가 공백일 경우
  Given 회원 가입 화면
  When <ID> <password1> <password2> 을 입력하고 Register 버튼 클릭하기
  Then "필수 항목을 입력하지 않았습니다!"라는 메시지가 출력되어야 함
  And OK 버튼 클릭하기
  Examples:  
    | ID                    | password1  | password2 |
    | "registered_user0001"  | ""  |  "success1" |
    | "registered_user0001"  | " " |  "success1" |






    
    
    
    
    
    
        