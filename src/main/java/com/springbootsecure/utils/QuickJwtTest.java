package com.springbootsecure.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class QuickJwtTest {
	
	 public static void main(String[] args) {
		 
		 String jwtString = Jwts.builder()
				 .setHeaderParam("type", "JWT")
				 .setHeaderParam("issueDate", System.currentTimeMillis())
				 .setSubject("안녕안녕")
				 .signWith(SignatureAlgorithm.HS512, "aaaa")
				 .compact();

		 System.out.println(jwtString);
	    }    
}
