package com.douzone.smartchecker.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.security.JwtUser;
import com.douzone.smartchecker.repository.HolidayDao;
import com.douzone.smartchecker.vo.HoliDayVo;
import com.douzone.smartchecker.vo.RecordVo;

/**
 * @author PSH
 * @Date 2019. 5. 3.
 * @brief 휴일 설정 Service
 *
 */
@Service
public class HolidayService {

	@Autowired
	private HolidayDao holidayDao;

	@Autowired
	private RecordService recordService;

	@Autowired
	private TranslationService translationService;

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 등록
	 * @param day, description, userId
	 * @return insert 여부
	 */
	public Object create(String day, String description, String use, JwtUser userDetails, Locale locale) {
		int mysql = 0;
		RecordVo mongo = null;

		if (holidayDao.checkInsert(day) == 0) {
			RecordVo recordVo = new RecordVo();

			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(userDetails.getUsername());
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("휴일 설정");
			recordVo.setRecordTypeEn("Set holiday");
			recordVo.setContent(userDetails.getName() + "님이 휴일 설정 하였습니다.");
			recordVo.setContentEn(userDetails.getName() + "has set the holiday");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setUserNo(userDetails.getNo());

			String descriptionEn = "";
			if (locale.toString().equals("ko")) {
				descriptionEn = translationService.koToEn(description);
			} else if (locale.toString().equals("en")) {
				descriptionEn = description;
				description = translationService.enToKo(description);
			}

			mysql = holidayDao.insert(day, description, descriptionEn, userDetails.getUsername(), use);
			mongo = recordService.insert(recordVo);
		}
		else {
			return "날짜가 겹칩니다.";
		}

		if (mongo != null && mysql == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 전체 리스트 개수
	 * @param
	 * @return int
	 */
	public int getCount() {
		return holidayDao.getCount();
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 전체 리스트
	 * @param
	 * @return List<HoliDayVo>
	 */
	public List<HoliDayVo> getList(int page) {
		return holidayDao.getList(page);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 정보
	 * @param no
	 * @return HoliDayVo
	 */
	public HoliDayVo getData(long no) {
		return holidayDao.getData(no);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 정보 수정
	 * @param no, day, description, use
	 * @return boolean
	 */
	public Object editData(long no, String day, String description, String use, JwtUser userDetails, Locale locale) {
		int result = 0;
		RecordVo mongo = null;
		String descriptionEn;
		
		if (holidayDao.checkEdit(day, no) == 0) {
			if(locale.toString().equals("ko")) {
				descriptionEn = translationService.koToEn(description);
			}
			else {
				descriptionEn = description;
				description = translationService.enToKo(description);
			}
			
			result = holidayDao.editData(no, day, description, descriptionEn, use, userDetails.getUsername());

			RecordVo recordVo = new RecordVo();

			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(userDetails.getUsername());
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("휴일 수정");
			recordVo.setRecordTypeEn("Modify holi day");
			recordVo.setContent(userDetails.getName() + "님이 휴일 수정 하였습니다.");
			recordVo.setContentEn(userDetails.getName() + "has modified the holi day.");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setUserNo(userDetails.getNo());

			mongo = recordService.insert(recordVo);
		}
		else {
			return "날짜가 겹칩니다.";
		}

		if (result == 1 && mongo != null) {
			return true;
		}

		return false;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 삭제
	 * @param no
	 * @return boolean
	 */
	public boolean removeData(long no, JwtUser userDetails) {
		int result;

		result = holidayDao.removeData(no);

		RecordVo recordVo = new RecordVo();

		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(userDetails.getUsername());
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("휴일 삭제");
		recordVo.setRecordTypeEn("holi day remove");
		recordVo.setContent(userDetails.getName() + "님이 휴일 삭제 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + "has deleted the holi day.");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());

		RecordVo mongo = recordService.insert(recordVo);

		if (result == 1 && mongo != null) {
			return true;
		}

		return false;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 6. 13.
	 * @brief 어제가 휴일인지 체크
	 * @param 
	 * @return boolean
	 */
	public boolean checkHoliDay() {
		List<HoliDayVo> list = holidayDao.getUseList();
		
		for (HoliDayVo holiDayVo : list) {
			if(holiDayVo.getDay().equals(LocalDate.now().minusDays(1).toString())) {
				return true;
			}
		}
		
		return false;
	}
}