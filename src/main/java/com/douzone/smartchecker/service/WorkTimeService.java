package com.douzone.smartchecker.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.security.JwtUser;
import com.douzone.smartchecker.repository.WorkTimeDao;
import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

/**
 * @author JYS
 * @date 2019. 4. 24.
 * @brief WorkTime 서비스
 */

@Service
public class WorkTimeService {

	@Autowired
	private WorkTimeDao workTimeDao;
	
	@Autowired
	private RecordService recordService;
	
	
	public WorkTimeVo getUseWorkTime() {
		return workTimeDao.getUseWorkTime();
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근무 시간 설정
	 * @param start, end, userId
	 * @return insert 성공 여부
	 */
	public boolean insertWorkTime(String start, String end, String use, JwtUser userDetails) {
		int insert;
		int update = 0;
		
		if(use.equals("false")) {
			insert = workTimeDao.insertWorkTime(start, end, userDetails.getUsername(), use);
			if(insert==1)
				return true;
			return false;
		}
		else {
			insert = workTimeDao.insertWorkTime(start, end, userDetails.getUsername(), use);
			update = workTimeDao.updateWorkTime();
		}
		
		RecordVo recordVo = new RecordVo();
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(userDetails.getUsername());
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("근무시간 설정");
		recordVo.setRecordTypeEn("Set work time");
		recordVo.setContent(userDetails.getName() + "님이 근무시간 설정 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + "has set the work time");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());
		
		RecordVo mongo = recordService.insert(recordVo);
		
		if(mongo != null && insert == 1 && update != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 근무 설정 리스트
	 * @param
	 * @return List<WorkTimeVo>
	 */
	public List<WorkTimeVo> getList(int page){
		return workTimeDao.getList(page);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 근무 설정 리스트 개수
	 * @param
	 * @return int
	 */
	public int getCount() {
		return workTimeDao.getCount();
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 가져오기
	 * @param no
	 * @return WorkTimeVo
	 */
	public WorkTimeVo getData(long no){
		return workTimeDao.getData(no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 수정
	 * @param no, start, end, use
	 * @return boolean
	 */
	public boolean editData(long no, String start, String end, String use, JwtUser userDetails) {
		int edit;
		int update;
		
		RecordVo recordVo = new RecordVo();
		
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(userDetails.getUsername());
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("근무시간 수정");
		recordVo.setContent(userDetails.getName() + "님이 근무시간 수정 하였습니다.");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());
		
		RecordVo mongo = recordService.insert(recordVo);
		
		if(use.equals("false")) {
			edit = workTimeDao.editData(no, start, end, use, userDetails.getUsername());
			
			if(edit == 1 && mongo != null) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			edit = workTimeDao.editData(no, start, end, use, userDetails.getUsername());
			update = workTimeDao.updateData(no);
			
			if(edit == 1 && update != 0 && mongo != null) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 삭제
	 * @param no
	 * @return boolean
	 */
	public boolean removeDate(long no, JwtUser userDetails) {
		int result;
		
		result = workTimeDao.removeData(no);
		
		RecordVo recordVo = new RecordVo();
		
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(userDetails.getUsername());
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("근무시간 삭제");
		recordVo.setContent(userDetails.getName() + "님이 근무시간 삭제 하였습니다.");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());
		
		RecordVo mongo = recordService.insert(recordVo);
		
		if(result == 1 && mongo != null) {
			return true;
		}
		
		return false;
	}
}
