package com.douzone.smartchecker.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.dto.CalendarDto;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.repository.CommuteDao;
import com.douzone.smartchecker.vo.BreakTimeVo;
import com.douzone.smartchecker.vo.CommuteVo;
import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

/**
 * @author JYS
 * @date 2019. 4. 23.
 * @brief Commute서비스
 */
@Service
public class CommuteService {

	@Autowired
	private CommuteDao commuteDao;

	@Autowired
	private RecordService recordService;

	@Autowired
	private WorkTimeService workTimeService;

	@Autowired
	private BreakTimeService breakTimeService;

	/**
	 * @author JYS
	 * @date 2019. 4. 23.
	 * @brief 달력 정보 리스트 얻기
	 */
	public List<CommuteVo> getCalendarList(String no, String startDate, String endDate, String state) {

		List<CommuteVo> list = commuteDao.selectByMonthAndNo(no, startDate, endDate, state);
		for (CommuteVo vo : list) {
			if(vo.getToState()!=null) {
			if (vo.getFromState().equals("에러") || vo.getToState().equals("에러"))
				vo.setState("에러");
			else if (vo.getFromState().equals("정상") && vo.getToState().equals("정상"))
				vo.setState("정상");
			else if (vo.getFromState().equals("지각") && vo.getToState().equals("조퇴"))
				vo.setState("지각조퇴");
			else if (vo.getFromState().equals("지각"))
				vo.setState("지각");
			else if (vo.getToState().equals("조퇴"))
				vo.setState("조퇴");
			}
			else {
				if (vo.getFromState().equals("에러")) 
						vo.setState("에러");
				else if (vo.getFromState().equals("정상"))
					vo.setState("정상");
				else if (vo.getFromState().equals("지각"))
					vo.setState("지각");
			}
		}
		return list;
	}

	/**
	 * @author JYS
	 * @date 2019. 5. 16.
	 * @brief 달력 정보 리스트 얻기
	 */
	public List<CommuteVo> getCalendarListByName(String name, String startDate, String endDate, String state) {

		List<CommuteVo> list = commuteDao.selectByMonthAndName(name, startDate, endDate, state);
		for (CommuteVo vo : list) {
			if(vo.getToState()!=null) {
				if (vo.getFromState().equals("에러") || vo.getToState().equals("에러"))
					vo.setState("에러");
				else if (vo.getFromState().equals("정상") && vo.getToState().equals("정상"))
					vo.setState("정상");
				else if (vo.getFromState().equals("지각") && vo.getToState().equals("조퇴"))
					vo.setState("지각조퇴");
				else if (vo.getFromState().equals("지각"))
					vo.setState("지각");
				else if (vo.getFromState().equals("조퇴"))
					vo.setState("조퇴");
				}
				else {
					if (vo.getFromState().equals("에러")) 
							vo.setState("에러");
					else if (vo.getFromState().equals("정상"))
						vo.setState("정상");
					else if (vo.getFromState().equals("지각"))
						vo.setState("지각");
				}
		}
		return list;
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 22.
	 * @brief 출퇴근 등록
	 * @param no, id
	 * @return insert 여부
	 * @throws ParseException
	 */
	public Object insertCommute(String commute, String recordType,String totalWorkTime, String today, JwtUser userDetails) {

		String baseDate = null;
		if (commute.equals("퇴근")) {
			baseDate = commuteDao.getStartDate(userDetails.getNo());
		}

		String state = compareTime(commute, null, baseDate);
		//System.out.println("상태 :  " + compareTime(commute, null, baseDate));

		RecordVo recordVo = new RecordVo();
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(String.valueOf(userDetails.getNo()));
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType(recordType);
		if(commute.equals("출근")) {
		recordVo.setRecordTypeEn("Enter attendance record");
			if(state.equals("정상"))
				recordVo.setContentEn(userDetails.getName() + " has come to work normally");
			else if(state.equals("지각"))
				recordVo.setContentEn(userDetails.getName() + " has came to work late");
			else if(state.equals("에러"))
				recordVo.setContentEn(userDetails.getName() + " commuting record is an error..");
		}
		else {
			recordVo.setRecordTypeEn("Enter attendance record");
			 if(state.equals("정상"))
				recordVo.setContentEn(userDetails.getName() + " is off work normally.");
			else if(state.equals("조퇴"))
				recordVo.setContentEn(userDetails.getName() + " has left early");
			else if(state.equals("에러"))
				recordVo.setContentEn(userDetails.getName() + " commuting record is an error.");
		}
		recordVo.setContent(userDetails.getName() + "님이 " + state + " " + commute + " 하였습니다.");
		recordVo.setRead(true);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());

		RecordVo mongo = null;
		int count = commuteDao.getCommuteCount(userDetails.getNo(), today);
		
		if(commute.equals("퇴근")) {
			count = commuteDao.getGoOffCount(userDetails.getNo());
		}
		
		int mysql = 0;

		//System.out.println(userDetails.getNo() + " " + userDetails.getUsername() + " " + count + " " + today);

		if (count == 0) {
			mysql = commuteDao.insertCommute(userDetails.getNo(), userDetails.getUsername(), commute, state,totalWorkTime);
			recordVo.setKeyNo(commuteDao.getLastInsertId());
			mongo = recordService.insert(recordVo);
		}
		else {
			return "이미 " + commute + "하셨습니다.";
		}
		
		//System.out.println(mysql);

		if (mongo != null && mysql == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 25.
	 * @brief 출퇴근 상태 체크
	 * @param commute
	 * @return state
	 */
	public String compareTime(String commute, Date vDate, String baseDate) {
		WorkTimeVo workTimeVo = workTimeService.getUseWorkTime();
		String state = null;

		Date now;
		if (vDate == null) {
			now = new Date(System.currentTimeMillis());
		} else {
			now = vDate;
		}

		if (baseDate == null || baseDate == "") {
			baseDate = LocalDate.now().toString();
		}

		
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		//DateFormat date = DateFormat.getDateInstance(DateFormat.LONG, Locale.UK);//new SimpleDateFormat("yyyy-MM-dd HH:mm");
		//DateFormat date2 = DateFormat.getDateInstance(DateFormat.LONG, Locale.KOREA);//new SimpleDateFormat("yyyy-MM-dd HH:mm");
//		DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm (z Z)");
//		
//		String[] timeZoneArr = TimeZone.getAvailableIDs();
//		
//		for(String timeZone : timeZoneArr) {
//			System.out.println(timeZone);
//		}
//		
//		TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
//		date.setTimeZone(tz);
//		System.out.println(tz.getDisplayName() + " " + date.format(new Date()));
//		
//		TimeZone tz2 = TimeZone.getTimeZone("America/Los_Angeles");
//		date.setTimeZone(tz2);
//		System.out.println(tz2.getDisplayName() + " " + date.format(new Date()));
//
//		TimeZone tz3 = TimeZone.getTimeZone("UTC");
//		date.setTimeZone(tz3);
//		System.out.println(tz3.getDisplayName() + " " + date.format(new Date()));

		try {
			//System.out.println(LocalDate.now());
			                                                                                                       
			Date startTime = date.parse(baseDate + " " + workTimeVo.getStart());
			Date endTime = date.parse(baseDate + " " + workTimeVo.getEnd());

			Date afterStartTime;
			Calendar cal = Calendar.getInstance();
			cal.setTime(startTime);
			cal.add(Calendar.DATE, 1);
			afterStartTime = date.parse(date.format(cal.getTime()));

			//System.out.println(startTime + " " + endTime + " " + now + " " + afterStartTime);
			//System.out.println(startTime.getTime() + " " + endTime.getTime() + " " + now.getTime());

			if (commute.equals("출근")) {
				if (now.before(startTime)) { // 출근 시간 전 출근
					state = "정상";
				} else if (now.after(startTime)) { // 출근 시간 후 출근
					state = "지각";
				}
			} else if (commute.equals("퇴근")) {

				if (now.after(startTime) && now.before(endTime)) {
					state = "조퇴";
				} else if (now.after(endTime) && now.before(afterStartTime)) { // 퇴근 시간 후 퇴근 && 하루가 지난 후 출근 시간 전 퇴근
					state = "정상";
				} else {
					state = "에러";
				}
				/*
				 * else if(now.after(startTime)) { // 출근 시간 후 퇴근 state = "에러"; }
				 */
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return state;
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 23.
	 * @brief 출퇴근 현황 전체 리스트
	 * @param
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getList(int page) {
		// return commuteDao.getList(page);
		return null;
	}

	/**
	 * + * @author PSH + * @date 2019. 4. 26. + * @brief 촐퇴근 현황 리스트 개수 + * @param +
	 * * @return int +
	 */
	public int getTotalCount() {
		return commuteDao.getTotalCount();
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 출퇴근 현황 검색 리스트 개수
	 * @param startDate, endDate, userNo
	 * @return int
	 */
	public int getSearchCount(String startDate, String endDate, Long userNo, String searchState, boolean select,
			String userName, Long keyNo) {
		int count = 0;

		count = commuteDao.getSearchFullCount(startDate, endDate, userNo, searchState, select, userName, keyNo);

		return count;
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 현황 검색 리스트
	 * @param name, startDate, endDate
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getSearch(Long page, Long no, String startDate, String endDate, String state, boolean select, String userName, Long keyNo) {
		List<CommuteVo> list = null;

		list = commuteDao.getList(page, no, startDate, endDate, state, select, userName, keyNo);

		return list;
	}

	/**
	 * @author JYS
	 * @date 2019. 4. 25.
	 * @brief 출퇴근 정보 수정
	 * @param commuteVo , id
	 * @return 수정결과
	 * @throws ParseException
	 */
	public String modify(CommuteVo commuteVo, JwtUser userDetails) throws ParseException {
		WorkTimeVo workTimeVo = workTimeService.getUseWorkTime();
		List<BreakTimeVo> list = breakTimeService.getUseBreakTime();
		String time = commuteVo.getDay().substring(11, 16);
		String workStartTime = workTimeVo.getStart();
		String workEndTime = workTimeVo.getEnd();
		if (commuteVo.getCommute().equals("출근")) {
			commuteVo.setTotalWorktime("0");
			if (timeCompare(workStartTime, time) < 0)
				commuteVo.setState("지각");
			else
				commuteVo.setState("정상");
		} else {
			if (timeCompare(workEndTime, time) > 0)
				commuteVo.setState("조퇴");
			else
				commuteVo.setState("정상");

			CommuteVo vo = commuteDao.getGotoWorkDataByDateAndNo(commuteVo.getDay().substring(0, 11),
					commuteVo.getUserNo());
			if (vo == null)
				return "출근기록이 없습니다.";
			int breakTime = 0;
			for (BreakTimeVo breakTimeVo : list)
				breakTime += timeCompare(breakTimeVo.getEnd(), breakTimeVo.getStart());
			int total = timeCompare(commuteVo.getDay().substring(11, 16), vo.getDay().substring(11, 16)) - breakTime;
			int hour = total / 60;
			int min = total % 60;
			String resultTime = hour + ":" + min;
			commuteVo.setTotalWorktime(resultTime);
		}

		commuteVo.setUpdateUserId(userDetails.getUsername());

		if (commuteDao.update(commuteVo)) {
			RecordVo recordVo = new RecordVo();
			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(String.valueOf(commuteVo.getUserNo()));
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("출퇴근수정");
			recordVo.setContent(userDetails.getUsername() + "님이 출퇴근 수정 신청 하였습니다.");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setKeyNo(commuteVo.getStartNo());
			recordVo.setUserNo(userDetails.getNo());

			if (recordService.insert(recordVo) != null) {
				return "수정성공";
			}
		}
		return "수정실패";
	}

	private int timeCompare(String time1, String time2) {
		String time3[] = time1.split(":");
		String time4[] = time2.split(":");
		int result = 0;
		result += (Integer.parseInt(time3[0]) - Integer.parseInt(time4[0])) * 60;
		result += Integer.parseInt(time3[1]) - Integer.parseInt(time4[1]);
		return result;
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 8.
	 * @brief 츨퇴근 상세조회
	 * @param startNo, endNo
	 * @return CommuteVo
	 */
	public CommuteVo getSearchBetween(long startNo, long endNo) {

		return commuteDao.getSearchBetween(startNo, endNo);
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 8.
	 * @brief 출퇴근 수정
	 * @param CommuteVo
	 * @return int
	 */
	public boolean editCommuteBetween(CommuteVo vo, JwtUser userDetails) {
		int mysql = 0;
		RecordVo mongo = null;
		Date changeDate;

		String startTime = vo.getStartDate() + " " + vo.getStartTime().substring(0, 5);
		String endTime = vo.getEndDate() + " " + vo.getEndTime().substring(0, 5);

		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		try {
			changeDate = transFormat.parse(startTime);
			// vo.setFromState(compareTime(vo.getStartCommute(), changeDate,
			// vo.getStartDate()));
			vo.setFromState(compareTime("출근", changeDate, vo.getStartDate()));
			changeDate = transFormat.parse(endTime);

			if (null == vo.getEndCommute()) {
				vo.setEndCommute("퇴근");
			}

			vo.setToState(compareTime("퇴근", changeDate, vo.getStartDate()));

		} catch (ParseException e) {
			e.printStackTrace();
		}

		RecordVo recordVo = new RecordVo();

		//System.out.println("번호 : " + vo.getUserNo());
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(String.valueOf(vo.getUserNo()));
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("출퇴근 수정");
		recordVo.setRecordTypeEn("commuting modification");
		recordVo.setContent(userDetails.getName() + "님이 출퇴근 수정 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + "has modified the commute");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setKeyNo(vo.getStartNo());
		recordVo.setUserNo(userDetails.getNo());

		mysql = commuteDao.editCommuteBetween(vo);
		mongo = recordService.insert(recordVo);

		if (mongo != null && mysql == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 퇴근시간 - 출근시간
	 * @param no, day
	 * @return String
	 */
	public String getTotalWorkTime(long no, String day, String state) {
		return commuteDao.getTotalWorkTime(no, day, state);
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 14.
	 * @brief 출퇴근 삭제
	 * @param CommuteVo
	 * @return int
	 * @throws ParseException
	 */
	public boolean removeCommuteBetween(CommuteVo vo, JwtUser userDetails) {
		int mysql = 0;
		RecordVo mongo = null;

		RecordVo recordVo = new RecordVo();

		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(String.valueOf(vo.getUserNo()));
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("출퇴근 삭제");
		recordVo.setRecordTypeEn("Commute Delete");
		recordVo.setContent(userDetails.getName() + "님이 출퇴근 삭제 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + "has remove the commute");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setKeyNo(vo.getStartNo());
		recordVo.setUserNo(userDetails.getNo());

		mysql = commuteDao.removeCommuteBetween(vo);
		mongo = recordService.insert(recordVo);
		
		if (mongo != null && (mysql == 1 || mysql == 2)) {
			return true;
		} else {
			return false;
		}
	}

	public CommuteVo getUseCommuteTime(Long no) {
		return commuteDao.getTodayByNo(no);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 15.
	 * @brief 출퇴근 상태 건수
	 * @param
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getStateCount() {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("정상", commuteDao.getStateCount("정상"));
		map.put("지각", commuteDao.getStateCount("지각"));
		map.put("조퇴", commuteDao.getStateCount("조퇴"));
		map.put("에러", commuteDao.getStateCount("에러"));
		map.put("미등록", Math.ceil(commuteDao.getStateCount("미등록")/2.0));

		return map;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 출퇴근 검색 조건에 따른 상태건수
	 * @param String, String, Long, String, boolean, String
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getSearchStateList(String startDate, String endDate, Long userNo, String searchState,
			boolean select, String userName, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("정상", commuteDao.getSearchStateCount(startDate, endDate, userNo, searchState, select, userName, "정상", keyNo));
		map.put("지각", commuteDao.getSearchStateCount(startDate, endDate, userNo, searchState, select, userName, "지각", keyNo));
		map.put("조퇴", commuteDao.getSearchStateCount(startDate, endDate, userNo, searchState, select, userName, "조퇴", keyNo));
		map.put("에러", commuteDao.getSearchStateCount(startDate, endDate, userNo, searchState, select, userName, "에러", keyNo));
		map.put("미등록", commuteDao.getSearchStateCount(startDate, endDate, userNo, searchState, select, userName, "미등록", keyNo));
		
		//System.out.println("테스트 테스트 ----->  " + map);

		return map;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 출퇴근 달력 상태건수
	 * @param
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getCalendarStateList(String searchFromDate, Long searchUserNo, String userName, String searchState, boolean select){
		String date = "";
		
		if(searchFromDate == "") {
			date = LocalDate.now().toString().substring(0, 7);
		}
		else {
			date = searchFromDate;
		}
		
		//System.out.println(commuteDao.getCalendarStateCount(searchUserNo, userName, searchState, select, date));
	
		return commuteDao.getCalendarStateCount(searchUserNo, userName, searchState, select, date);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 로그인한 사용자의 출근 정보
	 * @param no, no
	 * @return long
	 */
	public boolean getGoToData(long no, String now) {
		if (commuteDao.getCommuteData(no, "출근", now) == 1) {
			return true;
		}

		return false;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 로그인한 사용자의 퇴근 정보
	 * @param no, now
	 * @return long
	 */
	public boolean getGoOffData(long no, String now) {
		if (commuteDao.getGoOffData(no, now) == 1) {
			return true;
		}

		return false;
	}
	
	public boolean getPreGoOffData(long no) {
		if(commuteDao.getPreGoOff(no) == 1) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @author JWH
	 * @date 2019. 5. 27.
     * @brief 오늘 퇴근 후 총 근무시간조회
	 * @param totalWorkTime
	 * @return totalWorkTime
	 */
	public CommuteVo selectTodayTotalWorkTimeByNo(long no) {
		return commuteDao.selectTodayTotalWorkTimeByNo(no);
	}
	
	public CommuteVo getTodayleaveWorkByNo(long no) {
		return commuteDao.getTodayleaveWorkByNo(no);
	}
	
}

