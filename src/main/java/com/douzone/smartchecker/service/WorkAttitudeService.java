package com.douzone.smartchecker.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.douzone.security.JwtUser;
import com.douzone.smartchecker.repository.WorkAttitudeDao;
import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.WorkAttitudeVo;

/**
 * @author JYSe
 * @date 2019. 4. 23.
 * @brief 근태 서비스
 */
@Service
public class WorkAttitudeService {

	@Autowired
	WorkAttitudeDao workAttitudeDao;

	@Autowired
	RecordService recordService;

	@Autowired
	TranslationService translationService;
	
	@Autowired
	MessageSource msg;

	/**
	 * @author JYS
	 * @date 2019. 4. 24.
	 * @brief 근태신청
	 * @param 근태정보 , 유저 정보
	 * @return 근태 신청 결과
	 */
	public Object insertWorkAttitude(WorkAttitudeVo workAttitudeVo, JwtUser userDetails) {
		if (workAttitudeDao.checkInsert(workAttitudeVo) == 0) {
			if (workAttitudeDao.insert(workAttitudeVo)) {
				RecordVo recordVo = new RecordVo();

				recordVo.setNo(recordService.getLastNo() + 1);
				recordVo.setId(String.valueOf(userDetails.getNo()));
				recordVo.setActor(userDetails.getName());
				recordVo.setDay(LocalDate.now().toString());
				recordVo.setRecordType("근태 신청");
				recordVo.setRecordTypeEn("workattitude application");
				recordVo.setContent(userDetails.getName() + "님이 " + msg.getMessage(workAttitudeVo.getWorkAttitudeList(),  null, new Locale("ko")) + " 신청 하였습니다.");
				recordVo.setContentEn(userDetails.getName() + " has " + msg.getMessage(workAttitudeVo.getWorkAttitudeList(),  null, new Locale("en")) + " application");
				recordVo.setRead(false);
				recordVo.setUpdateTime(LocalDateTime.now().toString());
				recordVo.setInsertTime(LocalDateTime.now().toString());
				recordVo.setUpdateUserId(userDetails.getUsername());
				recordVo.setInsertUserId(userDetails.getUsername());
				recordVo.setKeyNo(workAttitudeDao.getLastInsertId());
				recordVo.setUserNo(userDetails.getNo());

				if (recordService.insert(recordVo) != null) {
					return true;
				}
			}
		}
		else {
			return "날짜가 겹칩니다.";
		}

		return false;

	}

	/**
	 * @author JYS
	 * @date 2019. 4. 26.
	 * @brief 근태 달력 정보
	 * @param 시간 , 근태테이블 기본키
	 * @return 근태 달력 데이터
	 */
	public List<WorkAttitudeVo> getCalendarList(Long no, String startDate, String endDate, String workAttitude) {
		return workAttitudeDao.select(no, startDate, endDate, workAttitude);
	}

	/**
	 * @author JYS
	 * @date 2019. 5. 20.
	 * @brief 근태 달력 정보
	 * @param 시간 , 이름
	 * @return 근태 달력 데이터
	 */
	public List<WorkAttitudeVo> getCalendarList(String name, String startDate, String endDate, String workAttitude) {
		return workAttitudeDao.select(name, startDate, endDate, workAttitude);
	}

	/**
	 * @author JYS
	 * @date 2019. 4. 26.
	 * @brief 근태 수정
	 * @param 근태 정보,유저 정보
	 * @return 수정 성공 여부
	 */
	public boolean modify(WorkAttitudeVo workAttitudeVo, JwtUser userDetails) {
		if (workAttitudeDao.update(workAttitudeVo)) {
			RecordVo recordVo = new RecordVo();
			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(String.valueOf(workAttitudeVo.getUserNo()));
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("근태 수정");
			recordVo.setRecordTypeEn("workattitude modify");
			recordVo.setContent(userDetails.getName() + "님이 근태 수청 하였습니다.");
			recordVo.setContentEn(userDetails.getName() + " has workattitude modify");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setKeyNo(workAttitudeVo.getNo());
			recordVo.setUserNo(userDetails.getNo());

			if (recordService.insert(recordVo) != null) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근태 현황 리스트 총 개수
	 * @param
	 * @return 리스트 총 개수
	 */
	public int getTotalCount(String startDate, String endDate, Long userNo, String searchUserName,
			String searchWorkAttitude, Long keyNo) {
		return workAttitudeDao.getTotalCount(startDate, endDate, userNo, searchUserName, searchWorkAttitude, keyNo);
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근태 현황 전체 리스트
	 * @param
	 * @return List<WorkAttitudeVo>
	 */

	public List<WorkAttitudeVo> getList(Long page, Long searchUserNo, String searchFromDate
								, String searchToDate, String searchUserName, String searchWorkAttitude, Long keyNo, Locale locale){
		return workAttitudeDao.getList(page, searchUserNo, searchFromDate, searchToDate, searchUserName, searchWorkAttitude, keyNo, locale);

	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 근태 현황 상태건수
	 * @param
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getStateCount() {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("정상", workAttitudeDao.getStateCount("정상"));
		map.put("에러", workAttitudeDao.getStateCount("에러"));

		return map;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 근태 검색 정보의 상태건수
	 * @param String, String, Long, String, String
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getSearchStateCount(String startDate, String endDate, Long userNo, String searchUserName,
			String searchWorkAttitude, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();

		//System.out.println(startDate + " " + endDate + " " + userNo + " " + searchUserName + " " + searchWorkAttitude);

		map.put("정상", workAttitudeDao.getSearchStateCount(startDate, endDate, userNo, searchUserName,
				searchWorkAttitude, "정상", keyNo));
		map.put("에러", workAttitudeDao.getSearchStateCount(startDate, endDate, userNo, searchUserName,
				searchWorkAttitude, "에러", keyNo));

		return map;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 근태 달력 상태건수
	 * @param String, String, Long, String, String, Long
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getCalendarStateCount(String startDate, Long userNo, String searchUserName,
			String searchWorkAttitude, boolean select) {
		Map<String, Object> map = new HashMap<String, Object>();
		String date = "";

		if (startDate == "") {
			date = LocalDate.now().toString().substring(0, 7);
		} else {
			date = startDate;
		}
		
		map.put("전체",
				workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "전체", date, select));
		map.put("출장",
				workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "출장", date, select));
		map.put("외근",
				workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "외근", date, select));
		map.put("연차",
				workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "연차", date, select)
						+ workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "반차", date,
								select));
		map.put("교육",
				workAttitudeDao.getCalendarStateCount(userNo, searchUserName, searchWorkAttitude, "교육", date, select));

		return map;
	}

	/**
	 * @author KJS
	 * @date 2019. 5. 18.
	 * @brief 알람 리스트
	 * @param keyNo
	 * @return List<WorkAttitudeVo>
	 */
	public List<WorkAttitudeVo> getAlarmSearchList(long keyNo) {
		//System.out.println("서비스 실행");
		return workAttitudeDao.getAlarmSearchList(keyNo);
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태 정보 가져오기
	 * @param no
	 * @return WorkTimeVo
	 */

	public WorkAttitudeVo getData(long no, Locale locale){
		return workAttitudeDao.getData(no, locale);
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 근태 수정
	 * @param WorkAttitudeVo
	 * @return int
	 */
	public Object editWorkAttitudeData(WorkAttitudeVo vo, JwtUser userDetails) {
		int mysql = 0;
		RecordVo mongo = null;
		
		RecordVo recordVo = new RecordVo();
		
		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(String.valueOf(workAttitudeDao.getUserNo(vo.getNo())));
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("근태 수정");
		recordVo.setRecordTypeEn("workattitude modify");
		recordVo.setContent(userDetails.getName() + "님이 근태 수정 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + " has workattitude modify");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setKeyNo(vo.getNo());
		recordVo.setUserNo(userDetails.getNo());
		
		mysql = workAttitudeDao.updateData(vo);
		mongo = recordService.insert(recordVo);
		
		
		if(mongo != null && mysql == 1) {
			return true;
		}
		else {
			return "날짜가 겹칩니다.";
		}
		
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태신청 삭제
	 * @param no
	 * @return boolean
	 */
	public boolean removeData(long no, JwtUser userDetails) {

		int mysql = 0;
		RecordVo mongo = null;

		RecordVo recordVo = new RecordVo();

		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(String.valueOf(workAttitudeDao.getUserNo(no)));
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("근태 삭제");
		recordVo.setRecordTypeEn("workattitude delete");
		recordVo.setContent(userDetails.getName() + "님이 근태 삭제 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + " has workattitude delete");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setKeyNo(no);
		recordVo.setUserNo(userDetails.getNo());

		mysql = workAttitudeDao.removeData(no);
		mongo = recordService.insert(recordVo);

		if (mongo != null && mysql == 1) {
			return true;
		} else {
			return false;
		}
	}
}
