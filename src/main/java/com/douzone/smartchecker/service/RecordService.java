package com.douzone.smartchecker.service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.CriteriaDefinition;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.douzone.smartchecker.repository.RecordDao;
import com.douzone.smartchecker.vo.RecordVo;

import java.util.Arrays;

@Service
@EnableAsync
public class RecordService {

	@Autowired
	private RecordDao recordDao;

	@Autowired
	private MongoOperations mongo;

	@Autowired
	private MongoTemplate mongoTemplate;

	public RecordVo insert(RecordVo recordVo) {
		return recordDao.insert(recordVo);
	}

	public Long getLastNo() {
		Query query = new Query();
		
		query.with(new Sort(Sort.Direction.DESC, "no"));
		
		RecordVo vo = mongoTemplate.findOne(query.limit(1), RecordVo.class);
		
		if(vo == null) {
			vo = new RecordVo();
			vo.setNo(1);
		}
		
		return vo.getNo();
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 8.
	 * @brief 활동기록 검색
	 * @param page, start, end, name, content
	 * @return List<RecordVo>
	 */
	public List<RecordVo> getSearch(int page, String start, String end, String name, String content, String recordType, boolean select, String searchUserNo, String read, Locale locale) {
		Pageable pageable = PageRequest.of(page - 1, 30, Sort.by(Sort.Direction.DESC, "insertTime"));
		Query query = new Query();
		
		query.with(pageable);
		
		if(locale.equals(new Locale("en"))) {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content_en").regex(content));
			}
		}
		else {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content").regex(content));
			}
		}
		
		if(start != null && start != "" && end != null && end != "") {
			query.addCriteria(Criteria.where("day").gte(start).lte(end));
		}
		if(select == false && name != null && name != "") {
			query.addCriteria(Criteria.where("actor").regex(name));
		}
		
		if(select == true && searchUserNo != null && searchUserNo != "") {
			query.addCriteria(Criteria.where("user_no").is(Long.parseLong(searchUserNo)));
		}
		if(read != null && read != "") {
			query.addCriteria(Criteria.where("read").is(Boolean.parseBoolean(read)));
		}
		if(recordType != null && recordType != "") {
			query.addCriteria(Criteria.where("record_type").regex(recordType));
		}
		
		List<RecordVo> recordList = mongoTemplate.find(query, RecordVo.class);
		
		//System.out.println(query);
		
		return recordList;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 8.
	 * @brief 활동기록 검색 리스트 개수
	 * @param start, end, name, content
	 * @return long
	 */
	public long getSearchPage(String start, String end, String name, String content, String recordType, boolean select, String searchUserNo, String read, Locale locale) {
		Query query = new Query();
		
		if(locale.equals(new Locale("en"))) {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content_en").regex(content));
			}
		}
		else {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content").regex(content));
			}
		}
		
		if(start != null && start != "" && end != null && end != "") {
			query.addCriteria(Criteria.where("day").gte(start).lte(end));
		}
		if(select == false && name != null && name != "") {
			query.addCriteria(Criteria.where("actor").regex(name));
		}
		
		if(select == true && searchUserNo != null && searchUserNo != "") {
			query.addCriteria(Criteria.where("user_no").is(Long.parseLong(searchUserNo)));
		}
		if(read != null && read != "") {
			query.addCriteria(Criteria.where("read").is(Boolean.parseBoolean(read)));
		}
		if(recordType != null && recordType != "") {
			query.addCriteria(Criteria.where("record_type").regex(recordType));
		}
		
		return mongoTemplate.find(query, RecordVo.class).size();
	}

	/**
	 * @author KJS
	 * @date 2019. 5. 23.
	 * @brief 관리자 알람 리스트 개수
	 * @param role, user_no
	 * @return List<RecordVo>
	 */
	public List<RecordVo> getAlarm(String auth, String user_no) {
		List<RecordVo> recordList = null;
		
		// ADMIN도 mongoTemplate로 변경해야함
		if (auth.equals("ROLE_ADMIN")) { // ADMIN
			// record_type이 '근태 삭제'인 데이터를 list로 받음
			/*
			List<RecordVo> removeWorkattitude = recordDao.findWorkattitudeReomove();

			Long[] removeKey = new Long[removeWorkattitude.size()];

			for (int n = 0; n < removeWorkattitude.size(); n++) {
				// record_type이 '근태 삭제'인 데이터에서 key_no를 배열로 저장
				removeKey[n] = removeWorkattitude.get(n).getKeyNo();
			}

			// key_no를 비교해서 삭제되지 않은 근태 데이터를 list에 저장
			recordList = recordDao.findByAdminAlarmList(removeKey);
			*/
			
			// where
			AggregationOperation match = Aggregation.match(Criteria.where("read").is(false).and("record_type").is("근태 신청"));

			// sort
			AggregationOperation sort = Aggregation.sort(Sort.Direction.DESC, "day");

			// group
			AggregationOperation group = Aggregation.group("key_no").last("no").as("no").last("id").as("id").last("day")
					.as("day").last("actor").as("actor").last("record_type").as("record_type").last("record_type_en").as("record_type_en").last("content")
					.as("content").last("content_en").as("content_en").last("read").as("read").last("update_time").as("update_time").last("insert_time")
					.as("insert_time").last("update_user_id").as("update_user_id").last("insert_user_id")
					.as("insert_user_id").last("key_no").as("key_no").last("user_no").as("user_no");

			Aggregation aggregation = Aggregation.newAggregation(match, group, sort);

			AggregationResults<RecordVo> result = mongoTemplate.aggregate(aggregation, "record", RecordVo.class);

			recordList = result.getMappedResults();
			

		} else if (auth.equals("ROLE_USER")) { // USER
			String[] record_type = { "출퇴근 수정", "출퇴근 삭제", "근태 수정", "근태 삭제" };

			// where
			AggregationOperation match = Aggregation.match(Criteria.where("read").is(false).and("id").is(user_no)
					.and("record_type").in(Arrays.asList(record_type)));

			// sort
			AggregationOperation sort = Aggregation.sort(Sort.Direction.DESC, "update_time");

			// group
			AggregationOperation group = Aggregation.group("key_no").last("no").as("no").last("id").as("id").last("day")
					.as("day").last("actor").as("actor").last("record_type").as("record_type").last("record_type_en").as("record_type_en").last("content")
					.as("content").last("content_en").as("content_en").last("read").as("read").last("update_time").as("update_time").last("insert_time")
					.as("insert_time").last("update_user_id").as("update_user_id").last("insert_user_id")
					.as("insert_user_id").last("key_no").as("key_no").last("user_no").as("user_no");

			Aggregation aggregation = Aggregation.newAggregation(match, group, sort);

			AggregationResults<RecordVo> result = mongoTemplate.aggregate(aggregation, "record", RecordVo.class);

			recordList = result.getMappedResults();
		}

		return recordList;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 활동기록 읽음여부 건수
	 * @param
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getStateCount() {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("읽음", recordDao.countByReadTrue());
		map.put("읽지않음", recordDao.countByReadFalse());

		return map;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 30.
	 * @brief 활동기록 검색 상태건수
	 * @param
	 * @return long
	 */
	public long getSearchStateCount(String start, String end, String name, String content, String recordType, boolean select, String searchUserNo, String read, Locale locale) {
		Query query = new Query();
		
		if(locale.equals(new Locale("en"))) {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content_en").regex(content));
			}	
		}
		else {
			if(content != null && content != "") {
				query.addCriteria(Criteria.where("content").regex(content));
			}
		}
		
		if(start != null && start != "" && end != null && end != "") {
			query.addCriteria(Criteria.where("day").gte(start).lte(end));
		}
		if(select == false && name != null && name != "") {
			query.addCriteria(Criteria.where("actor").regex(name));
		}
		
		if(select == true && searchUserNo != null && searchUserNo != "") {
			query.addCriteria(Criteria.where("user_no").is(Long.parseLong(searchUserNo)));
		}
		if(read != null && read != "") {
			query.addCriteria(Criteria.where("read").is(Boolean.parseBoolean(read)));
		}
		if(recordType != null && recordType != "") {
			query.addCriteria(Criteria.where("record_type").regex(recordType));
		}
		
		return mongoTemplate.find(query, RecordVo.class).size();
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 30.
	 * @brief 활동기록 검색 상태건수
	 * @param
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getSearchStateList(String start, String end, String name, String content, String recordType, boolean select, String searchUserNo, String read, Locale locale) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(read == "") {
			map.put("읽음", getSearchStateCount(start, end, name, content, recordType, select, searchUserNo, "true", locale));
			map.put("읽지않음", getSearchStateCount(start, end, name, content, recordType, select, searchUserNo, "false", locale));
		}
		else if(read.equals("true")) {
			map.put("읽음", getSearchStateCount(start, end, name, content, recordType, select, searchUserNo, read, locale));
			map.put("읽지않음", 0);
		}
		else {
			map.put("읽음", 0);
			map.put("읽지않음", getSearchStateCount(start, end, name, content, recordType, select, searchUserNo, read, locale));
		}
		
		return map;
	}
	

	/**
	 * @author KJS
	 * @date 2019. 5. 20.
	 * @brief 활동기록 읽음 여부 업데이트
	 * @param key_no, auth, recordType
	 * @return RecordVo
	 */ 
	public RecordVo updateRead(String key_no, String auth, String recordType) {
		
		Query query = null;
		
		if (auth.equals("ROLE_ADMIN")) { // 관리자 : 근태 신청
			query = new Query(
					Criteria.where("key_no").is(Long.parseLong(key_no))
					.and("read").is(false)
					.and("record_type").is("근태 신청"));
			
		} else if (auth.equals("ROLE_USER")) { // 사용자 : 출퇴근 수정, 출퇴근 삭제, 근태 수정, 근태 삭제
			
			if(recordType.equals("출퇴근 수정") || recordType.equals("출퇴근 삭제") || recordType.equals("출퇴근")) {

				query = new Query(
						Criteria.where("key_no").is(Long.parseLong(key_no))
						.and("read").is(false)
						.and("record_type").in("출퇴근 수정", "출퇴근 삭제"));
				
			} else if(recordType.equals("근태 수정") || recordType.equals("근태 삭제") || recordType.equals("근태")) {

				query = new Query(
						Criteria.where("key_no").is(Long.parseLong(key_no))
						.and("read").is(false)
						.and("record_type").in("근태 수정", "근태 삭제"));
			}
		}

		Update update = new Update();
		update.set("read", true);

		FindAndModifyOptions findandmodifyoptions = new FindAndModifyOptions();
		
		findandmodifyoptions.returnNew(true).upsert(true);

		RecordVo vo = mongo.findAndModify(query, update, findandmodifyoptions, RecordVo.class);

		return vo;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 20.
	 * @brief 특정 활동기록 정보
	 * @param no
	 * @return RecordVo
	 */
	public RecordVo getData(long no) {
		return recordDao.findByNo(no);
	}
}