package com.douzone.smartchecker.service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.dto.JSONResult;
import com.douzone.smartchecker.repository.BreakTimeDao;
import com.douzone.smartchecker.repository.CommuteDao;
import com.douzone.smartchecker.vo.BreakTimeVo;
import com.douzone.smartchecker.vo.CommuteVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

@Service
public class TimeService {
	@Autowired
	private WorkTimeService workTimeService;
	
	@Autowired
	private CommuteService commuteService;
	
	@Autowired
	private BreakTimeService breaktimeService;
	
	@Autowired
	private CommuteDao commuteDao;
	
	public Map<String,Object> totaltime(Long no){
//		List <BreakTimeVo> list =breaktimeService.getUseBreakTime();
//		System.out.println("$$$$$$");
//		System.out.println(list);
		String totalWorkTime =commuteService.selectTodayTotalWorkTimeByNo(no).getTotalWorktime();
		
		//System.out.println(commuteService.selectTodayTotalWorkTimeByNo(no).getTotalWorktime());
//		for(BreakTimeVo vo : list) {
//			totalWorkTime = breaktimeService.getTotalWorkTime(totalWorkTime,vo.getNo());
//		}
//		totalWorkTime = totalWorkTime.split("\\.")[0];
		

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("totalWorkTime", totalWorkTime);
		return map;
	}
	
	
	public Map<String, Object> time(Long no){
		WorkTimeVo worktimeVo = workTimeService.getUseWorkTime();
		CommuteVo commuteVo = commuteService.getUseCommuteTime(no);
		List <BreakTimeVo> list =breaktimeService.getUseBreakTime();
//		System.out.println(list);
//		for(BreakTimeVo vo : list) {
//			totalWorkTime = breaktimeService.getTotalWorkTime(totalWorkTime,vo.getNo());
//		}
//		System.out.println("+++++");
		
		String state = commuteVo.getState();
		String date = commuteVo.getDay();
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("time", worktimeVo.getStart());
		map.put("state", state);
		map.put("date", date);
		map.put("breakTimeList", list);
		
		return map;
	}
	
	
	public CommuteVo getTodayleaveWorkByNo(long no) {
		return commuteService.getTodayleaveWorkByNo(no);
	}
}