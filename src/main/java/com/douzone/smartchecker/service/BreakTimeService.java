package com.douzone.smartchecker.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.security.JwtUser;
import com.douzone.smartchecker.repository.BreakTimeDao;
import com.douzone.smartchecker.vo.BreakTimeVo;
import com.douzone.smartchecker.vo.RecordVo;

/**
 * @author JYS
 * @date 2019. 4. 24.
 * @brief WorkTime 서비스
 */

@Service
public class BreakTimeService {

	@Autowired
	private BreakTimeDao breakTimeDao;

	@Autowired
	private RecordService recordService;

	@Autowired
	private TranslationService translationService;

	public List<BreakTimeVo> getUseBreakTime() {
		return breakTimeDao.getUseBreakTime();
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 휴게 시간 설정
	 * @param start, end, description
	 * @return insert 성공 여부
	 */
	public Object insertBreakTime(String start, String end, String description, String use, JwtUser userDetails,
			Locale locale) {
		int insert;
		String descriptionEn = "";
		RecordVo mongo = null;

		if (breakTimeDao.checkeInsert(start, end) == 0) {
			if (locale.toString().equals("ko")) {
				descriptionEn = translationService.koToEn(description);
			} else if (locale.toString().equals("en")) {
				descriptionEn = description;
				description = translationService.enToKo(description);
			}

			insert = breakTimeDao.insertBreakTime(start, end, description, descriptionEn, userDetails.getUsername(),
					use);

			RecordVo recordVo = new RecordVo();

			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(userDetails.getUsername());
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("휴게시간 설정");
			recordVo.setRecordTypeEn("Set break time");
			recordVo.setContent(userDetails.getName() + "님이 휴게시간 설정 하였습니다.");
			recordVo.setContentEn(userDetails.getName() + "has set the break time");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setUserNo(userDetails.getNo());

			mongo = recordService.insert(recordVo);
		} else {
			return "시간이 겹칩니다.";
		}

		if (mongo != null && insert == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴게시간 리스트 개수
	 * @param
	 * @return int
	 */
	public int getCount() {
		return breakTimeDao.getCount();
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴게시간 전체 리스트
	 * @param page
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getList(int page) {
		return breakTimeDao.getList(page);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 사용하는 휴게시간 리스트 no
	 * @param
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getNo() {
		return breakTimeDao.getNo();
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 촐 근무시간 - 휴게시간
	 * @param totalWorkoTime, no
	 * @return String
	 */
	public String getTotalWorkTime(String totalWorkTime, long no) {
		return breakTimeDao.getTotalWorkTime(totalWorkTime, no);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 정보
	 * @param no
	 * @return BreakTimeVo
	 */
	public BreakTimeVo getData(long no) {
		return breakTimeDao.getData(no);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 수정
	 * @param no, start, end, description, use
	 * @return boolean
	 */
	public Object editData(long no, String start, String end, String description, String use, JwtUser userDetails, Locale locale) {
		int edit;
		RecordVo mongo = null;
		String descriptionEn;

		if (breakTimeDao.checkEdit(no, start, end) == 0) {
			if(locale.toString().equals("ko")) {
				descriptionEn = translationService.koToEn(description);
			}
			else {
				descriptionEn = description;
				description = translationService.enToKo(description);
			}
			
			edit = breakTimeDao.editData(no, start, end, description, descriptionEn, use, userDetails.getUsername());

			RecordVo recordVo = new RecordVo();

			recordVo.setNo(recordService.getLastNo() + 1);
			recordVo.setId(userDetails.getUsername());
			recordVo.setActor(userDetails.getName());
			recordVo.setDay(LocalDate.now().toString());
			recordVo.setRecordType("휴게시간 수정");
			recordVo.setRecordTypeEn("Modify break time");
			recordVo.setContent(userDetails.getName() + "님이 휴게시간 수정 하였습니다.");
			recordVo.setContentEn(userDetails.getName() + "has modified the break time.");
			recordVo.setRead(false);
			recordVo.setUpdateTime(LocalDateTime.now().toString());
			recordVo.setInsertTime(LocalDateTime.now().toString());
			recordVo.setUpdateUserId(userDetails.getUsername());
			recordVo.setInsertUserId(userDetails.getUsername());
			recordVo.setUserNo(userDetails.getNo());

			mongo = recordService.insert(recordVo);
		}
		else {
			return "시간이 겹칩니다.";
		}

		if (edit == 1 && mongo != null) {
			return true;
		}

		return false;
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 삭제
	 * @param no
	 * @return boolean
	 */
	public boolean removeData(long no, JwtUser userDetails) {
		int result;

		result = breakTimeDao.removeData(no);

		RecordVo recordVo = new RecordVo();

		recordVo.setNo(recordService.getLastNo() + 1);
		recordVo.setId(userDetails.getUsername());
		recordVo.setActor(userDetails.getName());
		recordVo.setDay(LocalDate.now().toString());
		recordVo.setRecordType("휴게시간 삭제");
		recordVo.setRecordTypeEn("break time remove");
		recordVo.setContent(userDetails.getName() + "님이 휴게시간 삭제 하였습니다.");
		recordVo.setContentEn(userDetails.getName() + "has deleted the break time.");
		recordVo.setRead(false);
		recordVo.setUpdateTime(LocalDateTime.now().toString());
		recordVo.setInsertTime(LocalDateTime.now().toString());
		recordVo.setUpdateUserId(userDetails.getUsername());
		recordVo.setInsertUserId(userDetails.getUsername());
		recordVo.setUserNo(userDetails.getNo());

		RecordVo mongo = recordService.insert(recordVo);

		if (result == 1 && mongo != null) {
			return true;
		}

		return false;
	}
	public BreakTimeVo getUseBreakstartTime(Long no) {
		return breakTimeDao.getUseBreakstartTime(no);
	}
}
