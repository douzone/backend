package com.douzone.smartchecker.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author JYS
 * @date 2019. 5. 28.
 * @brief 번역 기능 제공 서비스
 */

@Service
public class TranslationService {
	
	 @Value("${papago.id}")
	 private String papagoId;
	 @Value("${papago.secret}")
	 private String papagoSecret;

	public String koToEn(String ko) {
		String result="";
		 String clientId = papagoId;//애플리케이션 클라이언트 아이디값";
	        String clientSecret = papagoSecret;//애플리케이션 클라이언트 시크릿값";
	        try {
	            String text = URLEncoder.encode(ko, "UTF-8");
	            String apiURL = "https://openapi.naver.com/v1/language/translate";
	            URL url = new URL(apiURL);
	            HttpURLConnection con = (HttpURLConnection)url.openConnection();
	            con.setRequestMethod("POST");
	            con.setRequestProperty("X-Naver-Client-Id", clientId);
	            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
	            // post request
	            String postParams = "source=ko&target=en&text=" + text;
	            con.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	            wr.writeBytes(postParams);
	            wr.flush();
	            wr.close();
	            int responseCode = con.getResponseCode();
	            BufferedReader br;
	            if(responseCode==200) { // 정상 호출
	                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
	            } else {  // 에러 발생
	                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	            }
	            String inputLine;
	            StringBuffer response = new StringBuffer();
	            while ((inputLine = br.readLine()) != null) {
	                response.append(inputLine);
	            }
	            br.close();
	            //System.out.println(response.toString());
	           // System.out.println(response.toString().substring(response.toString().indexOf(",\"result\":{\"translatedText\":")+29,response.toString().indexOf("\",\"srcLangType\":")));
	            result = response.toString().substring(response.toString().indexOf(",\"result\":{\"translatedText\":")+29,response.toString().indexOf("\",\"srcLangType\":"));
	        } catch (Exception e) {
	            System.out.println(e);
	        }
	        
	        return result;
	}
	
	public String enToKo(String en) {
		String result="";
		 String clientId = papagoId;//애플리케이션 클라이언트 아이디값";
	        String clientSecret = papagoSecret;//애플리케이션 클라이언트 시크릿값";
	        try {
	            String text = URLEncoder.encode(en, "UTF-8");
	            String apiURL = "https://openapi.naver.com/v1/language/translate";
	            URL url = new URL(apiURL);
	            HttpURLConnection con = (HttpURLConnection)url.openConnection();
	            con.setRequestMethod("POST");
	            con.setRequestProperty("X-Naver-Client-Id", clientId);
	            con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
	            // post request
	            String postParams = "source=en&target=ko&text=" + text;
	            con.setDoOutput(true);
	            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	            wr.writeBytes(postParams);
	            wr.flush();
	            wr.close();
	            int responseCode = con.getResponseCode();
	            BufferedReader br;
	            if(responseCode==200) { // 정상 호출
	                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
	            } else {  // 에러 발생
	                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
	            }
	            String inputLine;
	            StringBuffer response = new StringBuffer();
	            while ((inputLine = br.readLine()) != null) {
	                response.append(inputLine);
	            }
	            br.close();
	            //System.out.println(response.toString());
	           // System.out.println(response.toString().substring(response.toString().indexOf(",\"result\":{\"translatedText\":")+29,response.toString().indexOf("\",\"srcLangType\":")));
	            result = response.toString().substring(response.toString().indexOf(",\"result\":{\"translatedText\":")+29,response.toString().indexOf("\",\"srcLangType\":"));
	        } catch (Exception e) {
	            System.out.println(e);
	        }
	        
	        return result;
	}
	
}
