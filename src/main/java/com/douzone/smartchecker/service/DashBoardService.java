package com.douzone.smartchecker.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.douzone.dto.DashBoardDto;
import com.douzone.smartchecker.repository.DashBoardDao;


@Service
public class DashBoardService {

	@Autowired
	private DashBoardDao dashBoardDao;

	/**
	 * @author JYS
	 * @date 2019. 6. 10.
	 * @brief dashboard data get
	 */
	public DashBoardDto getToday(String searchDate) {
		return dashBoardDao.getToday(searchDate);
	}
	/**
	 * @author JYS
	 * @date 2019. 6. 10.
	 * @brief dashboard 차트에 사용될 일주일치 데이터
	 */
	public List<DashBoardDto> getByDate(String searchDate) {
		
		String date[] = searchDate.split("-");
		int year=Integer.parseInt(date[0]);
		int month=Integer.parseInt(date[1]);
		int day=Integer.parseInt(date[2]);
		
        LocalDate localDateTime = LocalDate.of(year,month,day);
		
		String Date = localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date1 = localDateTime.minusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date2 = localDateTime.minusDays(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date3 = localDateTime.minusDays(3).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date4 = localDateTime.minusDays(4).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date5 = localDateTime.minusDays(5).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date6 = localDateTime.minusDays(6).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		String Date7 = localDateTime.minusDays(7).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		List<DashBoardDto> list = new ArrayList<DashBoardDto>();
		list.add(dashBoardDao.getByDate(Date1));
		list.add(dashBoardDao.getByDate(Date2));
		list.add(dashBoardDao.getByDate(Date3));
		list.add(dashBoardDao.getByDate(Date4));
		list.add(dashBoardDao.getByDate(Date5));
		list.add(dashBoardDao.getByDate(Date6));
		list.add(dashBoardDao.getByDate(Date7));
		 return list;
	}
	


}
