package com.douzone.smartchecker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.douzone.smartchecker.repository.UserDao;
import com.douzone.smartchecker.vo.UserVo;



/**
 * @author JYS
 * @date 2019. 4. 23.
 * @brief 유저 서비스 
 */
@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	/**
	 * @author JYS
	 * @date 2019. 4. 23.
	 * @brief 유저 로그인
	 */
	//@Async 비동기 처리 예제
	public UserVo login(String id,String password) {
		UserVo authuser =userDao.get(id,password);
			return authuser;
	}
	
	public List<UserVo> searchUser(String name){
		return userDao.get(name);
	}
	
	/**
	 * @author JYS
	 * @date 2019. 5. 13.
	 * @brief 중복 제거한 유저 이름 리스트
	 */
	public List<UserVo> searchUser(){
		return userDao.get();
	}
	
	/**
	 * @author JYS
	 * @date 2019. 5. 16.
	 * @brief 회원검색
	 */
	public List<UserVo> searchUser(Long no){
		return userDao.get(no);
	}

	

}
