package com.douzone.smartchecker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.douzone.smartchecker.repository.RecordDao;
import com.douzone.smartchecker.repository.TokenDao;
import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.TokenVo;


/**
 * @author JYS
 * @date 2019. 5. 15.
 * @brief 토큰 MongoDB 관련 서비스
 */
@Service
public class TokenService {
	
	@Autowired
	TokenDao tokenDao;
	
	public boolean insert(String token) {
		 TokenVo vo = new TokenVo();
		 vo.setToken(token);
		 
		 boolean isBe= false;
		 
		 for(TokenVo vo2 :tokenDao.findAll()) {
			 if(vo2.getToken().equals(token)) {
				 isBe = true;
				 break;
			 }
		 }
//		 if(isBe==false) {
//			 System.out.println("이미 존재 하는 토큰입니다.");
//			 return false;
//		 }
		 
		 //입력 예제
		 if(tokenDao.insert(vo)!=null)
			 return true;
		 
		 return false;
	}

	
	public boolean delete(String token) {
		
		long cnt = tokenDao.count();
		
		 for(TokenVo vo :tokenDao.findAll()) {
			 if(vo.getToken().equals(token)) {
				 tokenDao.deleteById(vo.getId());
				 
				 break;
			 }
		 }
		 
		 if(cnt == tokenDao.count())
			 return false;
		 
		 return true;
		
	}

}
