package com.douzone.smartchecker.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.UserVo;

/**
 * Created by stephan on 20.03.16.
 */
@Repository
public interface UserRepository extends CrudRepository<UserVo, Long> {
    UserVo findByUsername(String username);
    
    
}
