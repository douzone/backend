package com.douzone.smartchecker.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.BreakTimeVo;

@Repository
public class BreakTimeDao {
	
	@Autowired
	private SqlSession sqlSession;
	
	
	/**
	 * @author JYS
	 * @date 2019. 4. 25.
	 * @brief 근무시간
	 * @return 사용중인 휴식시간 정보  
	 */
	public List<BreakTimeVo> getUseBreakTime() {
		return sqlSession.selectList("breaktime.getByUse");
	}
	
	public BreakTimeVo getUseBreakstartTime(Long no) {
		return sqlSession.selectOne("breaktime.getUseBreakstartTime",no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 휴게시간 존재 여부 체크
	 * @param
	 * @return count(*)
	 */
	public int getCount() {
		return sqlSession.selectOne("breaktime.getCount");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 휴게 시간 등록
	 * @param start, end, description
	 * @return insert 여부
	 */
	public int insertBreakTime(String start, String end, String description,String descriptionEn, String userId, String use) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("end", end);
		map.put("description", description);
		map.put("descriptionEn", descriptionEn);
		map.put("userId", userId);
		map.put("use", Boolean.parseBoolean(use));
		
		return sqlSession.insert("breaktime.insertBreakTime", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 기존의 휴게시간 사용여부 변경
	 * @param count
	 * @return update 여부
	 */
	public int updateBreakTime(int count) {
		return sqlSession.update("breaktime.updateBreakTime", count);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴게시간 전체 리스트
	 * @param page
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getList(int page){
		int start = (page - 1) * 30;
		
		return sqlSession.selectList("breaktime.getList", start);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 사용하는 휴식 시간 리스트 no
	 * @param
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getNo(){
		return sqlSession.selectList("breaktime.getNo");
	}
	
	/**
	 * @author JYS
	 * @date 2019. 6. 3.
	 * @brief 사용하는 휴식 시간 중 현재시간보다 작은 리스트 no
	 * @param
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getNoByCurrentTime(long no){
		return sqlSession.selectList("breaktime.getNoByCurrentTime",no);
	}
	/**
	 * @author JYS
	 * @date 2019. 6. 4.
	 * @brief 사용하는 휴식 시간 중 입력시간 보다 작은 리스트 no
	 * @param
	 * @return List<BreakTimeVo>
	 */
	public List<BreakTimeVo> getNoByEntTime(String endTime,long no,String startDate,String startTime){
Map<String, Object> map = new HashMap<String, Object>();
		map.put("endTime", endTime);
		map.put("no", no);
		map.put("startDate", startDate);
		map.put("startTime",startTime);
		return sqlSession.selectList("breaktime.getNoByEndTime",map);
	}

	
	
	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 총 근무시간 - 휴게시간 
	 * @param totalWorkTime, no
	 * @return String
	 */
	public String getTotalWorkTime(String totalWorkTime, long no) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("totalWorkTime", totalWorkTime);
		map.put("no", no);
		
		return sqlSession.selectOne("breaktime.getTotalWorkTime", map);
	}
	
	public String getBreakTime(long no) {
		return sqlSession.selectOne("breaktime.getBreakTime", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 정보
	 * @param no
	 * @return BreakTimeVo
	 */
	public BreakTimeVo getData(long no) {
		return sqlSession.selectOne("breaktime.getData", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 수정
	 * @param no, start, end, description, use
	 * @return int
	 */
	public int editData(long no, String start, String end, String description, String descriptionEn, String use, String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("start", start);
		map.put("end", end);
		map.put("description", description);
		map.put("descriptionEn", descriptionEn);
		map.put("use", Boolean.parseBoolean(use));
		map.put("userId", userId);
		
		return sqlSession.update("breaktime.editData", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 삭제
	 * @param no
	 * @return int
	 */
	public int removeData(long no) {
		return sqlSession.delete("breaktime.removeData", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 29.
	 * @brief 휴게시간 날짜 체크
	 * @param String, String
	 * @return long
	 */
	public long checkeInsert(String start, String end) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("end", end);
		
		return sqlSession.selectOne("breaktime.checkInsert", map);
	}
	
	public long checkEdit(long no, String start, String end) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("start", start);
		map.put("end", end);
		
		return sqlSession.selectOne("breaktime.checkEdit", map);
	}
}
