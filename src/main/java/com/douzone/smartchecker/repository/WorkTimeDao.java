package com.douzone.smartchecker.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.WorkTimeVo;

@Repository
public class WorkTimeDao {

	
	@Autowired
	SqlSession sqlSession;
	
	/**
	 * @author JYS
	 * @date 2019. 4. 24.
	 * @brief 근무시간
	 * @return 사용중인 근무시간 정보  
	 */
	public WorkTimeVo getUseWorkTime() {
		return sqlSession.selectOne("worktime.getByUse");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 마지막 insert한 데이터 no
	 * @param
	 * @return last_insert_id()
	 */
	public int getLastInsertId() {
		return sqlSession.selectOne("worktime.getLastInsertId");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 기존 근무시간이 존재하는지 개수 체크
	 * @param
	 * @return count(*)
	 */
	public int getCount() {
		return sqlSession.selectOne("worktime.getCount");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근무 시간 추가
	 * @param start, end ,userId
	 * @return insert 성공 여부
	 */
	public int insertWorkTime(String start, String end, String userId, String use) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("end", end);
		map.put("userId", userId);
		map.put("use", Boolean.parseBoolean(use));
		
		return sqlSession.insert("worktime.insertWorkTime", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근무 시간 추가시 기존 근무시간 사용여부 변경
	 * @param
	 * @return update 여부
	 */
	public int updateWorkTime() {
		return sqlSession.update("worktime.updateWorkTime");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 근무 시간 리스트
	 * @param
	 * @return List<WorkTimeVo>
	 */
	public List<WorkTimeVo> getList(int page){
		int start = (page - 1) * 30;
		
		return sqlSession.selectList("worktime.getList", start);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 가져오기
	 * @param no
	 * @return WorkTimeVo
	 */
	public WorkTimeVo getData(long no){
		return sqlSession.selectOne("worktime.getData", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 수정
	 * @param no, start, end, use
	 * @return int
	 */
	public int editData(long no, String start, String end, String use, String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("start", start);
		map.put("end", end);
		map.put("use", Boolean.parseBoolean(use));
		map.put("userId", userId);
		
		return sqlSession.update("worktime.editData", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief true 수정시 나머지 false 변경
	 * @param no
	 * @return int
	 */
	public int updateData(long no) {
		return sqlSession.update("worktime.updateData", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 삭제
	 * @param no
	 * @return int
	 */
	public int removeData(long no) {
		return sqlSession.delete("worktime.removeData", no); 
	}
}
