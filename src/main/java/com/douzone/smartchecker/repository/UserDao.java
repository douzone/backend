package com.douzone.smartchecker.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.UserVo;


@Repository
public class UserDao{
	
	
	@Autowired
	private SqlSession sqlSession;
	
	public UserVo get(String id,String password) {
		Map<String,String> map = new HashMap<>();
		map.put("id", id);
		map.put("password", password);
		return sqlSession.selectOne("user.getByIdAndPassword",map);
	}
	
	public List<UserVo> get(String name){
		Map<String,String> map = new HashMap<>();
		map.put("name", name);
		return sqlSession.selectList("user.getByName",map);
	}
	public List<UserVo> get(){
		return sqlSession.selectList("user.getByNameDis");
	}
	
	/**
	 * @author JYS
	 * @date 2019. 5. 16.
	 * @brief 회원 검색
	 * @param 회원번호
	 * @return List<UserVO>
	 */
	public List<UserVo> get(Long no){
		return sqlSession.selectList("user.getByNo",no);
	}
	
	
	public boolean InsertUser(String id,String name) {
		Map<String,String> map = new HashMap<>();
		map.put("id", id);
		map.put("name", name);
		return 1==sqlSession.insert("user.userInsert",map);
	}
	
	public List<UserVo> getTestData(){
		return sqlSession.selectList("user.getTestDataUser");
	}
	
	
	
}