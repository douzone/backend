package com.douzone.smartchecker.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.WorkAttitudeVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

@Repository
public class WorkAttitudeDao {
	
	@Autowired
	private SqlSession sqlSession;
	
	
	
	/**
	 * @author JYS
	 * @date 2019. 4. 24.
	 * @brief 근태 입력
	 * @return 성공여부  
	 */
	public boolean insert(WorkAttitudeVo workAttitudeVo) {
		return 1==sqlSession.insert("workattitude.insertWorkAttitude", workAttitudeVo);
	}
	
	/**
	 * @author JYS
	 * @date 2019. 4. 26.
	 * @brief 근태 검색
	 * @return 검색 리스트  
	 */
	public List<WorkAttitudeVo> select(long no, String startDate,String endDate,String workAttitude) {
		Map <String,Object> map = new HashMap<String,Object>();
		map.put("no",no);
		map.put("startDate",startDate);
		map.put("endDate",endDate);
		map.put("workAttitude", workAttitude);
		return sqlSession.selectList("workattitude.getByMonthAndNo", map);
	}
	
	/**
	 * @author JYS
	 * @date 2019. 5. 20.
	 * @brief 근태 검색(like 이름검색)
	 * @return 검색 리스트  
	 */
	public List<WorkAttitudeVo> select(String name, String startDate,String endDate,String workAttitude) {
		Map <String,Object> map = new HashMap<String,Object>();
		map.put("name",name);
		map.put("startDate",startDate);
		map.put("endDate",endDate);
		map.put("workAttitude", workAttitude);
		return sqlSession.selectList("workattitude.getByMonthAndName", map);
	}
	
	public boolean update(WorkAttitudeVo workAttitudeVo) {
		return 1==sqlSession.update("workattitude.update",workAttitudeVo);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근태 현황 리스트 총 개수
	 * @param
	 * @return 리스트 총 개수
	 */
	public int getTotalCount(String startDate, String endDate, Long searchUserNo, String searchUserName, String searchWorkAttitude, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("searchUserNo", searchUserNo);
		map.put("searchUserName", searchUserName);
		map.put("searchWorkAttitude", searchWorkAttitude);
		map.put("keyNo", keyNo);
		
		return sqlSession.selectOne("workattitude.getTotalCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근태 현황 전체 리스트
	 * @param page
	 * @return List<WorkAttitudeVo>
	 */
	public List<WorkAttitudeVo> getList(Long page, Long searchUserNo, String searchFromDate
						, String searchToDate, String searchUserName, String searchWorkAttitude, Long keyNo, Locale locale){
		
		long pages = ((Number) page).longValue();
		
		long start = (pages - 1) * 30;
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("searchUserNo", searchUserNo);
		map.put("searchFromDate", searchFromDate);
		map.put("searchToDate", searchToDate);
		map.put("searchUserName", searchUserName);
		map.put("searchWorkAttitude", searchWorkAttitude);
		map.put("keyNo", keyNo);
		map.put("locale", locale.toString());
		
		return sqlSession.selectList("workattitude.getList", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 15.
	 * @brief 추가한 근태 신청의 id
	 * @param
	 * @return long
	 */
	public long getLastInsertId() {
		return sqlSession.selectOne("workattitude.getLastInsertId");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 근태 상태건수
	 * @param state
	 * @return long
	 */
	public long getStateCount(String state) {
		return sqlSession.selectOne("workattitude.getStateCount", state);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 근태 검색 정보의 상태건수
	 * @param String, String, Long, String, String, String
	 * @return long
	 */
	public long getSearchStateCount(String startDate, String endDate, Long searchUserNo, String searchUserName, String searchWorkAttitude, String inputState, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("searchUserNo", searchUserNo);
		map.put("searchUserName", searchUserName);
		map.put("searchWorkAttitude", searchWorkAttitude);
		map.put("inputState", inputState);
		map.put("keyNo", keyNo);
		
		return sqlSession.selectOne("workattitude.getSearchStateCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 근태 달력 상태건수
	 * @param String, String, Long, String, String, String, String
	 * @return long
	 */
	public long getCalendarStateCount(Long searchUserNo, String searchUserName, String searchWorkAttitude, String inputState, String date, boolean select) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("searchUserNo", searchUserNo);
		map.put("searchUserName", searchUserName);
		map.put("searchWorkAttitude", searchWorkAttitude);
		map.put("inputState", inputState);
		map.put("date", date);
		map.put("select", select);
		
		return sqlSession.selectOne("workattitude.getCalendarStateCount", map);
	}
	
	
	/**
	 * @author KJS
	 * @date 2019. 5. 18.
	 * @brief 알람 리스트
	 * @param keyNo
	 * @return List<WorkAttitudeVo>
	 */
	public List<WorkAttitudeVo> getAlarmSearchList(long keyNo) {
//		System.out.println("리턴");
//		System.out.println(sqlSession.selectList("workattitude.getAlarmSearchList", keyNo));
		return sqlSession.selectList("workattitude.getAlarmSearchList", keyNo);
	}
	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태 정보 가져오기
	 * @param no
	 * @return WorkTimeVo
	 */
	public WorkAttitudeVo getData(long no, Locale locale){
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("locale", locale.toString());

		return sqlSession.selectOne("workattitude.getData", map);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 근태 신청 수정
	 * @param WorkAttitudeVo
	 * @return int
	 */
	public int updateData(WorkAttitudeVo vo) {
		
		return sqlSession.update("workattitude.updateData", vo);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태신청 삭제
	 * @param no
	 * @return int
	 */
	public int removeData(long no) {
		return sqlSession.delete("workattitude.removeData", no); 
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 사용자 no
	 * @param long
	 * @return long
	 */
	public long getUserNo(long no) {
		return sqlSession.selectOne("workattitude.getUserNo", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019.5.29
	 * @brief insert check
	 * @param vo
	 * @return long
	 */
	public long checkInsert(WorkAttitudeVo vo) {
		return sqlSession.selectOne("workattitude.checkInsert", vo);
	}
	
	public boolean insertTestData(WorkAttitudeVo vo) {
		return 1==sqlSession.insert("workattitude.insertTestData", vo);
	}
	
}
