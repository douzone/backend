package com.douzone.smartchecker.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.dto.CalendarDto;
import com.douzone.smartchecker.vo.BreakTimeVo;
import com.douzone.smartchecker.vo.CommuteVo;

@Repository
public class CommuteDao {
	
	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private BreakTimeDao breaktimeDao;


	
	public List<CalendarDto> selectByMonth(int month){
		return sqlSession.selectList("commute.getByMonth", month);
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 22.
	 * @brief 출근 등록
	 * @param no, id
	 * @return insert 여부 
	 */
	public int insertCommute(long no, String id, String commute, String state,String totalWorkTime) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		//String totalWorkTime = "00:00";
		
//		if(commute.equals("퇴근")){
//			//현재시간 - 출근시간
////			totalWorkTime = getTotalWorkTime(no, getCommuteDay(no), getGoToState(no));
////			
////			totalWorkTime = getSubBreakTime(totalWorkTime,no);
//			
//		}
		
		map.put("totalWorkTime", totalWorkTime);
		map.put("no", no);
		map.put("id", id);
		map.put("commute", commute);
		map.put("state", state);
		
		//System.out.println(no + " " + id + " " + commute + " " + state);
		//System.out.println(sqlSession.getConfiguration().getMappedStatement("commute.insertCommute").getSqlSource().getBoundSql("commute.insertCommute").getSql());
		
		return sqlSession.insert("commute.insertCommute", map);
	} 
	
	/**
	 * @author PSH
	 * @date 2019. 6. 12.
	 * @brief 출근 상태
	 * @param no
	 * @return 출근시 상태 
	 */
	public String getGoToState(long no) {
		return sqlSession.selectOne("commute.getGoToState", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 13.
	 * @brief 총 근무시간에서 휴계시간 뺀 시간
	 * @param totalWorkTime
	 * @return totalWorkTime
	 */
	public String getSubBreakTime (String totalWorkTime,long no) {
		
		List<BreakTimeVo> breakTimeList = breaktimeDao.getNoByCurrentTime(no);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		
		if(breakTimeList != null) {
			for(BreakTimeVo vo : breakTimeList) {
				try {
					//System.out.println("total : " + totalWorkTime);
					//long time = sdf.parse(breaktimeDao.getTotalWorkTime(totalWorkTime, vo.getNo())).getTime();
					
					long time = sdf.parse(totalWorkTime).getTime() - sdf.parse(breaktimeDao.getBreakTime(vo.getNo())).getTime();
					
//					System.out.println(totalWorkTime);
//					System.out.println(sdf.parse(totalWorkTime).getTime());
//					System.out.println(sdf.parse(breaktimeDao.getBreakTime(vo.getNo())).getTime());
//					System.out.println(breaktimeDao.getBreakTime(vo.getNo()));
					
					long hour = (time / (1000* 60 * 60)) % 24 < 0 ? 0 : (time/(1000*60*60));
					long minute = (time / (1000 * 60)) % 60 < 0 ? 0 : (time/(1000*60)) % 60;
					long second = (time / 1000) % 60 < 0 ? 0 : (time/1000) % 60;
					
					totalWorkTime = String.format("%02d:%02d:%02d", hour, minute, second);
					
					//System.out.println("total : " + totalWorkTime);
					//totalWorkTime = totalWorkTime.substring(0, totalWorkTime.lastIndexOf(":"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			totalWorkTime = totalWorkTime.substring(0, totalWorkTime.lastIndexOf(":"));
		}
		else {
			totalWorkTime = "00:00";
		}		
		
		return totalWorkTime;
	}
	
	/**
	 * @author JYS
	 * @date 2019. 6. 04.
	 * @brief 총 근무시간에서 입력시간 이후의 휴계시간 뺀 시간
	 * @param totalWorkTime
	 * @return totalWorkTime
	 */
	public String getSubBreakTime (String totalWorkTime,String endTime,long no,String startDate,String startTime) {
		
		List<BreakTimeVo> breakTimeList = breaktimeDao.getNoByEntTime(endTime,no,startDate,startTime);
//		System.out.println(breakTimeList.size());
//		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@");
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		//System.out.println(totalWorkTime);
		
		if(breakTimeList != null) {
			for(BreakTimeVo vo : breakTimeList) {
				try {
					//System.out.println("total : " + totalWorkTime);
					//long time = sdf.parse(breaktimeDao.getTotalWorkTime(totalWorkTime, vo.getNo())).getTime();
					
					long time = sdf.parse(totalWorkTime).getTime() - sdf.parse(breaktimeDao.getBreakTime(vo.getNo())).getTime();
					
//					System.out.println(totalWorkTime);
//					System.out.println(sdf.parse(totalWorkTime).getTime());
//					System.out.println(sdf.parse(breaktimeDao.getBreakTime(vo.getNo())).getTime());
//					System.out.println(breaktimeDao.getBreakTime(vo.getNo()));
					
					long hour = (time / (1000* 60 * 60)) % 24 < 0 ? 0 : (time/(1000*60*60));
					long minute = (time / (1000 * 60)) % 60 < 0 ? 0 : (time/(1000*60)) % 60;
					long second = (time / 1000) % 60 < 0 ? 0 : (time/1000) % 60;
					
					totalWorkTime = String.format("%02d:%02d:%02d", hour, minute, second);
					
//					System.out.println("total : " + totalWorkTime);
					//totalWorkTime = totalWorkTime.substring(0, totalWorkTime.lastIndexOf(":"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			totalWorkTime = totalWorkTime.substring(0, totalWorkTime.lastIndexOf(":"));
		}
		else {
			totalWorkTime = "00:00";
		}		
		
		return totalWorkTime;
	}
	
	
	/**
	 * @author JYS
	 * @date 2019. 4. 23.
	 * @brief 출근 조회
	 * @param month,id
	 * @return 회원 달력 list  
	 */
		public List<CommuteVo> selectByMonthAndNo(String no,String startDate,String endDate,String state){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("state",state);
			if(!no.equals("0")) {
			map.put("no",no);
			return sqlSession.selectList("commute.getByMonthAndNo", map);
			}
			else {
				return sqlSession.selectList("getAllListByMonth",map);
			}
		}
		
		/**
		 * @author JYS
		 * @date 2019. 5. 16.
		 * @brief 출근 조회
		 * @param month,name
		 * @return 회원 달력 list  
		 */
			public List<CommuteVo> selectByMonthAndName(String name,String startDate,String endDate,String state){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("startDate", startDate);
				map.put("endDate", endDate);
				map.put("state",state);
				if(!name.equals("")) {
				map.put("name",name);
				return sqlSession.selectList("commute.getByMonthAndName", map);
				}
				else {
					return sqlSession.selectList("getAllListByMonth",map);
				}
			}	

	
	/**
	 * @author PSH
	 * @date 2019. 4. 23.
	 * @brief 출퇴근 현황 전체 리스트
	 * @param
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getList(Long page, Long userNo, String startDate, String endDate, String state, boolean select, String userName, Long keyNo){
		long pages = ((Number) page).longValue();
		
		long start = (pages - 1) * 30;
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("userNo", userNo);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("state", state);
		map.put("select", select);
		map.put("userName", userName);
		map.put("keyNo", keyNo);
		
		return sqlSession.selectList("commute.getByfindFull", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 검색 리스트
	 * @param name, startDate, endDate 
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getSearch(int page, Long userNo, String startDate, String endDate){
		int start = (page - 1) * 30;
		Map<String, Object> map = new HashMap<String, Object>();
		
		// map.put("name", "%".concat(name).concat("%"));
		map.put("start", start);
		map.put("userNo", userNo);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		
		return sqlSession.selectList("commute.searchCommute", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 현황 날짜 검색
	 * @param startDate, endDate
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getByDate(int page, String startDate, String endDate){
		int start = (page - 1) * 30;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		
		return sqlSession.selectList("commute.getByDate", map);
	}
	
	
	/**
	 * @author PSH
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 현황 이름 검색
	 * @param name
	 * @return List<CommuteVo>
	 */
	public List<CommuteVo> getByNo(int page, long no){
		int start = (page - 1) * 30;
		
		// name = "%".concat(name).concat("%");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("start", start);
		map.put("no", no);
		
		return sqlSession.selectList("commute.getByNo", map);
	}
	
	/**
	 * @author JYS
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 기록 수정
	 * @param commuteVo
	 * @return 수정 성공 여부
	 */
	public boolean update(CommuteVo commuteVo) {
		return 1==sqlSession.update("commute.update", commuteVo);
	}
	
	
	/**
	 * @author JYS
	 * @date 2019. 4. 25.
	 * @brief 출근 시간 데이터 GET
	 * @param Date,no
	 * @return commuteVo
	 */
	public CommuteVo getGotoWorkDataByDateAndNo(String Date,Long no) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("no", no);
		String startDate = Date + " 00:00:00";
		String endDate = Date + " 23:59:00";
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		
		List <CommuteVo> list = sqlSession.selectList("commute.getByDateAndNo",map);
		for(CommuteVo vo : list) {
			if(vo.getCommute().equals("출근"))
				return vo;
		}
		return null;
	}
	
	/**
	+	 * @author PSH
	+	 * @date 2019. 4. 26.
	+	 * @brief 출퇴근 전체 리스트 개수
	+	 * @param
	+	 * @return int
	+	 */
	public int getTotalCount() {
		return sqlSession.selectOne("commute.getTotalCount");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 출퇴근 검색 - 날짜
	 * @param startDate, endDate
	 * @return int
	 */
	public int getDateCount(String startDate, String endDate) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		
		return sqlSession.selectOne("commute.getDateCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 출퇴근 검색 - 사용자 번호
	 * @param userNo
	 * @return int
	 */
	public int getNoCount(long no) {
		return sqlSession.selectOne("commute.getNoCount", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 출퇴근 검색 - 날짜 + 사용자 번호
	 * @param startDate, endDate, userNo
	 * @return int
	 */
	public int getSearchCount(String startDate, String endDate, long no) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("no", no);
		
		return sqlSession.selectOne("commute.getSearchCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 출퇴근 검색 - 날짜 + 사용자 번호
	 * @param startDate, endDate, userNo
	 * @return int
	 */
	public int getSearchFullCount(String startDate, String endDate, Long userNo, String searchState, boolean select, String userName, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("userNo", userNo);
		map.put("searchState", searchState);
		map.put("select", select);
		map.put("userName", userName);
		map.put("keyNo", keyNo);
		
		return sqlSession.selectOne("commute.getSearchFullCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 오늘의 출퇴근 유무
	 * @param no, commute, today
	 * @return int
	 */
	public int getCommuteCount(long no, String today) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("today", today);
		
		return sqlSession.selectOne("commute.getCommuteCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 6. 11.
	 * @brief 퇴근 유무
	 * @param no
	 * @return long
	 */
	public int getGoOffCount(long no) {
		return sqlSession.selectOne("commute.getGoOffCount", no);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 8.
	 * @brief 출퇴근 상세 조회
	 * @param startNo, endNo
	 * @return CommuteVo
	 */
	public CommuteVo getSearchBetween(long startNo, long endNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startNo", startNo);
		map.put("endNo", endNo);
		
		return sqlSession.selectOne("commute.getSearchBetween", map);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 13.
	 * @brief 출퇴근 수정
	 * @param CommuteVo
	 * @return int
	 */
	public int editCommuteBetween(CommuteVo vo) {
		
		//퇴근시간 - 출근시간
		String totalWorktime = getEditTotalWorkTime(vo);
//		System.out.println("bbb");
//		System.out.println(getEditTotalWorkTime(vo));
//		System.out.println(totalWorktime);
		//총시간 - 휴게시간
		totalWorktime = getSubBreakTime(totalWorktime,vo.getEndTime(),vo.getUserNo(),vo.getStartDate(),vo.getStartTime());
		
		vo.setTotalWorktime(totalWorktime);
		
		//System.out.println(sqlSession.getConfiguration().getMappedStatement("commute.editCommuteBetween").getSqlSource().getBoundSql("commute.editCommuteBetween").getSql());
		
		return sqlSession.update("commute.editCommuteBetween", vo);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 9.
	 * @brief 퇴근시간 - 출근시간
	 * @param no, day
	 * @return String
	 */
	public String getTotalWorkTime(long no, String day, String state) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("day", day);
		map.put("state", state);
		
		return sqlSession.selectOne("commute.getTotalWorkTime", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 13.
	 * @brief 쵝근 출근한 날짜
	 * @param no
	 * @return string
	 */
	public String getCommuteDay(long no) {
		return sqlSession.selectOne("commute.getCommuteDay", no);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 13.
	 * @brief 출퇴근 수정 퇴근시간 - 출근시간
	 * @param no, day
	 * @return String
	 */
	public String getEditTotalWorkTime(CommuteVo vo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", vo.getUserNo());
		map.put("fromState", vo.getFromState());
		map.put("startDate", vo.getStartDate());
		map.put("endDate", vo.getEndDate());
		map.put("endTime", vo.getEndTime());
		map.put("startTime", vo.getStartTime());
		
		//System.out.println(sqlSession.getConfiguration().getMappedStatement("commute.getEditTotalWorkTime").getSqlSource().getBoundSql("commute.getEditTotalWorkTime").getSql());
		
		return sqlSession.selectOne("commute.getEditTotalWorkTime", map);
	}
	
	/**
	 * @author JYD
	 * @date 2019. 5. 14.
	 * @brief 출퇴근 삭제
	 * @param CommuteVo
	 * @return int
	 */
	public int removeCommuteBetween(CommuteVo vo) {
		
		//System.out.println(sqlSession.getConfiguration().getMappedStatement("commute.editCommuteBetween").getSqlSource().getBoundSql("commute.editCommuteBetween").getSql());
		
		return sqlSession.update("commute.removeCommuteBetween", vo);
	}
	
	public CommuteVo getTodayByNo(Long no) {
		return sqlSession.selectOne("commute.getTodayByNo",no);
	}
	
	public String getStartDate(long userNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("userNo", userNo);
		
		return sqlSession.selectOne("commute.getStartDate", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 15.
	 * @brief 출퇴근 상태 건수
	 * @param String
	 * @return long
	 */
	public long getStateCount(String state){
		return sqlSession.selectOne("commute.getStateCount", state);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 출퇴근 검색 상태건수
	 * @param String, String, Long, String, boolean, String, String
	 * @return long
	 */
	public long getSearchStateCount(String startDate, String endDate, Long userNo, String searchState, boolean select, String userName, String inputState, Long keyNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("userNo", userNo);
		map.put("state", searchState);
		map.put("select", select);
		map.put("userName", userName);
		map.put("inputState", inputState);
		map.put("keyNo", keyNo);
		
		return sqlSession.selectOne("commute.getSearchStateCount", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 출퇴근 달력 상태건수
	 * @param long, String
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getCalendarStateCount(Long userNo, String userName, String searchState, boolean select, String date){
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("userNo", userNo);
		map.put("state", searchState);
		map.put("select", select);
		map.put("userName", userName);
		map.put("date", date);
		
		return sqlSession.selectOne("commute.getCalendarState", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 로그인한 사용자 출퇴근 정보
	 * @param no, commute, now
	 * @return Map<String, Object>
	 */
	public long getCommuteData(long no, String commute, String now) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("commute", commute);
		map.put("now", now);
		
		//System.out.println(sqlSession.getConfiguration().getMappedStatement("commute.getCommuteData").getSqlSource().getBoundSql("commute.getCommuteData").getSql());
		
		return sqlSession.selectOne("commute.getCommuteData", map);
	}
	
	public long getGoOffData(long no, String now) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("now", now);
		
		return sqlSession.selectOne("commute.getGoOffData", map);
	}
	
	public long getLastInsertId() {
		return sqlSession.selectOne("commute.getLastInsertId");
	}
	
	public long getPreGoOff(long no) {
		return sqlSession.selectOne("commute.getPreGoOff", no);
	}
	
	/**
	 * @author JWH
	 * @date 2019. 5. 28.
	 * @brief 오늘 퇴근 후 총 근무시간조회
	 * @param totalWorkTime
	 * @return totalWorkTime
	 */
	public CommuteVo selectTodayTotalWorkTimeByNo(Long no) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("no",no);
		
		List <CommuteVo> list = sqlSession.selectList("commute.getByTodayTotalWorkTime",map);
		
		for(CommuteVo vo : list) {
			
			return vo;
		}
		return null;
	}
	
	public CommuteVo getTodayleaveWorkByNo(long no) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("no",no);
		
		List <CommuteVo> list = sqlSession.selectList("commute.getTodayleaveWorkByNo",map);
		
		for(CommuteVo vo : list) {
			
			return vo;
		}
		return null;
	}
	
	
	public boolean testDataInput(CommuteVo vo) {
		return 1==sqlSession.insert("commute.InsertTestData",vo);

	}
	public boolean testDataInput2(CommuteVo vo) {
		return 1==sqlSession.insert("commute.InsertTestData2",vo);

	}
	
	public CommuteVo getTestDate(long no,long groupNo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("no",no);
		map.put("groupNo",groupNo);
		return sqlSession.selectOne("commute.getTestDate",map);

	}
	
	

}

