package com.douzone.smartchecker.repository;

import java.util.HashSet;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.douzone.smartchecker.vo.RecordVo;



/**
 * @author PSH
 * @Date 2019. 4. 22.
 * @brief 활동 기록 DB 관리 - MongoDB
 *
 */
@Repository
public interface RecordDao extends MongoRepository<RecordVo, Object> {
	
	// 활동기록 검색과 상관없이 필요한 함수// 
	public RecordVo findByNo(long no);
	public Page<RecordVo> findAll(Pageable pageable);
	//////////
	
	/*
	@Query("{$cond: {if: {$and: [{$where: '?0 != null'}, {$where: '?0 != \"\"'}, {$where: '?1 != null'}, {$where: '?1 != \"\"'}]}, then: {'day': {$gte: ?0, $lt: ?1}}}}, "
			+ "{$cond: {if: {$and: [{$where: '?2 != null'}, {$where: '?2 != \"\"'}]}, then: {'actor': {$regex: ?2}}}}, "
			+ "{$cond: {if: {$and: [{$where: '?3 != null'}, {$where: '?3 != \"\"'}]}, then: {'content': {$regex: ?3}}}}, "
			+ "{$cond: {if: {$and: [{$where: '?4 != null'}, {$where: '?4 != \"\"'}]}, then: {'record_type': {$regex: ?4}}}}, "
			+ "{$cond: {if:{$and: [{$where: '?5 != null'}, {$where: '?5 != \"\"'}]}, then: {'user_no': {$eq: ?5}}}}")
	public Page<RecordVo> findAll(String start, String end, String name, String content, String recordType, String searchUserNo, Pageable pageable);
	*/
	
	// name like
	public Page<RecordVo> findByActorRegex(String actor, Pageable pageable);
	
	@Query("{'day': {$gte: ?0, $lte: ?1}}")
	public Page<RecordVo> findByDayBetween(String start, String end, Pageable pageable);
	
	public Page<RecordVo> findByContentRegex(String content, Pageable pageable);
	public Page<RecordVo> findByRecordTypeRegex(String recordType, Pageable pageable);
	
	@Query("{$and: [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}]}")
	public Page<RecordVo> findByActorDay(String name, String start, String end, Pageable pageable);
	
	@Query("{$and: [{'content': {$regex: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}")
	public Page<RecordVo> findByContentDay(String content, String start, String end, Pageable pageable);
	
	@Query("{$and: [{'actor': {$regex: ?0}}, {'content': {$regex: ?1}}]}")
	public Page<RecordVo> findByActorContent(String name, String content, Pageable paeable);
	
	@Query("{$and: [{'record_type': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}]}")
	public Page<RecordVo> findByRecordTypeDay(String recordType, String start, String end, Pageable pageable);
	
	@Query("{$and: [{'actor': {$regex: ?0}}, {'record_type': {$regex: ?1}}]}")
	public Page<RecordVo> findByActorRecordTypeRegex(String name, String recordType, Pageable pageable);

	@Query("{$and: [{'content': {$regex: ?0}}, {'record_type': {$regex: ?1}}]}")
	public Page<RecordVo> findByContentRecordTypeRegex(String content, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}")
	public Page<RecordVo> findByActorDayContent(String actor, String start, String end, String content, Pageable pageable);
	
	@Query("{'$and': [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}")	
	public Page<RecordVo> findByActorDayRecordType(String actor, String start, String end, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'content': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}")	
	public Page<RecordVo> findByContnetDayRecordType(String content, String start, String end, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'content': {$regex: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}")	
	public Page<RecordVo> findByContnetActorRecordType(String content, String actor, String recordType, Pageable pageable);

	@Query("{'$and': [{'content': {$regex: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'day':{$gte: ?3, $lte: ?4}}]}")	
	public Page<RecordVo> findByDayActorContentRecordType(String content, String actor, String recordType, String start, String end, Pageable pageable);
	
	public Page<RecordVo> findByRead(Boolean read, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}")
	public Page<RecordVo> findByReadDay(Boolean read, String start, String end, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}]}")
	public Page<RecordVo> findByReadActor(Boolean read, String actor, Pageable pageable);

	@Query("{'$and': [{'read': {$eq: ?0}}, {'content': {$regex: ?1}}]}")
	public Page<RecordVo> findByReadContent(Boolean read, String content, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'record_type': {$regex: ?1}}]}")
	public Page<RecordVo> findByReadRecordType(Boolean read, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'actor': {$regex: ?3}}]}")
	public Page<RecordVo> findByReadDayActor(Boolean read, String start, String end, String actor, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}")
	public Page<RecordVo> findByReadDayContent(Boolean read, String start, String end, String content, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}")
	public Page<RecordVo> findByReadRecordTypeDay(Boolean read, String start, String end, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}, {'content': {$regex: ?2}}]}")
	public Page<RecordVo> findByReadActorContent(Boolean read, String actor, String content, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}")
	public Page<RecordVo> findByReadContentRecordType(Boolean read, String content, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}")
	public Page<RecordVo> findByReadActorRecordType(Boolean read, String actor, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'actor': {$regex: ?3}}, {'content': {$regex: ?4}}]}")
	public Page<RecordVo> findByReadDayActorContent(Boolean read, String start, String end, String actor, String content, Pageable pageable);
	
	@Query("{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'actor': {$regex: ?2}}, {'record_type': {$regex: ?3}}, {'read': {$eq: ?4}}]}")
	public Page<RecordVo> findByDayActorRecordTypeRead(String start, String end, String name, String recordType, Boolean read, Pageable pageable);
	
	@Query("{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'content': {$regex: ?2}}, {'record_type': {$regex: ?3}}, {'read': {$eq: ?4}}]}")
	public Page<RecordVo> findByDayContentRecordTypeRead(String start, String end, String content, String recordType, Boolean read, Pageable pageable);
	
	@Query("{'$and': [{'actor': {$regex: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'read': {$eq: ?3}}]}")
	public Page<RecordVo> findByActorContentRecordTypeRead(String actor, String content, String recordType, Boolean read, Pageable pageable);
	
	@Query("{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'actor': {$regex: ?2}}, {'content': {$regex: ?3}}, {'record_type': {$regex: ?4}}, {'read': {$eq: ?5}}]}")
	public Page<RecordVo> findAllSearch(String start, String end, String actor, String content, String recordType, Boolean read, Pageable pageable);
	
	
	
	
	
	
	
	
	
	// no
	public Page<RecordVo> findByUserNo(Long userNo, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}")
	public Page<RecordVo> findByUserNoDay(Long userNo, String start, String end, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'record_type': {$regex: ?1}}]}")
	public Page<RecordVo> findByUserNoRecordType(Long userNo, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'content': {$regex: ?1}}]}")
	public Page<RecordVo> findByUserNoContent(Long userNo, String content, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}")
	public Page<RecordVo> findByUserNoDayContent(Long userNo, String start, String end, String content, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}")
	public Page<RecordVo> findByUserNoDayRecordType(Long userNo, String start, String end, String recordType, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'record_type': {$regex: ?1}}, {'content': {$regex: ?2}}]}")
	public Page<RecordVo> findByUserNoRecordTypeContent(Long userNo, String recordType, String content, Pageable pageable);
	
	@Query("{'$and': [{'user_no': {$eq: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'day':{$gte: ?3, $lte: ?4}}]}")	
	public Page<RecordVo> findByUserNoDayContentRecordType(Long userNo, String content, String recordType, String start, String end, Pageable pageable);
	
	
	
	
	
	
	
	// name like
	@Query(value="{'day': {$gte: ?0, $lte: ?1}}", count=true)
	public long countByDayBetween(String start, String end);
	
	public long countByActorRegex(String actor);
	public long countByContentRegex(String content);
	public long countByRecordTypeRegex(String recordType);
	
	@Query(value="{$and: [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}]}", count=true)
	public long countByActorDay(String name, String start, String end);
	
	@Query(value="{$and: [{'content': {$regex: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}", count=true)
	public long countByContentDay(String content, String start, String end);
	
	@Query(value="{$and: [{'actor': {$regex: ?0}}, {'content': {$regex: ?1}}]}", count=true)
	public long countByActorContent(String name, String content);
	
	@Query(value="{$and: [{'record_type': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}]}", count=true)
	public long countByRecordTypeDay(String recordType, String start, String end);
	
	@Query(value="{$and: [{'actor': {$regex: ?0}}, {'record_type': {$regex: ?1}}]}", count=true)
	public long countByActorRecordTypeRegex(String name, String recordType);
	
	@Query(value="{$and: [{'content': {$regex: ?0}}, {'record_type': {$regex: ?1}}]}", count=true)
	public long countByContentRecordTypeRegex(String content, String recordType);
	
	@Query(value="{'$and': [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}", count=true)
	public long countByActorDayContent(String actor, String start, String end, String content);
	
	@Query(value="{'$and': [{'actor': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}", count=true)	
	public long countByActorDayRecordType(String actor, String start, String end, String recordType);
	
	@Query(value="{'$and': [{'content': {$regex: ?0}}, {'day':{$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}", count=true)	
	public long countByContnetDayRecordType(String content, String start, String end, String recordType);
	
	@Query(value="{'$and': [{'content': {$regex: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}", count=true)	
	public long countByContnetActorRecordType(String content, String actor, String recordType);

	@Query(value="{'$and': [{'content': {$regex: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'day':{$gte: ?3, $lte: ?4}}]}", count=true)	
	public long countByDayActorContentRecordType(String content, String actor, String recordType, String start, String end);
	
	@Query(value="{'$and': [{'read': {$eq: false}}, {'record_type': {$eq: '근태 신청'}}, {'key_no': {$nin: ?0}}]}", sort="{insert_time : -1}")
	public List<RecordVo> findByAdminAlarmList(Long[] key_no);
	
	@Query(value="{'record_type': {$eq: '근태 삭제'}}", count=false)
	public List<RecordVo> findWorkattitudeReomove();
	
	@Query(value="{'$and': [{'record_type': {$eq: '근태 삭제'}}]}", fields="{'key_no': 1}", count=false)
	public List<String> findReomove();
	
	@Query(value="{'$and': [{'read': {$eq: false}}, {'actor': {$regex: ?0}}, {'record_type': {$eq: '출퇴근 수정'}}, {'record_type': {$eq: '근태 수정'}}]}", sort="{day : -1}", count=false)
	public List<RecordVo> findByUserAlarmList(String name);
	
	@Query(value="{'read': {$eq: true}}", count=true)
	public long countByReadTrue();
	
	@Query(value="{'read': {$eq: false}}", count=true)
	public long countByReadFalse();
	
	public long countByRead(Boolean read);

	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}", count=true)
	public long countByReadDay(Boolean read, String start, String end);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}]}", count=true)
	public long countByReadActor(Boolean read, String actor);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'content': {$regex: ?1}}]}", count=true)
	public long countByReadContent(Boolean read, String content);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'record_type': {$regex: ?1}}]}", count=true)
	public long countByReadRecordType(Boolean read, String recordType);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte:?2}}, {'actor': {$regex: ?3}}]}", count=true)
	public long countByReadDayActor(Boolean read, String start, String end, String actor);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}", count=true)
	public long countByReadDayContent(Boolean read, String start, String end, String content);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}", count=true)
	public long countByReadRecordTypeDay(Boolean read, String start, String end, String recordType);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}, {'content': {$regex: ?2}}]}", count=true)
	public long countByReadActorContent(Boolean read, String actor, String content);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}", count=true)
	public long countByReadContentRecordType(Boolean read, String content, String recordType);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'actor': {$regex: ?1}}, {'record_type': {$regex: ?2}}]}", count=true)
	public long countByReadActorRecordType(Boolean read, String actor, String recordType);
	
	@Query(value="{'$and': [{'read': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'actor': {$regex: ?3}}, {'content': {$regex: ?4}}]}", count=true)
	public long countByReadDayActorContent(Boolean read, String start, String end, String actor, String content);
	
	@Query(value="{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'actor': {$regex: ?2}}, {'record_type': {$regex: ?3}}, {'read': {$eq: ?4}}]}", count=true)
	public long countByDayActorRecordTypeRead(String start, String end, String name, String recordType, Boolean read);
	
	@Query(value="{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'content': {$regex: ?2}}, {'record_type': {$regex: ?3}}, {'read': {$eq: ?4}}]}", count=true)
	public long countByDayContentRecordTypeRead(String start, String end, String content, String recordType, Boolean read);
	
	@Query(value="{'$and': [{'actor': {$regex: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'read': {$eq: ?3}}]}", count=true)
	public long countByActorContentRecordTypeRead(String actor, String content, String recordType, Boolean read);
	
	@Query(value="{'$and': [{'day': {$gte: ?0, $lte: ?1}}, {'actor': {$regex: ?2}}, {'content': {$regex: ?3}}, {'record_type': {$regex: ?4}}, {'read': {$eq: ?5}}]}", count=true)
	public long countAllSearch(String start, String end, String actor, String content, String recordType, Boolean read);
	
	
	
	
	
	
	
	
	// no
	public long countByUserNo(Long userNo);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}]}", count=true)
	public long countByUserNoDay(Long userNo, String start, String end);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'record_type': {$regex: ?1}}]}", count=true)
	public long countByUserNoRecordType(Long userNo, String recordType);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'content': {$regex: ?1}}]}", count=true)
	public long countByUserNoContent(Long userNo, String content);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'content': {$regex: ?3}}]}", count=true)
	public long countByUserNoDayContent(Long userNo, String start, String end, String content);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'day': {$gte: ?1, $lte: ?2}}, {'record_type': {$regex: ?3}}]}", count=true)
	public long countByUserNoDayRecordType(Long userNo, String start, String end, String recordType);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'record_type': {$regex: ?1}}, {'content': {$regex: ?2}}]}", count=true)
	public long countByUserNoRecordTypeContent(Long userNo, String recordType, String content);
	
	@Query(value="{'$and': [{'user_no': {$eq: ?0}}, {'content': {$regex: ?1}}, {'record_type': {$regex: ?2}}, {'day':{$gte: ?3, $lte: ?4}}]}", count=true)	
	public long countByUserNoDayContentRecordType(Long userNo, String content, String recordType, String start, String end);
}
