package com.douzone.smartchecker.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.RecordVo;
import com.douzone.smartchecker.vo.TokenVo;



/**
 * @author JYS
 * @Date 2019. 5.13
 * @brief token DB 관리 - MongoDB
 *
 */
@Repository
public interface TokenDao extends MongoRepository<TokenVo, Object> {


	
}
