package com.douzone.smartchecker.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.smartchecker.vo.HoliDayVo;

/**
 * @author PSH
 * @Date 2019. 5. 3.
 * @brief 휴일 설정 Repository
 *
 */
@Repository
public class HolidayDao {

	@Autowired
	private SqlSession sqlSession;
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 등록
	 * @param day, description, userId
	 * @return insert 여부
	 */
	public int insert(String day, String description,String descriptionEn, String userId, String use) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("day", day);
		map.put("description", description);
		map.put("descriptionEn", descriptionEn);
		map.put("userId", userId);
		map.put("use", Boolean.parseBoolean(use));
		
		return sqlSession.insert("holiday.insert", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 전체 리스트 개수
	 * @param
	 * @return int
	 */
	public int getCount() {
		return sqlSession.selectOne("holiday.getCount");
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 전체 리스트
	 * @param page
	 * @return List<HoliDayVo>
	 */
	public List<HoliDayVo> getList(int page) {
		int start = (page - 1) * 30;
		
		return sqlSession.selectList("holiday.getList", start);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 정보
	 * @param no
	 * @return HoliDayVo
	 */
	public HoliDayVo getData(long no) {
		return sqlSession.selectOne("holiday.getData", no);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 수정
	 * @param no, day, description, use
	 * @return int
	 */
	public int editData(long no, String day, String description, String descriptionEn, String use, String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("no", no);
		map.put("day", day);
		map.put("description", description);
		map.put("descriptionEn", descriptionEn);
		map.put("use", Boolean.parseBoolean(use));
		map.put("userId", userId);
		
		return sqlSession.update("holiday.editData", map);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 삭제
	 * @param no
	 * @return int
	 */
	public int removeData(long no) {
		return sqlSession.delete("holiday.removeData", no);
	}
	
	public long checkEdit(String day, long no) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("day", day);
		map.put("no", no);
		
		return sqlSession.selectOne("holiday.checkEdit", map);
	}
	
	public long checkInsert(String day) {
		return sqlSession.selectOne("holiday.checkInsert", day);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 6. 13.
	 * @brief 사용하는 휴일 리스트
	 * @param 
	 * @return List<HoliDayVo>
	 */
	public List<HoliDayVo> getUseList() {
		return sqlSession.selectList("holiday.getUseList");
	}
}
