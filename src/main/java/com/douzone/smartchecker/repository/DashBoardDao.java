package com.douzone.smartchecker.repository;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.douzone.dto.DashBoardDto;

@Repository
public class DashBoardDao {
	
	@Autowired
	private SqlSession sqlSession;
	
	
	/**
	 * @author JYS
	 * @date 2019. 6. 10.
	 * @return dashboard 에 사용될 데이터  
	 */
	public DashBoardDto getToday(String searchDate) {
		return sqlSession.selectOne("dashboard.today",searchDate);
	}
	
	/**
	 * @author JYS
	 * @date 2019. 6. 10.
	 * @return dashboard 에 사용될 데이터  
	 */
	public DashBoardDto getByDate(String date) {
		return sqlSession.selectOne("dashboard.getByDay",date);
	}
	
	
}
