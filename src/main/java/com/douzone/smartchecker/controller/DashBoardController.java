package com.douzone.smartchecker.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.smartchecker.service.DashBoardService;

@CrossOrigin
@RequestMapping("/dashboard")
@Controller
public class DashBoardController {

	
	@Autowired
	DashBoardService dashBoardService;
	
	@ResponseBody
	@GetMapping("/{searchDate}")
	public ResponseEntity<?> get(@PathVariable String searchDate) {
		
			Map <String,Object> map = new HashMap<String,Object>();
			map.put( "today",dashBoardService.getToday(searchDate));
			map.put("chartList", dashBoardService.getByDate(searchDate));
			return ResponseEntity.ok(JSONResult.success(map));

	}
}
	