package com.douzone.smartchecker.controller;

import java.time.LocalDate;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.CommuteService;

/**
 * @author PSH
 * @Date 2019. 4. 22.
 * @brief 출퇴근 등록 관리
 *
 */
@CrossOrigin
@RequestMapping("/work")
@Controller
public class WorkController {

	@Autowired
	private CommuteService commuteService;

	/**
	 * @author PSH
	 * @date 2019. 4. 22.
	 * @brief 출석 등록
	 * @param no, id | json
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping("/goto")
	public ResponseEntity<?> goTo(@RequestBody Map<Object, Object> map, HttpSession session) {
		//System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			String commute = (String) map.get("commute");
			String recordType = (String) map.get("recordType");
			String totalWorkTime = "00:00:00";
			Object result = commuteService.insertCommute(commute, recordType,totalWorkTime, LocalDate.now().toString(), userDetails);
			
			if(!Boolean.FALSE.equals(result)) {
				if(Boolean.TRUE.equals(result)) {
					return ResponseEntity.ok(JSONResult.success(true));
				}
				else {
					return ResponseEntity.ok(JSONResult.success(result));
				}
			}
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 23.
	 * @brief 퇴근 등록
	 * @param no
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping("/gooff")
	public ResponseEntity<?> goOff(@RequestBody Map<Object, Object> map) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			String commute = (String) map.get("commute");
			String recordType = (String) map.get("recordType");
			String totalWorkTime = (String) map.get("totalWorkTime");
			//System.out.println(totalWorkTime);
			totalWorkTime=totalWorkTime.substring(0,5);
			//System.out.println(totalWorkTime);
			Object result = commuteService.insertCommute(commute, recordType,totalWorkTime, LocalDate.now().toString(), userDetails);
			
			if(!Boolean.FALSE.equals(result)) {
				if(Boolean.TRUE.equals(result)) {
					return ResponseEntity.ok(JSONResult.success(true));
				}
				else {
					return ResponseEntity.ok(JSONResult.success(result));
				}
			}
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 로그인한 사용자의 출근 상태
	 * @param
	 * @return long
	 */
	@ResponseBody
	@GetMapping("/gotodata/{isToday}")
	public JSONResult getGoToData(@PathVariable boolean isToday) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			//System.out.println(isToday);
			if(isToday) {
				return JSONResult.success(commuteService.getGoToData(userDetails.getNo(), LocalDate.now().toString()));
			}
			else {
				return JSONResult.success(commuteService.getGoToData(userDetails.getNo(), LocalDate.now().minusDays(1).toString()));
			}
		}
		
		return null;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 로그인한 사용자의 퇴근 상태
	 * @param
	 * @return long
	 */
	@ResponseBody
	@GetMapping("/gooffdata/{isToday}")
	public JSONResult getGoOffData(@PathVariable boolean isToday) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			//System.out.println(isToday);
			if(isToday) {
				return JSONResult.success(commuteService.getGoOffData(userDetails.getNo(), LocalDate.now().toString()));
			}
			else {
				return JSONResult.success(commuteService.getGoOffData(userDetails.getNo(), LocalDate.now().minusDays(1).toString()));
			}
		}
		
		return null;
	}
	
	@ResponseBody
	@GetMapping("/pregooffdata")
	public JSONResult getPreGoOffData() {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			return JSONResult.success(commuteService.getPreGoOffData(userDetails.getNo()));
		}
		
		return null;
	}
}