package com.douzone.smartchecker.controller;

import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.WorkTimeService;
import com.douzone.smartchecker.vo.PageVo;
import com.douzone.smartchecker.vo.WorkTimeVo;
import com.mongodb.util.JSON;


@CrossOrigin
@RequestMapping("/worktime")
@Controller
public class WorkTimeController {

	@Autowired
	private WorkTimeService worktimeService;

	  @Autowired
	  private MessageSource msg;
	  
	  
	 
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근무 시간 설정
	 * @param start, end, userId
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping("/new")
	public ResponseEntity<?> create(@Valid @RequestBody WorkTimeVo vo,Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(worktimeService.insertWorkTime(vo.getStart(), vo.getEnd(), String.valueOf(vo.isUse()), userDetails))
			return	ResponseEntity.ok(JSONResult.success(msg.getMessage("insert.success", null, locale)));
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 근무 시간 리스트
	 * @param page
	 * @return List<WorkTimeVo>
	 */
	@ResponseBody
	@GetMapping("/list/{page}")
	public JSONResult getList(@PathVariable int page) {
		//System.out.println("현재 페이지 : " + page);

		int listCnt = worktimeService.getCount();

		PageVo pageVo = new PageVo();
		pageVo.pageInfo(page, listCnt);

		return JSONResult.success(worktimeService.getList(page));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 근무 시간 리스트 개수
	 * @param
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getCount() {
		return JSONResult.success(worktimeService.getCount());
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 가져오기
	 * @param no
	 * @return List<WorkTimeVo>
	 */
	@ResponseBody
	@GetMapping("/search/{no}")
	public JSONResult getData(@PathVariable(required = false) long no) {
		return JSONResult.success(worktimeService.getData(no));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 정보 수정
	 * @param no, start, end, use
	 * @return boolean
	 */
	@ResponseBody
	@PutMapping("/edit/{no}")
	public ResponseEntity<?> editData(@PathVariable long no, @Valid @RequestBody WorkTimeVo vo,Locale locale) {
		//System.out.println(vo);
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			if(worktimeService.editData(no, vo.getStart(), vo.getEnd(), String.valueOf(vo.isUse()), userDetails))
					return	ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.success", null, locale)));
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
		}
		
		return null;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 특정 근무시간 삭제
	 * @param no
	 * @return boolean
	 */
	@ResponseBody
	@DeleteMapping("/remove/{no}")
	public ResponseEntity<?> removeData(@PathVariable(required = false) long no,Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			if(worktimeService.removeDate(no, userDetails)) {
				return	ResponseEntity.ok(JSONResult.success(msg.getMessage("remove.success", null, locale)));
			}
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
		}
		
		return null;
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 20.
	 * @brief 사용하는 퇴근 시간 
	 * @param
	 * @return String
	 */
	@ResponseBody
	@GetMapping("/endtime")
	public ResponseEntity<?> getEndTime(){
		if(worktimeService.getUseWorkTime() != null) {
			return ResponseEntity.ok(JSONResult.success(worktimeService.getUseWorkTime().getEnd() + ":00"));
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
}
