package com.douzone.smartchecker.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.douzone.security.JwtTokenUtil;
import com.douzone.security.JwtUser;
import com.douzone.security.JwtUserFactory;
import com.douzone.smartchecker.repository.UserRepository;
import com.douzone.smartchecker.vo.UserVo;



@RestController
@CrossOrigin
public class UserRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private UserRepository userRepository;


    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    // user 정보
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public JwtUser getAuthenticatedUser(HttpServletRequest request) {
    	if(request.getHeader(tokenHeader) == null) {
    		return null;
    	}
    	
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        UserVo vo = userRepository.findByUsername(username);
        vo.setPassword("");
	     JwtUser user =  JwtUserFactory.create(vo);
        return user;
    }

}
