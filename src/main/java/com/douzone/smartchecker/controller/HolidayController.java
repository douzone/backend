package com.douzone.smartchecker.controller;

import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.HolidayService;
import com.douzone.smartchecker.vo.HoliDayVo;
import com.douzone.smartchecker.vo.PageVo;

/**
 * @author PSH
 * @Date 2019. 5. 3.
 * @brief 휴일 설정 Controller
 *
 */
@CrossOrigin
@RequestMapping("/holiday")
@Controller
public class HolidayController {

	@Autowired
	private HolidayService holidayService;

	@Autowired
	private MessageSource msg;

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 등록
	 * @param day, description, userId
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping("/new")
	public ResponseEntity<?> create(@Valid @RequestBody HoliDayVo holiDayVo, Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {

			Object result = holidayService.create(holiDayVo.getDay(), holiDayVo.getDescription(),
					String.valueOf(holiDayVo.isUse()), userDetails, locale);

			if (!Boolean.FALSE.equals(result)) {
				if (Boolean.TRUE.equals(result)) {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("insert.success", null, locale)));
				} else {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("insert.fail", null, locale)));
				}
			}

		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 리스트 개수
	 * @param
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getCount() {
		return JSONResult.success(holidayService.getCount());
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴일 전체 리스트
	 * @param page
	 * @return List<HoliDayVo>
	 */
	@ResponseBody
	@GetMapping("/list/{page}")
	public JSONResult getList(@PathVariable int page) {
		//System.out.println("현재 페이지 : " + page);

		int listCnt = holidayService.getCount();

		PageVo pageVo = new PageVo();
		pageVo.pageInfo(page, listCnt);

		return JSONResult.success(holidayService.getList(page));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 정보
	 * @param no
	 * @return HoliDayVo
	 */
	@ResponseBody
	@GetMapping("/search/{no}")
	public JSONResult getData(@PathVariable(required = false) long no) {
		return JSONResult.success(holidayService.getData(no));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 수정
	 * @param no, day, use, description
	 * @return boolean
	 */
	@ResponseBody
	@PutMapping("/edit/{no}")
	public ResponseEntity<?> editData(@PathVariable(required = false) long no, @Valid @RequestBody HoliDayVo vo,
			Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Object result = holidayService.editData(no, vo.getDay(), vo.getDescription(), String.valueOf(vo.isUse()),
				userDetails, locale);

		if (userDetails != null) {
			if (!Boolean.FALSE.equals(result)) {
				if (Boolean.TRUE.equals(result)) {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.success", null, locale)));
				} else {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.fail", null, locale)));
				}
			}

		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴일 삭제
	 * @param no
	 * @return boolean
	 */
	@ResponseBody
	@DeleteMapping("/remove/{no}")
	public ResponseEntity<?> removeData(@PathVariable(required = false) long no, Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			if (holidayService.removeData(no, userDetails)) {
				return ResponseEntity.ok(JSONResult.success(msg.getMessage("remove.success", null, locale)));
			}

		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 6. 13.
	 * @brief 사용하는 휴일 리스트
	 * @param 
	 * @return boolean
	 */
	@ResponseBody
	@GetMapping("/use")
	public JSONResult getUseList() {
		return JSONResult.success(holidayService.checkHoliDay());
	}
}
