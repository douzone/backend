package com.douzone.smartchecker.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.TranslationService;
import com.douzone.smartchecker.service.WorkAttitudeService;
import com.douzone.smartchecker.vo.CommuteVo;
//import com.douzone.smartchecker.service.WorkAttitudeService;
import com.douzone.smartchecker.vo.WorkAttitudeVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

/**
 * @author JSY
 * @Date 2019. 4. 23.
 * @brief 근태 관리
 *
 */

@CrossOrigin
@RequestMapping("/workattitude")
@Controller
public class WorkAttitudeController {

	@Autowired
	WorkAttitudeService workAttitudeService;

	@Autowired
	TranslationService translationService;

	@Autowired
	private MessageSource msg;

	/**
	 * @author JYS
	 * @date 2019. 4. 23.
	 * @brief 근태 등록
	 * @param workAttutudeVo | json
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping(value = "/new")
	public ResponseEntity<?> insert(@Valid @RequestBody WorkAttitudeVo workAttitudeVo, Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Object result;
		
		if (userDetails != null) {
//			System.out.println("로그인아이디:" + userDetails.getUsername());
//			System.out.println("로그인비번:" + userDetails.getPassword());
//			System.out.println("이름:" + userDetails.getName());
			//System.out.println(workAttitudeVo);
			
//			String name = (String) map.get("name");
//			String title = (String) map.get("title");
//			String startDay = (String) map.get("startDay");
//			String endDay = (String) map.get("endDay");
//			String state = (String) map.get("state");
//			String workAttitudeList = (String) map.get("workAttitudeList");
//			String content = (String) map.get("content");
			// long userNo = ((Number)map.get("userNo")).longValue();
			// String userId= (String)userDetails.getUsername();
			workAttitudeVo.setEndDay(workAttitudeVo.getEndDay()+" 23:59:59");
			workAttitudeVo.setStartDay(workAttitudeVo.getStartDay()+" 00:00:00");
//			endDay += " 23:59:59";
//			startDay += " 00:00:00";
			//WorkAttitudeVo workAttitudeVo = new WorkAttitudeVo();

			// 한국어 일때
			if (locale.toString().equals("ko")) {
				workAttitudeVo.setUserNo(userDetails.getNo());
				workAttitudeVo.setInsertUserId(userDetails.getUsername());
				workAttitudeVo.setUpdateUserId(userDetails.getUsername());
				// 번역해야하는것: title,content,state,work_attitude_list
				workAttitudeVo.setTitleEn(translationService.koToEn(workAttitudeVo.getTitle()));
				workAttitudeVo.setContentEn(translationService.koToEn(workAttitudeVo.getContent()));

//			System.out.println(name);
//			System.out.println(title);
//			System.out.println(startDay);
//			System.out.println(endDay);
//			System.out.println(state);
//			System.out.println(content);
//			System.out.println(userDetails.getUsername());
//			System.out.println(userDetails.getNo());
				
				result = workAttitudeService.insertWorkAttitude(workAttitudeVo, userDetails);
				if (!Boolean.FALSE.equals(result)) {
					if(Boolean.TRUE.equals(result)) {
						return ResponseEntity.ok(JSONResult.success(msg.getMessage("workAttitude.success", null, locale)));
					}
					else {
						return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.fail", null, locale)));
					}
				}

			} else if (locale.toString().equals("en")) {
//				workAttitudeVo.setName(name);
//				workAttitudeVo.setTitleEn(title);
//				workAttitudeVo.setStartDay(startDay);
//				workAttitudeVo.setEndDay(endDay);
//				workAttitudeVo.setState(state);
//				workAttitudeVo.setWorkAttitudeList(workAttitudeList);
//				workAttitudeVo.setContentEn(content);
				workAttitudeVo.setUserNo(userDetails.getNo());
				workAttitudeVo.setInsertUserId(userDetails.getUsername());
				workAttitudeVo.setUpdateUserId(userDetails.getUsername());
				// 번역해야하는것: title,content,state,work_attitude_list
				workAttitudeVo.setTitleEn(workAttitudeVo.getTitle());
				workAttitudeVo.setContentEn(workAttitudeVo.getContent());
				workAttitudeVo.setTitle(translationService.enToKo(workAttitudeVo.getTitle()));
				workAttitudeVo.setContent(translationService.enToKo(workAttitudeVo.getContent()));
				
				result = workAttitudeService.insertWorkAttitude(workAttitudeVo, userDetails);
				if (!Boolean.FALSE.equals(result)) {
					if(Boolean.TRUE.equals(result)) {
						return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.success", null, locale)));
					}
					else {
						return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.fail", null, locale)));
					}
				}
			}

		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author JYS
	 * @date 2019. 4. 26.
	 * @brief 근태 달력 정보 조회
	 * @param 조회 날짜 , 회원번호
	 * @return 근태 정보
	 */
	@ResponseBody
	@GetMapping(value = "/calendar/{no}")
	public ResponseEntity<?> getCalendarList(
			@RequestParam(value = "month", required = false, defaultValue = "no") String month,
			@RequestParam(value = "workattitude", required = false, defaultValue = "전체") String workAttitude,
			@RequestParam(value = "select", required = false, defaultValue = "false") String select,
			@PathVariable(value = "no") String no, Locale locale) {

		String startDate;
		String endDate;
		if (month.equals("no")) {
			startDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-00"));
			endDate = LocalDateTime.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-00"));
		} else {

			startDate = month + "-00";
			endDate = new DateTime(month).plusMonths(1).toString().substring(0, 7) + "-00";
		}
		List<WorkAttitudeVo> list;
		if (select.equals("true")) {
			if (locale.toString().equals("en")) {
				if (workAttitude.equals("All"))
					workAttitude = "전체";
				else if (workAttitude.equals("Business trip"))
					workAttitude = "출장";
				else if (workAttitude.equals("Work outside"))
					workAttitude = "외근";
				else if (workAttitude.equals("Annual leave"))
					workAttitude = "연차";
				else if (workAttitude.equals("Semi-annual leave"))
					workAttitude = "반차";
				else if (workAttitude.equals("Education"))
					workAttitude = "교육";
			}
			//System.out.println(workAttitude);
			list = workAttitudeService.getCalendarList(Long.parseLong(no), startDate, endDate, workAttitude);
//			for (WorkAttitudeVo vo : list)
//				System.out.println(vo);
		} else {
			list = workAttitudeService.getCalendarList(no, startDate, endDate, workAttitude);
		}

//		if(list.size()==0) {
//			return ResponseEntity.ok(JSONResult.success("해당 데이터가 DB에 없습니다."));
//    	}
		return ResponseEntity.ok(JSONResult.success(list));

	}

	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 근태 현황 전체 리스트
	 * @param page
	 * @return List<WorkAttitudeVo>
	 */
	@ResponseBody
	@GetMapping("/list")
	public JSONResult getList(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchToDate", required = false) String searchToDate,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchUserName", required = false) String searchUserName,
			@RequestParam(value = "searchWorkAttitude", required = false) String searchWorkAttitude,
			@RequestParam(value = "keyNo", required = false) Long keyNo,
			@RequestParam(value = "page", required = false) Long page, Locale locale) {

		//System.out.println("현재 페이지 : " + page);

//		System.out.println(searchToDate);
//		System.out.println(searchFromDate);
//		System.out.println(searchUserNo);
//		System.out.println(keyNo);
//		System.out.println("locale  :   " + locale);
		
		if(searchUserNo != null) {
			searchUserName = "";
		}
		
		List<WorkAttitudeVo> list = workAttitudeService.getList(page, searchUserNo, searchFromDate, 
				searchToDate, searchUserName, searchWorkAttitude, keyNo, locale);
		//new Locale("en") 
		for(int i = 0; i < list.size(); i++) {
			//System.out.println(list.get(i));
			list.get(i).setWorkAttitudeList(msg.getMessage(list.get(i).getWorkAttitudeList(), null, new Locale("ko")));
			list.get(i).setDay(msg.getMessage(list.get(i).getDay(), null, new Locale("ko")));
			list.get(i).setState(msg.getMessage(list.get(i).getState(), null, new Locale("ko")));
			list.get(i).setWorkAttitudeListEn(msg.getMessage(list.get(i).getWorkAttitudeList(), null, new Locale("en")));
			list.get(i).setDayEn(msg.getMessage(list.get(i).getDay(), null, new Locale("en")));
			list.get(i).setStateEn(msg.getMessage(list.get(i).getState(), null, new Locale("en")));
		}
		
		return JSONResult.success(list);
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 10.
	 * @brief 근태 현황 전체 리스트 개수
	 * @param
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getTotalCount(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchToDate", required = false) String searchToDate,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchUserName", required = false) String searchUserName,
			@RequestParam(value = "searchWorkAttitude", required = false) String searchWorkAttitude,
			@RequestParam(value = "keyNo", required = false) Long keyNo) {

		if (searchUserNo != null) {
			searchUserName = "";
		}

//		System.out.println("searchFromDate--------->   " + searchFromDate);
//		System.out.println("searchToDate--------->   " + searchToDate);
//		System.out.println("searchUserNo--------->   " + searchUserNo);
//		System.out.println("searchUserName--------->   " + searchUserName);
//		System.out.println("searchWorkAttitude--------->   " + searchWorkAttitude);
//		System.out.println("keyNo--------->   " + keyNo);

		return JSONResult.success(workAttitudeService.getTotalCount(searchFromDate, searchToDate, searchUserNo,
				searchUserName, searchWorkAttitude, keyNo));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 16.
	 * @brief 근태 상태 건수
	 * @param
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/state")
	public JSONResult getStateList() {
		return JSONResult.success(workAttitudeService.getStateCount());
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 근태 검색한 정보의 상태 건수
	 * @param
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/searchstate")
	public JSONResult getSearchStateList(
			@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchToDate", required = false) String searchToDate,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchUserName", required = false) String searchUserName,
			@RequestParam(value = "searchWorkAttitude", required = false) String searchWorkAttitude,
			@RequestParam(value = "keyNo", required = false) Long keyNo) {

		return JSONResult.success(workAttitudeService.getSearchStateCount(searchFromDate, searchToDate, searchUserNo,
				searchUserName, searchWorkAttitude, keyNo));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 근태 달력 상태건수
	 * @param String, String, Long, String, String
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/calendarstate")
	public JSONResult getCalendarStateList(
			@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchUserName", required = false) String searchUserName,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchWorkAttitude", required = false) String searchWorkAttitude,
			@RequestParam(required = false) boolean select) {

		return JSONResult.success(workAttitudeService.getCalendarStateCount(searchFromDate, searchUserNo,
				searchUserName, searchWorkAttitude, select));
	}

	/**
	 * @author KJS
	 * @date 2019. 5. 18.
	 * @brief 알람 리스트
	 * @param keyNo
	 * @return List<WorkAttitudeVo>
	 */
	@ResponseBody
	@GetMapping("/alarm/list/{keyno}")
	public JSONResult getAlarmSearchList(@PathVariable long keyno) {
		//System.out.println("keyNo" + keyno);

		// return null;
		return JSONResult.success(workAttitudeService.getAlarmSearchList(keyno));
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태정보 정보 가져오기
	 * @param no
	 * @return List<workAttitudeVo>
	 */
	@ResponseBody
	@GetMapping("/search/{no}/{locale}")
	public JSONResult getData(@PathVariable long no, @PathVariable Locale locale) {
		return JSONResult.success(workAttitudeService.getData(no, locale));
	}

	/**
	 * @author JYD
	 * @date 2019. 05. 17.
	 * @brief 근태 신청 수정
	 * @param no, WorkAttitudeVo
	 * @return int
	 */
	@ResponseBody
	@PutMapping("/{no}")
	public ResponseEntity<?> editWorkAttitudeData(@PathVariable(required = false) long no, @RequestBody WorkAttitudeVo vo,
			Locale locale) {

		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		// 여기 달력부분 보여지는 화면때문에 제가 넣은 코드입니다. - 영석
		vo.setEndDay(vo.getEndDay() + " 23:59:59");
		vo.setStartDay(vo.getStartDay() + " 00:00:00");

		if (userDetails != null) {
			vo.setUpdateUserId(userDetails.getUsername());
			vo.setNo(no);

			if(locale.toString().equals("ko")) {
				vo.setTitleEn(translationService.koToEn(vo.getTitle()));
				vo.setContentEn(translationService.koToEn(vo.getContent()));
			}
			else if(locale.toString().equals("en"))
			{
				vo.setTitleEn(vo.getTitle());
				vo.setContentEn(vo.getContent());
				vo.setTitle(translationService.enToKo(vo.getTitleEn())); 
				vo.setContent(translationService.enToKo(vo.getContentEn()));
			}
			
			Object result = workAttitudeService.editWorkAttitudeData(vo, userDetails);
			
			if (!Boolean.FALSE.equals(result))
				if(Boolean.TRUE.equals(result)) {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.success", null, locale)));	
				}
				else {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.fail", null, locale)));
				}
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author JYD
	 * @date 2019. 5. 17.
	 * @brief 특정 근태신청 삭제
	 * @param no
	 * @return boolean
	 */
	@ResponseBody
	@DeleteMapping("/{no}")
	public ResponseEntity<?> removeData(@PathVariable long no) {

		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			if (workAttitudeService.removeData(no, userDetails))
				return ResponseEntity.ok(JSONResult.success(true));
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
}