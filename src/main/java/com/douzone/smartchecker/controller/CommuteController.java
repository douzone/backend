package com.douzone.smartchecker.controller;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.CalendarDto;
import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.CommuteService;
import com.douzone.smartchecker.vo.CommuteVo;
import com.douzone.smartchecker.vo.PageVo;
import com.douzone.smartchecker.vo.UserVo;
import com.douzone.smartchecker.vo.WorkAttitudeVo;

/**
 * @author JYS
 * @Date 2019. 4. 23.
 * @brief 출퇴근 조회
 */
@CrossOrigin
@RequestMapping("/commute")
@Controller
public class CommuteController {

	@Autowired
	private CommuteService commuteService;
	
	@Autowired
	private MessageSource msg;


	/**
	 * @author JYS
	 * @Date 2019. 4. 23.
	 * @brief 출퇴근 조회
	 * @param page | json
	 * @return CalendarDto
	 */
	//@PreAuthorize("hasRole('ADMIN')")
	@RequestMapping(value = "/calendar/{no}", method = RequestMethod.GET)
	public ResponseEntity<?> calendar(@RequestParam(value = "month", required = false, defaultValue = "no") String month,
			@RequestParam(value = "state", required = false, defaultValue = "전체") String state,
			@RequestParam(value="select",required= false , defaultValue="false") String select,
			@PathVariable(value = "no") String no) {
		String startDate;
		String endDate;
		if (month.equals("no")) {
			startDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-00"));
			endDate = LocalDateTime.now().plusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM-00"));
		}else {
			
			startDate=month+"-00";
			endDate=new DateTime(month).plusMonths(1).toString().substring(0,7)+"-00";
		}
//		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//		System.out.println(startDate);
//		System.out.println(endDate);
//		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//		
//		System.out.println(select);
//		System.out.println(no);
		List<CommuteVo> list;
		if(select.equals("true")) {
		 list = commuteService.getCalendarList(no, startDate, endDate, state);
		return ResponseEntity.ok(JSONResult.success(list));
		}
		else {
			 list = commuteService.getCalendarListByName(no, startDate, endDate, state);
		}
//		if(list.size()==0) {
//    		return ResponseEntity.ok(JSONResult.success("해당 데이터가 DB에 없습니다."));
//    	}
		return ResponseEntity.ok(JSONResult.success(list));
		
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 23.
	 * @brief 출퇴근 조희 전체 리스트
	 * @param page | json
	 * @return CommuteVo
	 */
	@ResponseBody
	@GetMapping("/list/{page}")
	public JSONResult getList(@PathVariable int page) {
		//System.out.println("현재 페이지 : " + page);

		int listCnt = commuteService.getTotalCount();

		PageVo pageVo = new PageVo();
		pageVo.pageInfo(page, listCnt);

		return JSONResult.success(commuteService.getList(page));
	}

	/**
	 * + * @author PSH + * @date 2019. 4. 26. + * @brief 출퇴근 리스트 전체 개수 + * @param +
	 * * @return int +
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getTotalCount() {
		return JSONResult.success(commuteService.getTotalCount());
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 2.
	 * @brief 검색한 리스트 개수
	 * @param startdate, enddate, paramNo
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/searchpage")
	public JSONResult getSearchCount(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchToDate", required = false) String searchToDate,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchState", required = false) String searchState,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "select", required = true) boolean select,
			@RequestParam(value = "keyNo", required = false) Long keyNo) {

		//System.out.println(userName);
		
		int num = commuteService.getSearchCount(searchFromDate, searchToDate,
				searchUserNo, searchState, select, userName, keyNo);
		return JSONResult.success(num);
	}

	/**
	 * @author PSH
	 * @date 2019. 4. 24.
	 * @brief 출퇴근 조회 검색 리스트
	 * @param startDate, endDate, keyword
	 * @return List<CommuteVo>
	 */
	@ResponseBody
	@GetMapping("/search")
	public JSONResult getSearch(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
			@RequestParam(value = "searchToDate", required = false) String searchToDate,
			@RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
			@RequestParam(value = "searchState", required = false) String searchState,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "page", required = false) Long page,
			@RequestParam(value = "select", required = true) boolean select,
			@RequestParam(value = "keyNo", required = false) Long keyNo,
			Locale locale) {
		
		List<CommuteVo> list = commuteService.getSearch(page, searchUserNo, searchFromDate, searchToDate, searchState, select, userName, keyNo);
		for(int i = 0; i < list.size(); i++) {
			list.get(i).setFromState(msg.getMessage(list.get(i).getFromState(), null, new Locale("ko")));
			list.get(i).setFromStateEn(msg.getMessage(list.get(i).getFromState(), null, new Locale("en")));
			if(list.get(i).getToState() != null) {
				list.get(i).setToStateEn(msg.getMessage(list.get(i).getToState(), null, new Locale("en")));
				list.get(i).setToState(msg.getMessage(list.get(i).getToState(), null, new Locale("ko")));
			}
			list.get(i).setDay(msg.getMessage(list.get(i).getDay(), null, new Locale("ko")));
			list.get(i).setDayEn(msg.getMessage(list.get(i).getDay(), null, new Locale("en")));
		}
		
		return JSONResult
				.success(list);
	}


	/**
	 * @author JYD
	 * @date 2019. 05. 08.
	 * @brief 출퇴근 상세 조회
	 * @param stsrtNo, endNo
	 * @return CommuteVo
	 */
	@ResponseBody
	@GetMapping("/search/{stsrtNo}/{endNo}")
	public JSONResult getSearchBetween(@PathVariable(required = false) long stsrtNo,
			@PathVariable(required = false) long endNo) {
		
		//System.out.println(stsrtNo);
		//System.out.println(endNo);

		return JSONResult.success(commuteService.getSearchBetween(stsrtNo, endNo));
	}

	/**
	 * @author JYD
	 * @date 2019. 05. 08.
	 * @brief 출퇴근 수정
	 * @param startNo, endNo, CommuteVo
	 * @return int
	 */
	@ResponseBody
	@PutMapping("/{startNo}/{endNo}")
	public  ResponseEntity<?> editCommuteBetween(@PathVariable(required = false) long startNo,
			@PathVariable(required = false) long endNo, @RequestBody CommuteVo vo) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			vo.setUpdateUserId(userDetails.getUsername());
			vo.setStartNo(startNo);
			vo.setEndNo(endNo);
			if(commuteService.editCommuteBetween(vo, userDetails))
				return ResponseEntity.ok(JSONResult.success(true));
		} 
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}

	/**
	 * @author JYD
	 * @date 2019. 05. 14.
	 * @brief 출퇴근 삭제
	 * @param startNo, endNo
	 * @return int
	 */
	@ResponseBody
	@DeleteMapping("/{startNo}/{endNo}")
	public ResponseEntity<?> removeCommuteBetween(@PathVariable(required = false) Long startNo,
										   @PathVariable(required = false) Long endNo) {		
		
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (userDetails != null) {
			CommuteVo vo = new CommuteVo();
			
			vo.setStartNo(startNo);
			vo.setEndNo(endNo);
			
			//System.out.println(vo);
			if(commuteService.removeCommuteBetween(vo, userDetails))
				return ResponseEntity.ok(JSONResult.success(true));
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 15.
	 * @brief 출퇴근 상태 건수 
	 * @param
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/state")
	public JSONResult getStateList() {
		return JSONResult.success(commuteService.getStateCount());
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 21.
	 * @brief 출퇴근 검색 조건에 따른 상태건수
	 * @param String, String, Long, String, String, boolean
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/searchstate")
	public JSONResult getSearchStateList(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
									     @RequestParam(value = "searchToDate", required = false) String searchToDate,
										 @RequestParam(value = "searchUserNo", required = false) Long searchUserNo,
										 @RequestParam(value = "searchState", required = false) String searchState,
										 @RequestParam(value = "userName", required = false) String userName,
										 @RequestParam(value = "select", required = true) boolean select,
										 @RequestParam(value = "keyNo", required = false) Long keyNo) {
		
		//System.out.println("keyNo ------------------>> " + keyNo);
				
		return JSONResult.success(commuteService.getSearchStateList(searchFromDate, searchToDate, searchUserNo, searchState, select, userName, keyNo));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 22.
	 * @brief 출퇴근 달력 상태건수
	 * @param 
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/calendarstate")
	public JSONResult getCalendarStateList(@RequestParam(value = "searchFromDate", required = false) String searchFromDate,
											@RequestParam(required=false) Long searchUserNo,
										   @RequestParam(required=false) String userName,
										   @RequestParam(value = "searchState", required = false) String searchState,
										   @RequestParam(required=true) boolean select) {
		
		return JSONResult.success(commuteService.getCalendarStateList(searchFromDate, searchUserNo, userName, searchState, select));
	}
}