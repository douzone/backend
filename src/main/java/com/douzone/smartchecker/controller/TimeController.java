package com.douzone.smartchecker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.smartchecker.service.CommuteService;
import com.douzone.smartchecker.service.TimeService;
import com.douzone.smartchecker.service.WorkTimeService;
import com.douzone.smartchecker.vo.CommuteVo;
import com.douzone.smartchecker.vo.WorkTimeVo;

@CrossOrigin
@RequestMapping("/time")
@Controller
public class TimeController {

	@Autowired
	TimeService timeService;
	
	@Autowired
	WorkTimeService workTimeService;
	
	@Autowired
	CommuteService commuteService;
	
	@ResponseBody
	@GetMapping("/{no}")
	public ResponseEntity<?> get(@PathVariable long no) {
		CommuteVo vo = commuteService.getUseCommuteTime(no);
		if(vo!=null) {
			return ResponseEntity.ok(JSONResult.success(timeService.time(no)));
		}
		else
			return ResponseEntity.ok(JSONResult.success(null));
	}
	
	 @ResponseBody
	   @GetMapping("/total/{no}")
	   public ResponseEntity<?> gettotaltimie(@PathVariable long no) {
	      CommuteVo vo = commuteService.getUseCommuteTime(no);
	      
	      if(vo!=null) {
	         return ResponseEntity.ok(JSONResult.success(timeService.totaltime(no)));
	      }
	      else
	         return ResponseEntity.ok(JSONResult.success(null));
	   }

	@ResponseBody
	@GetMapping("/use")
	public ResponseEntity<?> getUseWorkTime() {
		WorkTimeVo vo = workTimeService.getUseWorkTime();
		
		if(vo!=null)
		return ResponseEntity.ok(JSONResult.success(vo));
		
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("사용중인 근무시간이 없습니다."));
	}
	
	@ResponseBody
	@GetMapping("/leaveWork/{no}")
	public ResponseEntity<?> getTodayleaveWorkByNo(@PathVariable long no) {
		CommuteVo vo =timeService.getTodayleaveWorkByNo(no);
		if(vo!=null)
			return ResponseEntity.ok(JSONResult.success(vo));
		else
			return 
		 ResponseEntity.ok(JSONResult.success(null));
	}
}
	