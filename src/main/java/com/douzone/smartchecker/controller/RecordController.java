package com.douzone.smartchecker.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.smartchecker.repository.RecordDao;
import com.douzone.smartchecker.service.RecordService;
import com.douzone.smartchecker.vo.RecordVo;


/**
 * @author PSH
 * @Date 2019. 4. 22.
 * @brief 활동기록 DB 관리
 *
 */
@CrossOrigin
@Controller
@RequestMapping("/record")
public class RecordController {

	@Autowired
	private RecordDao recordDao;
	
	@Autowired
	private RecordService recordService;
	
	/**
	 * @author PSH
	 * @date 2019. 4. 22.
	 * @brief 활동 기록 보기
	 * @param
	 * @return List<RecordVo>
	 */
	@ResponseBody
	@GetMapping("/list/{page}")
	public JSONResult findAll(@PathVariable int page) {
		Pageable pageable = PageRequest.of(page-1, 30, Sort.by(Sort.Direction.DESC, "insertTime"));
		
		Page<RecordVo> recordPage = recordDao.findAll(pageable);
		List<RecordVo> recordList = recordPage.getContent();
		
		return JSONResult.success(recordList);
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 7.
	 * @brief 활동 기록 리스트 개수
	 * @param
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getCount() {
		return JSONResult.success(recordDao.count()); 
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 7.
	 * @brief 활동 기록 검색
	 * @param
	 * @return List<RecordVo>
	 */
	@ResponseBody 
	@GetMapping("/search/{page}")
	public JSONResult getSearchList(@PathVariable int page, 
									@RequestParam(required=false) String start, 
									@RequestParam(required=false) String end, 
									@RequestParam(required=false) String name,
									@RequestParam(required=false) String content,
									@RequestParam(required=false) String recordType,
									@RequestParam(required=true) boolean select,
									@RequestParam(required=false) String searchUserNo,
									@RequestParam(required=false) String read,
									@RequestParam(required=false) Locale locale) {
		
		if(select == true) {
			name = "";
		}
		else {
			searchUserNo = "";
		}
		
		if(recordType.equals("전체")) {
			recordType = "";
		}
		
		if(read.equals("전체")) {
			read = "";
		}
		
		///System.out.println("content   ======> " + recordType);
		//System.out.println("read   ======> " + read);
		
		return JSONResult.success(recordService.getSearch(page, start, end, name, content, recordType, select, searchUserNo, read, locale));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 8.
	 * @brief 활동기록 검색 리스트 개수
	 * @param startdate, enddate, name, content
	 * @return count
	 */
	@ResponseBody
	@GetMapping("/searchpage")
	public JSONResult getSearchCount(@RequestParam(value="startdate", required=false) String start, 
									 @RequestParam(value="enddate", required=false) String end,
									 @RequestParam(required=false) String name,
									 @RequestParam(required=false) String content,
									 @RequestParam(required=false) String recordType,
									 @RequestParam(required=true) boolean select,
									 @RequestParam(required=false) String searchUserNo,
									 @RequestParam(required=false) String read,
									 @RequestParam(value="language", required=false) Locale locale) {
		
		if(select == true) {
			name = "";
		}
		else {
			searchUserNo = "";
		}
		
		if(recordType.equals("전체")) {
			recordType = "";
		}
		
		if(read.equals("전체")) {
			read = "";
		}
		
		return JSONResult.success(recordService.getSearchPage(start, end, name, content, recordType, select, searchUserNo, read, locale));
	}
	
	/**
	 * @author KJS
	 * @date 2019. 5. 14.
	 * @brief 알람 리스트 개수
	 * @param auth
	 * @return List<RecordVo>
	 */
	@ResponseBody
	@GetMapping("/alarm/{auth}")
	public JSONResult getAlarmList(
			@PathVariable String auth, 
			@RequestParam(required=false) String user_no) {
		return JSONResult.success(recordService.getAlarm(auth, user_no));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 활동기록 읽음여부 상태건수
	 * @param
	 * @return Map<String, Object>
	 */
	@ResponseBody
	@GetMapping("/state")
	public JSONResult getStateList() {
		return JSONResult.success(recordService.getStateCount());
	}
	
	@ResponseBody
	@GetMapping("/searchstate")
	public JSONResult getSearchStateList(@RequestParam(value="startdate", required=false) String start, 
										 @RequestParam(value="enddate", required=false) String end,
										 @RequestParam(required=false) String name,
										 @RequestParam(required=false) String content,
										 @RequestParam(required=false) String recordType,
										 @RequestParam(required=true) boolean select,
										 @RequestParam(required=false) String searchUserNo,
										 @RequestParam(required=false) String read,
										 @RequestParam(value="language", required=false) Locale locale) {
		
		if(select == true) {
			name = "";
		}
		else {
			searchUserNo = "";
		}
		
		if(recordType.equals("전체")) {
			recordType = "";
		}
		
		if(read.equals("전체")) {
			read = "";
		}
		
		return JSONResult.success(recordService.getSearchStateList(start, end, name, content, recordType, select, searchUserNo, read, locale));
	}
	
	/**
	 * @author KJS
	 * @date 2019. 5. 20.
	 * @brief 활동기록 읽음 업데이트
	 * @param key_no
	 * @return RecordVo
	 */
	@ResponseBody
	@GetMapping("/alarm/update/{key_no}/{auth}/{recordType}")
	public  ResponseEntity<?> updateRead(
			@PathVariable String key_no, 
			@PathVariable String auth, 
			@PathVariable String recordType) {
		if(recordService.updateRead(key_no, auth, recordType)!=null)
			return	ResponseEntity.ok(JSONResult.success(true));
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
	/**
	 * @author PSH
	 * @date 2019. 5. 20.
	 * @brief 특정 활동기록 정보
	 * @param no
	 * @return RecordVo
	 */
	@ResponseBody
	@GetMapping("/data/{no}")
	public JSONResult getData(@PathVariable(required=false) long no){
		return JSONResult.success(recordService.getData(no));
	}
}
