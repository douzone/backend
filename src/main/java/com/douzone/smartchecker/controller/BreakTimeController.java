package com.douzone.smartchecker.controller;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.xmlbeans.impl.jam.JSourcePosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.douzone.smartchecker.service.BreakTimeService;
import com.douzone.smartchecker.vo.BreakTimeVo;
import com.douzone.smartchecker.vo.PageVo;
import com.douzone.smartchecker.vo.WorkAttitudeVo;
import com.mongodb.util.JSON;


@CrossOrigin
@RequestMapping("/breaktime")
@Controller
public class BreakTimeController {
	
	@Autowired
	private BreakTimeService breakTimeService;

	 @Autowired
	  private MessageSource msg;
	  
	  
	/**
	 * @author PSH
	 * @date 2019. 4. 29.
	 * @brief 휴게시간 등록
	 * @param 
	 * @return insert 여부
	 */
	@ResponseBody
	@PostMapping("/new")
	public ResponseEntity<?> create(@Valid @RequestBody BreakTimeVo breaktimeVo, HttpSession session,Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			
			String start = breaktimeVo.getStart();
			String end = breaktimeVo.getEnd();
			String description = breaktimeVo.getDescription();
			String use = String.valueOf(breaktimeVo.isUse());
			
			Object result = breakTimeService.insertBreakTime(start, end, description, use, userDetails,locale);
			
			if(!Boolean.FALSE.equals(result)) {
				if(Boolean.TRUE.equals(result)) {
					return	ResponseEntity.ok(JSONResult.success(msg.getMessage("insert.success", null, locale)));	
				}
				else {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("breakTimeInsert.fail", null, locale)));
				}
			}			
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("failz"));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴게 시간 전체 리스트
	 * @param page
	 * @return List<BreakTimeVo>
	 */
	@ResponseBody
	@GetMapping("/list/{page}")
	public JSONResult getList(@PathVariable int page) {
		//System.out.println("현재 페이지 : " + page);
		
		int listCnt = breakTimeService.getCount();
		
		PageVo pageVo = new PageVo();
		pageVo.pageInfo(page, listCnt);
		
		return JSONResult.success(breakTimeService.getList(page));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 3.
	 * @brief 휴게시간 리스트 개수
	 * @param
	 * @return int
	 */
	@ResponseBody
	@GetMapping("/page")
	public JSONResult getCount() {
		return JSONResult.success(breakTimeService.getCount());
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 정보
	 * @param no
	 * @return BreakTimeVo
	 */
	@ResponseBody
	@GetMapping("/search/{no}")
	public JSONResult getData(@PathVariable(required = false) long no) {
		return JSONResult.success(breakTimeService.getData(no));
	}

	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 수정
	 * @param no, start, end, use, description
	 * @return boolean
	 */
	@ResponseBody
	@PutMapping("/edit/{no}")
	public ResponseEntity<?> editData(@PathVariable(required = false) long no, @Valid @RequestBody BreakTimeVo vo, Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Object result = breakTimeService.editData(no, vo.getStart(), vo.getEnd(), vo.getDescription(), String.valueOf(vo.isUse()), userDetails, locale);
		
		if(userDetails != null) {
			if(!Boolean.FALSE.equals(result)) {
				if(Boolean.TRUE.equals(result)) {
					return	ResponseEntity.ok(JSONResult.success(msg.getMessage("apply.success", null, locale)));	
				}
				else {
					return ResponseEntity.ok(JSONResult.success(msg.getMessage("breakTimeInsert.fail", null, locale)));
				}
			}
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
	
	/**
	 * @author PSH
	 * @date 2019. 5. 17.
	 * @brief 특정 휴게시간 삭제
	 * @param no
	 * @return boolean
	 */
	@ResponseBody
	@DeleteMapping("/remove/{no}")
	public ResponseEntity<?> removeData(@PathVariable(required = false) long no,Locale locale) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(userDetails != null) {
			if(breakTimeService.removeData(no, userDetails)) {
				return	ResponseEntity.ok(JSONResult.success(msg.getMessage("remove.success", null, locale)));
			}
			
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	}
}
