
package com.douzone.smartchecker.controller;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtAuthenticationRequest;
import com.douzone.security.JwtTokenUtil;
import com.douzone.security.RSA;
import com.douzone.smartchecker.service.JwtAuthenticationResponse;
import com.douzone.smartchecker.service.TokenService;
import com.douzone.smartchecker.service.UserService;
/**
 * @author JYS
 * @Date 2019. 4. 25.
 * @brief 유저 관련 컨트롤러
 *
 */
@CrossOrigin
@RequestMapping("/user")
@Controller
public class UserController {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenService tokenService;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    

	  @Autowired
	  private MessageSource msg;
	  
	  @Value("${rsa.privateKey}")
		 private String privateKey;
		 @Value("${rsa.publicKey}")
		 private String publicKey;
	
    // 토큰 발급 : /user/auth
	   @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
	    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,Locale locale) throws Exception {

		   String encPw = authenticationRequest.getPassword();
		   	PrivateKey key = RSA.getPrivateKeyFromString(privateKey);
		   	 
	        if(!authenticate(authenticationRequest.getUsername(), RSA.decryptRSA(encPw, key)))
	        {
	  		//ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail(msg.getMessage("login.bad", null, locale)));
	        	Map <String,Object> map = new HashMap<String,Object>();
	        	map.put("result", "fail");
	        	map.put("message",msg.getMessage("login.bad", null, locale));
	  		return ResponseEntity.ok(map);
	  		// return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail(msg.getMessage("login.bad", null, locale)));
	        
	        }
	       

	        // Reload password post-security so we can generate the token
	        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
	        final String token = jwtTokenUtil.generateToken(userDetails);

	        if(tokenService.insert(token)) {
	        // Return the token
	        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
	        }
	        
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));
	    }
	   
	   // 토큰 삭제
	   @ResponseBody
	   @RequestMapping(value = "/logout", method = RequestMethod.POST)
	    public ResponseEntity<?>  logout(@RequestBody Map<Object, Object> map) {
			String token = (String) map.get("token");
			if(tokenService.delete(token))
				return ResponseEntity.ok(JSONResult.success(true));
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(JSONResult.fail("fail"));	
	    }

	   
	   
	    private boolean authenticate(String username, String password) {
	        Objects.requireNonNull(username);
	        Objects.requireNonNull(password);
	        
	        try {
	            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
	            return true;
	        } catch (DisabledException e) {
	        	return false;
	        	//throw new AuthenticationException("User is disabled!", e);
	        } catch (BadCredentialsException e) {
	        	return false;
	           // throw new AuthenticationException("Bad credentials!", e);
	        }
	    }
	    
	    @ResponseBody
	    @GetMapping("/{name}")
	    public JSONResult searchUser(@PathVariable("name") String name) {
	    	return JSONResult.success(userService.searchUser(name));
	    }
	    
	
	    
	    @ResponseBody
	    @GetMapping("/distinct")
	    public JSONResult searchUser() {
	    	return JSONResult.success(userService.searchUser());
	    	
	    }
	    
	    @ResponseBody
	    @GetMapping("/a/{no}")
	    public JSONResult searchUserByNo(@PathVariable("no") Long no) {
	    	return JSONResult.success(userService.searchUser(no));
	    }
	    
	    


}