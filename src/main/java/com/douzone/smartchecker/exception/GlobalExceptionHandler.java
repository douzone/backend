package com.douzone.smartchecker.exception;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.douzone.dto.JSONResult;
import com.douzone.security.JwtUser;
import com.fasterxml.jackson.databind.ObjectMapper;


@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Log LOG = LogFactory.getLog( GlobalExceptionHandler.class );
	@ExceptionHandler(Exception.class)
	public void handlerException(HttpServletRequest request,HttpServletResponse response,Exception e) throws Exception {
		
//    	response.setContentType("application/json");
//      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//      response.getOutputStream().println("{ \"error\": \"" + authException.getMessage() + "\" }");
		

		
		//1.로깅 작업
		//errors를 string 으로 바꾸는 작업 printwriter 연결이 메모리로 되있음.
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		//System.out.println(errors.toString());
		
		//서비스할때는 log 찍어줘야함 -> 에러내용 파일저장]
		 if(SecurityContextHolder.getContext().getAuthentication() != null) {
		JwtUser userDetails = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userDetails != null)
		LOG.error("아이디:"+userDetails.getUsername()+"\t이름:"+userDetails.getName());
		 }
		 SimpleDateFormat sdf = new SimpleDateFormat("시간 : yyyy년 MM월 dd일 hh시 mm분 ss초");
		 Date now = new Date();
			String s =sdf.format(now);
			 LOG.error(s);
		 LOG.error("에러난 요청 URL : "+request.getRequestURL());
		LOG.error(errors.toString());
		
		
		String accept = request.getHeader("accept");
		if(accept.matches(".*application/json.*")) {
			
			if(e.getClass().equals(AccessDeniedException.class))
			{
				//json 응답 권한 에러 401
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
			} 		
			else if(e.getClass().equals(MethodArgumentNotValidException.class))
			{
				//json 응답 valid 에러 400 
				response.sendError(400, "valid error");
			}
			else {
			//json 응답 에러 500
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		    //get Writer 로 써도 됨. 예시라서 outputstream 으로
			OutputStream out = response.getOutputStream();
			JSONResult jsonResult = JSONResult.fail("Internal Server Error");
			
			out.write(new ObjectMapper().writeValueAsString(jsonResult).getBytes("utf-8"));
			out.flush();
			out.close();
			}
		}else {
			//html 응답
			request.setAttribute("uri", request.getRequestURI());
			request.setAttribute("exception", errors.toString());
				request.getRequestDispatcher("/WEB-INF/views/error/exception.jsp").forward(request, response);
		}
	}
	
	
}
