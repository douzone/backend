package com.douzone.smartchecker.vo;


import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


/**
 * @author PSH
 * @Date 2019. 4. 22.
 * @brief 활동 기록 테이블
 *
 */

@Document(collection="record")
public class RecordVo {
	private long no;
	
	@Field("id")
	private String id;
	@Field("day")
	private String day;
	private String actor;
	
	@Field("record_type")
	private String recordType;
	@Field("record_type_en")
	private String recordTypeEn;
	private String content;
	@Field("content_en")
	private String contentEn;
	
	public String getRecordTypeEn() {
		return recordTypeEn;
	}

	public void setRecordTypeEn(String recordTypeEn) {
		this.recordTypeEn = recordTypeEn;
	}

	public String getContentEn() {
		return contentEn;
	}

	public void setContentEn(String contentEn) {
		this.contentEn = contentEn;
	}

	private boolean read;
	
	@Field("update_time")
	private String updateTime;
	
	@Field("insert_time")
	private String insertTime;
	
	@Field("update_user_id")
	private String updateUserId;
	
	@Field("insert_user_id")
	private String insertUserId;
	
	@Field("key_no")
	private long keyNo;
	
	@Field("user_no")
	private long userNo;
	
	public long getUserNo() {
		return userNo;
	}

	public void setUserNo(long userNo) {
		this.userNo = userNo;
	}

	public long getKeyNo() {
		return keyNo;
	}

	public void setKeyNo(long keyNo) {
		this.keyNo = keyNo;
	}

	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getDay() {
		return day;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getActor() {
		return actor;
	}
	
	public void setActor(String actor) {
		this.actor = actor;
	}
	
	public String getRecordType() {
		return recordType;
	}
	
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public boolean isRead() {
		return read;
	}
	
	public void setRead(boolean read) {
		this.read = read;
	}
	
	public String getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getInsertTime() {
		return insertTime;
	}
	
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}
	
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public String getInsertUserId() {
		return insertUserId;
	}
	
	public void setInsertUserId(String insertUserId) {
		this.insertUserId = insertUserId;
	}

	@Override
	public String toString() {
		return "RecordVo [no=" + no + ", id=" + id + ", day=" + day + ", actor=" + actor + ", recordType=" + recordType
				+ ", recordTypeEn=" + recordTypeEn + ", content=" + content + ", contentEn=" + contentEn + ", read="
				+ read + ", updateTime=" + updateTime + ", insertTime=" + insertTime + ", updateUserId=" + updateUserId
				+ ", insertUserId=" + insertUserId + ", keyNo=" + keyNo + ", userNo=" + userNo + "]";
	}
}
