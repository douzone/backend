package com.douzone.smartchecker.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author JYS
 * @Date 2019. 4. 24.
 * @brief 휴식시간 테이블
 *
 */
public class BreakTimeVo {
	private long no;
	@NotEmpty 
	private String start;
	@NotEmpty 
	private String description;
	private String descriptionEn;
	@NotEmpty 
	private String end;
	@NotNull 
	private boolean use;
	private String updateTime;
	private String insertTime;
	private String updateUserId;
	private String insertUserId;
	
	public String getDescriptionEn() {
		return descriptionEn;
	}

	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}

	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}
	
	public String getStart() {
		return start;
	}
	
	public void setStart(String start) {
		this.start = start;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getEnd() {
		return end;
	}
	
	public void setEnd(String end) {
		this.end = end;
	}
	
	public boolean isUse() {
		return use;
	}
	
	public void setUse(boolean use) {
		this.use = use;
	}
	
	public String getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getInsertTime() {
		return insertTime;
	}
	
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}
	
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public String getInsertUserId() {
		return insertUserId;
	}
	
	public void setInsertUserId(String insertUserId) {
		this.insertUserId = insertUserId;
	}
	
	@Override
	public String toString() {
		return "BreakTimeVo [no=" + no + ", start=" + start + ", description=" + description + ", descriptionEn="
				+ descriptionEn + ", end=" + end + ", use=" + use + ", updateTime=" + updateTime + ", insertTime="
				+ insertTime + ", updateUserId=" + updateUserId + ", insertUserId=" + insertUserId + "]";
	}
}
