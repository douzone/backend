package com.douzone.smartchecker.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author PSH
 * @Date 2019. 5. 3.
 * @brief t_holiday
 *
 */
public class HoliDayVo {

	private long no;
	@NotEmpty 
	private String day;
	@NotEmpty 
	private String description;
	private String descriptionEn;
	@NotNull 
	private boolean use;
	private String updateTime;
	private String insertTime;
	private String updateUserId;
	private String insertUserId;
	
	public String getDescriptionEn() {
		return descriptionEn;
	}

	public void setDescriptionEn(String descriptionEn) {
		this.descriptionEn = descriptionEn;
	}

	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}
	
	public String getDay() {
		return day;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isUse() {
		return use;
	}
	
	public void setUse(boolean use) {
		this.use = use;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getInsertUserId() {
		return insertUserId;
	}

	public void setInsertUserId(String insertUserId) {
		this.insertUserId = insertUserId;
	}

	@Override
	public String toString() {
		return "HoliDayVo [no=" + no + ", day=" + day + ", description=" + description + ", descriptionEn="
				+ descriptionEn + ", use=" + use + ", updateTime=" + updateTime + ", insertTime=" + insertTime
				+ ", updateUserId=" + updateUserId + ", insertUserId=" + insertUserId + "]";
	}
	
}
