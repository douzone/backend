package com.douzone.smartchecker.vo;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * @author JYS
 * @Date 2019. 5. 13.
 * @brief token 저장 테이블
 *
 */

@Document(collection="token")
public class TokenVo {
	private long no;
	
	@Id
	private String id;
	
	private String token;
	
	
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TokenVo [no=" + no + ", id=" + id + ", token=" + token + "]";
	}

	



	

}
