package com.douzone.smartchecker.vo;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author JYS
 * @Date 2019. 4. 23.
 * @brief 근태 테이블
 *
 */
public class WorkAttitudeVo {
	private long no;
	private long userNo;
	private String name;
	private String day;
	private String dayEn;
	private String goTo;
	private String goOff;
	private String workTime;
	@NotEmpty 
	private String title;
	private String titleEn;
	@NotEmpty
	private String startDay;
	@NotEmpty
	private String endDay;
	private String applicationDate;
	@NotEmpty
	private String content;
	private String contentEn;
	private String state;
	private String stateEn;
	@NotNull
	private String workAttitudeList;
	private String workAttitudeListEn;
	private String updateTime;
	private String insertTime;
	private String updateUserId;
	private String insertUserId;
	
	public String getTitleEn() {
		return titleEn;
	}

	public void setTitleEn(String titleEn) {
		this.titleEn = titleEn;
	}

	public String getContentEn() {
		return contentEn;
	}

	public void setContentEn(String contentEn) {
		this.contentEn = contentEn;
	}

	
	public String getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getInsertTime() {
		return insertTime;
	}
	
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	
	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}
	
	public long getUserNo() {
		return userNo;
	}
	
	public void setUserNo(long userNo) {
		this.userNo = userNo;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getStartDay() {
		return startDay;
	}
	
	public void setStartDay(String startDay) {
		this.startDay = startDay;
	}
	
	public String getEndDay() {
		return endDay;
	}
	
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	
	public String getApplicationDate() {
		return applicationDate;
	}
	
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getWorkAttitudeList() {
		return workAttitudeList;
	}
	
	public void setWorkAttitudeList(String workAttitudeList) {
		this.workAttitudeList = workAttitudeList;
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}
	
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public String getInsertUserId() {
		return insertUserId;
	}
	
	public void setInsertUserId(String insertUserId) {
		this.insertUserId = insertUserId;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getGoTo() {
		return goTo;
	}

	public void setGoTo(String goTo) {
		this.goTo = goTo;
	}

	public String getGoOff() {
		return goOff;
	}

	public void setGoOff(String goOff) {
		this.goOff = goOff;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

	public String getDayEn() {
		return dayEn;
	}

	public void setDayEn(String dayEn) {
		this.dayEn = dayEn;
	}

	public String getStateEn() {
		return stateEn;
	}

	public void setStateEn(String stateEn) {
		this.stateEn = stateEn;
	}
	
	public String getWorkAttitudeListEn() {
		return workAttitudeListEn;
	}

	public void setWorkAttitudeListEn(String workAttitudeListEn) {
		this.workAttitudeListEn = workAttitudeListEn;
	}

	@Override
	public String toString() {
		return "WorkAttitudeVo [no=" + no + ", userNo=" + userNo + ", name=" + name + ", day=" + day + ", goTo=" + goTo
				+ ", goOff=" + goOff + ", workTime=" + workTime + ", title=" + title + ", titleEn=" + titleEn
				+ ", startDay=" + startDay + ", endDay=" + endDay + ", applicationDate=" + applicationDate
				+ ", content=" + content + ", contentEn=" + contentEn + ", state=" + state + ", workAttitudeList="
				+ workAttitudeList + ", updateTime=" + updateTime + ", insertTime=" + insertTime + ", updateUserId="
				+ updateUserId + ", insertUserId=" + insertUserId + "]";
	}
}
