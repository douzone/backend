package com.douzone.smartchecker.vo;

public class TimeVo {
	private long no;
	private String insertTime;
	private String startTime;
	private String endTime;
	public long getNo() {
		return no;
	}
	public void setNo(long no) {
		this.no = no;
	}
	public String getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	@Override
	public String toString() {
		return "TimeVo [no=" + no + ", insertTime=" + insertTime + ", startTime=" + startTime + ", endTime=" + endTime
				+ "]";
	}
	
}