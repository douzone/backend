package com.douzone.smartchecker.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author PSH
 * @Date 2019. 4. 22.
 * @brief 출퇴근 테이블
 *
 */
@Entity
@Table(name = "t_commute")
public class CommuteVo {
	
	@Id
	@Column(name="commute_no", nullable = false, unique = true)
	private long no;
	
	@Column(name="user_no", nullable = false, unique = true)
	private long userNo;
	
	private long groupNo;
	private String state;
	private String day;
	private String dayEn;
	private String totalWorktime;
	private String commute;
	private String updateTime;
	private String insertTime;
	private String updateUserId;
	private String insertUserId;
	private String name;
	private long startNo;
	private long endNo;
	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String fromState;
	private String fromStateEn;
	private String toState;
	private String toStateEn;
	private String startCommute;
	private String endCommute;

	public long getGroupNo() {
		return groupNo;
	}

	public void setGroupNo(long groupNo) {
		this.groupNo = groupNo;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getNo() {
		return no;
	}
	
	public void setNo(long no) {
		this.no = no;
	}
	
	public long getUserNo() {
		return userNo;
	}
	
	public void setUserNo(long userNo) {
		this.userNo = userNo;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getDay() {
		return day;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public String getTotalWorktime() {
		return totalWorktime;
	}
	
	public void setTotalWorktime(String totalWorktime) {
		this.totalWorktime = totalWorktime;
	}
	
	public String getCommute() {
		return commute;
	}
	
	public void setCommute(String commute) {
		this.commute = commute;
	}
	
	public String getUpdateTime() {
		return updateTime;
	}
	
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	public String getInsertTime() {
		return insertTime;
	}
	
	public void setInsertTime(String insertTime) {
		this.insertTime = insertTime;
	}
	
	public String getUpdateUserId() {
		return updateUserId;
	}
	
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	
	public String getInsertUserId() {
		return insertUserId;
	}
	
	public void setInsertUserId(String insertUserId) {
		this.insertUserId = insertUserId;
	}

	public long getStartNo() {
		return startNo;
	}

	public void setStartNo(long startNo) {
		this.startNo = startNo;
	}

	public long getEndNo() {
		return endNo;
	}

	public void setEndNo(long endNo) {
		this.endNo = endNo;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getFromState() {
		return fromState;
	}

	public void setFromState(String fromState) {
		this.fromState = fromState;
	}

	public String getToState() {
		return toState;
	}

	public void setToState(String toState) {
		this.toState = toState;
	}

	public String getStartCommute() {
		return startCommute;
	}

	public void setStartCommute(String startCommute) {
		this.startCommute = startCommute;
	}

	public String getEndCommute() {
		return endCommute;
	}

	public void setEndCommute(String endCommute) {
		this.endCommute = endCommute;
	}

	public String getDayEn() {
		return dayEn;
	}

	public void setDayEn(String dayEn) {
		this.dayEn = dayEn;
	}

	public String getFromStateEn() {
		return fromStateEn;
	}

	public void setFromStateEn(String fromStateEn) {
		this.fromStateEn = fromStateEn;
	}

	public String getToStateEn() {
		return toStateEn;
	}

	public void setToStateEn(String toStateEn) {
		this.toStateEn = toStateEn;
	}

	@Override
	public String toString() {
		return "CommuteVo [no=" + no + ", userNo=" + userNo + ", groupNo=" + groupNo + ", state=" + state + ", day="
				+ day + ", totalWorktime=" + totalWorktime + ", commute=" + commute + ", updateTime=" + updateTime
				+ ", insertTime=" + insertTime + ", updateUserId=" + updateUserId + ", insertUserId=" + insertUserId
				+ ", name=" + name + ", startNo=" + startNo + ", endNo=" + endNo + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", startTime=" + startTime + ", endTime=" + endTime + ", fromState="
				+ fromState + ", toState=" + toState + ", startCommute=" + startCommute + ", endCommute=" + endCommute
				+ "]";
	}
}
