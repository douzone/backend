package com.douzone.security;

import java.util.Collection;

import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.User;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtUser extends User {

	   private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	    // 유저의 정보를 더 추가하고 싶다면 이곳과, 아래의 생성자 파라미터를 조절해야 한다.
	  
	    private String name;
	    private Long no;

	    public JwtUser(String id, String password ,String name,Long no
	            , boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked
	            , Collection authorities
	            ) {
	        super(id, password
	                , enabled, accountNonExpired, credentialsNonExpired, accountNonLocked
	                , authorities);
	      
	        this.name=name;
	        this.no=no;
	    }
	    
		public Long getNo() {
			return no;
		}

		public void setNo(Long no) {
			this.no = no;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

    
    



}
