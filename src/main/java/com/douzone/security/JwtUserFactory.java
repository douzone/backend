package com.douzone.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.douzone.smartchecker.vo.UserVo;

public final class JwtUserFactory {
	
	  static boolean enabled = true;
	  static boolean accountNonExpired = true;
	  static boolean credentialsNonExpired = true;
	  static boolean accountNonLocked = true;


    private JwtUserFactory() {
    }

    public static JwtUser create(UserVo user) {
    	  if (user != null) {
          	int num=0;
              if(user.getRole().equals("ADMIN"))
               num = 1;
              else
               num=2;
              JwtUser jwtUser = new JwtUser(user.getUsername(),
                      user.getPassword(),
                      user.getName(),
                      user.getNo(),
                      enabled,
                      accountNonExpired,
                      credentialsNonExpired,
                      accountNonLocked,
                      getAuthorities(num)
              );
              return jwtUser;
          } else {
        	  JwtUser jwtUser = new JwtUser("empty",
                      "empty",
                      "empty",
                      null,
                      false,
                      true,
                      true,
                      false,
                      getAuthorities(1)
              );
              return jwtUser;
          	}

    }
    	  
    	  public static List<GrantedAuthority> getAuthorities(Integer role) {

    	        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
    	        if (role == 1) {
    	            authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
    	        } else if (role == 2) {
    	            authList.add(new SimpleGrantedAuthority("ROLE_USER"));
    	        }

    	        return authList;
    	    }




}
