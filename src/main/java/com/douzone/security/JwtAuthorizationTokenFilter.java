package com.douzone.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.douzone.smartchecker.repository.TokenDao;
import com.douzone.smartchecker.repository.UserRepository;
import com.douzone.smartchecker.vo.TokenVo;
import com.douzone.smartchecker.vo.UserVo;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserDetailsService userDetailsService;
    private final JwtTokenUtil jwtTokenUtil;
    private final String tokenHeader;
    private final UserRepository userRepository;
    private final TokenDao tokenDao;

    public JwtAuthorizationTokenFilter(@Qualifier("jwtUserDetailsService") UserDetailsService userDetailsService, JwtTokenUtil jwtTokenUtil, @Value("${jwt.header}") String tokenHeader,UserRepository userRepository,TokenDao tokenDao) {
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.tokenHeader = tokenHeader;
        this.userRepository=userRepository;
        this.tokenDao=tokenDao;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        logger.debug("processing authentication for '{}'", request.getRequestURL());
        
        System.out.println("@@@@@@@@@@필터처리 시작@@@@@@@@@@@@@");
        System.out.println("요청 URL : "+request.getRequestURL());
        final String requestHeader = request.getHeader(this.tokenHeader);

        String username = null;
        String authToken = null;
        
        System.out.println(request.getMethod());
        
        System.out.println("헤더: " + requestHeader);
        
        if(!request.getRequestURL().toString().equals("http://localhost:8080/user/auth")&&!request.getRequestURL().toString().equals("http://localhost:8080/user")&&!request.getMethod().equals("OPTIONS")) {
        
        if (requestHeader != null && requestHeader.startsWith("Bearer ")) {
        	
        	boolean isBe = false;
            authToken = requestHeader.substring(7);
            //Mongo 에서 토큰 찾기
            for(TokenVo vo :tokenDao.findAll()) {
   			 if(vo.getToken().equals(authToken)) {
   				 isBe=true;
   				 break;
   			 }	
   		 }
            if(!isBe) {
            System.out.println("등록되지 않은 토큰입니다.");
            return;
            }
            
            try {
                username = jwtTokenUtil.getUsernameFromToken(authToken);
            } catch (IllegalArgumentException e) {
                logger.error("an error occurred during getting username from token", e);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore", e);
            }
        } else {
            logger.warn("couldn't find bearer string, will ignore the header");
            //return;
        }

        logger.debug("checking authentication for user '{}'", username);
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            logger.debug("security context was null, so authorizing user");

            // It is not compelling necessary to load the use details from the database. You could also store the information
            // in the token and read it from it. It's up to you ;)            
            UserDetails userDetails;
            try {
            	 UserVo vo = userRepository.findByUsername(username);;
                userDetails = JwtUserFactory.create(vo);
            } catch (UsernameNotFoundException e) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
                return;
            }


            // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
            // the database compellingly. Again it's up to you ;)
            if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                

                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                logger.info("authorized user '{}', setting security context", username);
                
                JwtUser jwtUser = (JwtUser)authentication.getPrincipal();
                
                SecurityContext securityContext = SecurityContextHolder.getContext();
                securityContext.setAuthentication(authentication);
                
               // System.out.println("유저등록@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
               // System.out.println(jwtUser);
            }
        }
        
        System.out.println("@@@@@@@@@@필터처리 끝@@@@@@@@@@@@@");
        }
        chain.doFilter(request, response);
        
        
    }
}