package com.douzone.dto;

public class DashBoardDto {
	
	
	private long totalUserCount;
	private long gotoWorkCount;
	private long tardyCount;
	private long businessTripCount;
	private long workOutsideCount;
	private long annualLeaveCount;
	private long educationCount;
	private String date;
	
	public long getBusinessTripCount() {
		return businessTripCount;
	}
	public void setBusinessTripCount(long businessTripCount) {
		this.businessTripCount = businessTripCount;
	}
	public long getWorkOutsideCount() {
		return workOutsideCount;
	}
	public void setWorkOutsideCount(long workOutsideCount) {
		this.workOutsideCount = workOutsideCount;
	}

	public long getAnnualLeaveCount() {
		return annualLeaveCount;
	}
	public void setAnnualLeaveCount(long annualLeaveCount) {
		this.annualLeaveCount = annualLeaveCount;
	}
	public long getEducationCount() {
		return educationCount;
	}
	public void setEducationCount(long educationCount) {
		this.educationCount = educationCount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public long getTotalUserCount() {
		return totalUserCount;
	}
	public void setTotalUserCount(long totalUserCount) {
		this.totalUserCount = totalUserCount;
	}
	public long getGotoWorkCount() {
		return gotoWorkCount;
	}
	public void setGotoWorkCount(long gotoWorkCount) {
		this.gotoWorkCount = gotoWorkCount;
	}
	public long getTardyCount() {
		return tardyCount;
	}
	public void setTardyCount(long tardyCount) {
		this.tardyCount = tardyCount;
	}
	
	

}
